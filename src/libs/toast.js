import {codeMsg} from 'libs/api.js'

export default {
  e (ctx, title, msg) {
    ctx.$root.$refs.toast.e(msg, title)
  },

  s (ctx, title, msg) {
    ctx.$root.$refs.toast.s(msg, title)
  },

  w (ctx, title, msg) {
    ctx.$root.$refs.toast.w(msg, title)
  },

  i (ctx, title, msg) {
    ctx.$root.$refs.toast.i(msg, title)
  },

  code (ctx, data) {
    let msg = codeMsg(ctx, data)
    ctx.$root.$refs.toast.e(msg, ctx.$t('common.fail'))
  }
}