// import axios from 'axios'
import debugCfg from "share/debugCfg.js"
import ebus from 'libs/ebus.js'
// var nats = require('websocket-nats')
var nats = require('./websocket-nats')


var useScheme = 'http'
var hostname = window.location.hostname
if (debugCfg.debugOn) {
  hostname = debugCfg.ctServerIp
}
var config = {
  baseURL: `${useScheme}://${hostname}:${debugCfg.ctServerPort}`,
  withCredentials: debugCfg.debugOn ? true : false,
  timeout: 1000
}
export var websocket = window.location.protocol === 'https:' ? 'wss' : 'ws';

export function NatsApi() {
  //连接
  this.CreatConnect = function (optin) {
    return new Promise(function (resolve, reject) {
      var host = ''
      if(optin && optin.url){
        host = optin.url
      } else{
        host = hostname
      }
			let port = optin.port ? optin.port : 8080
			console.log('ws_url','ws://' + host + ':'+ port +'/_ws/stbp');
      this.conn = nats.connect({
        reconnect: false,
        // verbose: true,
        url: 'ws://' + host + ':'+ port +'/_ws/stbp'

      },function(res){
        resolve('connct')
        // ebus.emit('connct')
      },function(res){
        ebus.emit('disconnct')
        reject('timeout');
      }).on('error', e => {
        console.warn('catch error:', e);
      })
      var timer = setTimeout(function () {
        if(timer){
          clearTimeout(timer)
        }
        reject('timeout');
      }, 3400)
    }.bind(this));   
    
  }
  //断开连接
  this.DisConnect = function () {
    // if (this.conn) {
    //   this.conn.close()
    //   this.conn = null
    // }
    this.conn && this.conn.close()
  }
  //订阅
  this.onSubscribe = function () {
    if (!this.conn) {
      return
    }
    let topic = this.subTopicType + '.' + this.subTopicId
    if (!topic) {
      return
    }
    let sub = _.find(this.subList, {
      key: topic
    })
    if (sub) {
      return
    }
    var sid = this.conn.subscribe(topic, function (msg) {
      console.log('msg:', msg)
      this.addMsg(topic, msg)
    }.bind(this))
    this.addMsg('debug', 'subscribe: ' + topic)
    this.subList.push({
      key: topic,
      sid: sid,
      show: true,
      index: 0,
      msgs: [],
      actions: [{
        name: '退订',
        doClick: this.onUnsubscribe
      }, {
        name: '暂停',
        doClick: this.onPause
      }, {
        name: '恢复',
        doClick: this.onResume
      }]
    })
  }
  //取消订阅
  this.onUnsubscribe = function (item) {
    if (!this.conn) {
      return
    }

    let topic = item.key

    let index = _.findIndex(this.subList, {
      key: topic
    })
    if (index < 0) {
      return
    }

    if (this.selected === topic) {
      this.selected = this.subList[0].key
    }
    this.subList.splice(index, 1)

    this.conn.unsubscribe(item.sid)
    this.addMsg('debug', 'unsubscribe: ' + topic)
  }
  //发送请求
  this.onRequest = function (subject, opt_msg, opt_options, clientId, callback) {
    // this.conn.request(subject, opt_msg, opt_options, clientId, callback);    
    var promise = new Promise(function (resolve, reject) {
      this.conn.request(subject, opt_msg, opt_options, clientId, function(msg, reply){                
        resolve(msg)
        if(typeof(callback) == 'function'){
          callback(msg, reply)
        }        
      }.bind(this));
      setTimeout(function () {
        reject('timeout');
      }, 3000)
    }.bind(this));
    return promise;  
    
  }

  this.isConnected = function () {
    if (this && this.conn && this.conn.isConnected()) {
      console.log('Connected')
      return true
    } else {
      console.log('DisConnected')
      return false
    }
  }
}
export function NatsApiWss() {
  //连接
  this.CreatConnect = function (optin) {
    return new Promise(function (resolve, reject) {
      var host = ''
      if(optin && optin.url){
        host = optin.url
      } else{
        host = hostname
      }
      let port = optin.port ? optin.port : 8080
      this.conn = nats.connect({
        reconnect: false,
        // verbose: true,
        url: 'wss://' + host + ':'+ port +'/_ws/stbp'

      },function(res){
        resolve('connct')
        // ebus.emit('connct')
      },function(res){
        ebus.emit('disconnct')
        reject('timeout');
      }).on('error', e => {
        console.warn('catch error:', e);
      })
      var timer = setTimeout(function () {
        if(timer){
          clearTimeout(timer)
        }
        reject('timeout');
      }, 3400)
    }.bind(this));   
    
  }
  //断开连接
  this.DisConnect = function () {
    // if (this.conn) {
    //   this.conn.close()
    //   this.conn = null
    // }
    this.conn && this.conn.close()
  }
  //订阅
  this.onSubscribe = function () {
    if (!this.conn) {
      return
    }
    let topic = this.subTopicType + '.' + this.subTopicId
    if (!topic) {
      return
    }
    let sub = _.find(this.subList, {
      key: topic
    })
    if (sub) {
      return
    }
    var sid = this.conn.subscribe(topic, function (msg) {
      console.log('msg:', msg)
      this.addMsg(topic, msg)
    }.bind(this))
    this.addMsg('debug', 'subscribe: ' + topic)
    this.subList.push({
      key: topic,
      sid: sid,
      show: true,
      index: 0,
      msgs: [],
      actions: [{
        name: '退订',
        doClick: this.onUnsubscribe
      }, {
        name: '暂停',
        doClick: this.onPause
      }, {
        name: '恢复',
        doClick: this.onResume
      }]
    })
  }
  //取消订阅
  this.onUnsubscribe = function (item) {
    if (!this.conn) {
      return
    }

    let topic = item.key

    let index = _.findIndex(this.subList, {
      key: topic
    })
    if (index < 0) {
      return
    }

    if (this.selected === topic) {
      this.selected = this.subList[0].key
    }
    this.subList.splice(index, 1)

    this.conn.unsubscribe(item.sid)
    this.addMsg('debug', 'unsubscribe: ' + topic)
  }
  //发送请求
  this.onRequest = function (subject, opt_msg, opt_options, clientId, callback) {
    // this.conn.request(subject, opt_msg, opt_options, clientId, callback);    
    var promise = new Promise(function (resolve, reject) {
      this.conn.request(subject, opt_msg, opt_options, clientId, function(msg, reply){                
        resolve(msg)
        if(typeof(callback) == 'function'){
          callback(msg, reply)
        }        
      }.bind(this));
      setTimeout(function () {
        reject('timeout');
      }, 3000)
    }.bind(this));
    return promise;  
    
  }

  this.isConnected = function () {
    if (this && this.conn && this.conn.isConnected()) {
      console.log('Connected')
      return true
    } else {
      console.log('DisConnected')
      return false
    }
  }
}


export var api = new NatsApi() //通用
export var getHostName = function () {
  return hostname;
}
