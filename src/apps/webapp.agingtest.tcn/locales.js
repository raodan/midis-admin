export default {
  en: {
    webapp: {
      type: 'Type',
      duration: 'Duration',
      start: 'Start',
      end: 'End',
      plzRemove: 'Please remove the devices that have been turned off',
      areYouSure: 'Are you sure set this device({ipaddr}) test error?',
      count: 'Count：',
      ipaddr: 'IP'
    }
  },
  cn: {
    webapp: {
      type: '测试类型',
      duration: '老化时长',
      start: '启动老化',
      end: '重置老化完成设备',
      plzRemove: '请移除已经关机的设备',
      areYouSure: '确定设置{ipaddr}老化错误吗？',
      count: '总数：',
      ipaddr: '设备IP'
    }
  }  
}