export default [{
  name: '关机',
  service: 'platform|md-server|mcn',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'shutdown.sh'
    }
  }
}, {
  name: '开机',
  service: 'mcn',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'restart.sh'
    }
  }
}, {
  name: '重启',
  service: 'platform|md-server|mcn',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'restart.sh'
    }
  }
}, {
  name: '查看网口信息',
  service: 'platform|ct-server|md-server|mcn',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'ethtool.sh'
    }
  }
}, {
  name: '浅压缩初始化',
  service: 'mcn',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'ethtool.sh'
    }
  }
}, {
  name: '修复用户参数区',
  service: 'platform',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'ubiformat_usercfg.sh',
      isBg: 1,
      shArg: ''
    }
  }
}, {
  name: '修复用户数据区',
  service: 'platform',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'ubiformat_userdata.sh',
      isBg: 1,
      shArg: ''
    }
  }
}, {
  name: '更新uboot',
  service: 'platform',
  URI: '/api/common/cmd',
  body: {
    action: 'runShell',
    paramlist: {
      shName: 'updateUBoot.sh',
      isBg: 1,
      shArg: ''
    }
  },
  password: '308db9f9e1faa9674b4cf12ece447bbf'
}]