export default {
  background: {
    rect: {
      left: 0,
      top: 0,
      right: 1,
      bottom: 1
    },
    image: "",
    show: 0
  },
  text: {
    color: "#1133ff",
    align: "center",
    alignV: "center",
    text: {
      fontName: "宋体",
      fontSize: 100,
      rect: {
        left: 0,
        top: 0,
        right: 1,
        bottom: 1
      },   
      content: "",   
    },
    show: 0
  },
  banner: {
    totalWidth: 0,
    totalHeight: 0,
    showRect: {
      left: 0,
      top: 0,
      right: 1,
      bottom: 1
    }    
  }
}