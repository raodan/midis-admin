
import cnAuth from './cnAuth'
import cnKvm from './cnKvm'
import cnCommon from './cnCommon'
import cnLcd from './cnLcd'
import cnCode from './cnCode'
import cnMcnCode from './cnMcnCode'
import cnError from './cnError'

export const message = {
///菜单
system: '系统管理',
syssetting: '系统设置',
sysstorage: '系统存储',
runninginfo: '运行信息',
netsetting: '网络设置',

audio: '音频设置',
video: '视频设置',
media: '媒体管理',
inputdevsetting: '输入设备设置',
inputchannelsetting: '输入通道设置',
outputchannelsetting: '输出设备设置',
outputdevsetting: '输出设备设置',
multicast: '组播设置',

control: '中控系统',
maintain: '系统维护',
kvm: '中控管理',
comsetting: '串口设置',
irsetting: '红外设置',
kvmDetail: 'KVM',

debug: '调试诊断',
packetcapture: '网络抓包',
webdav: '网络存储',
swagger: 'swagger',



homepage: '首页',
help: '帮助',
shortcut: '快捷操作',
translate: 'English',
save: '保存',
refresh: '刷新',
search: '搜索',
add: '添加',
name: '名称',
type: '类型',
shutdown: '关机',
reboot: '重启',
rebooting: '重启中...',
success: '成功',
addSuccess: '添加成功',
delSuccess: '删除成功',
updateSuccess: '更新成功',
fail: '失败',
fastOperation: '快捷操作',
operation: '操作',
guid: '操作指引',
language: 'zh_CN',
ok: '确定',
cancel: '取消',
upload: '上传',
youAreLost: '无法访问',
pageNotExist: '该页面不存在',
takeMeHome: '返回主页',
backup: '恢复出厂设置',
tip: '提示',
//登录相关
login: '登录',
username: '用户名',
password: '密码',
oldpassword: '旧密码',
newpassword: '新密码',
confirmpassword: '确认密码',
signout: '登出',
rememberpwd: '记住密码',
loginPage: '后台管理登录',
changePwd: '修改密码',

areyousure: '确定吗？',
willbeshutdown: '此操作将导致关机，确定吗？',
uploading: '升级中...',  
willbeRestart: '设备已掉线！',
import: '导入',
export: '导出',
send: '发送',

yes: '是',
no: '否',
sub: '订阅',
unSub: '取消订阅',
audio: '音频',
video: '视频',
timeout: '请求超时',
formatErr: '格式错误',

navigation: '导航',
dashboard: '后台管理',
home: '主页',
prevPage: '上页',
nextPage: '下页',
noChange: '无更改',
back: '返回',
edit: '编辑',

//多用户
addUser:'添加用户',
userSet: '用户配置',

auth: cnAuth,
kvm: cnKvm,
common: cnCommon,
lcd: cnLcd,
code: cnCode,
error: cnError,
mcnCode: cnMcnCode,


}
