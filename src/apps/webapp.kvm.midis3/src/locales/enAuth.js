export default {
  username: 'User name',
  password: 'Password',
  rememberme: 'Remember me',
  please: 'Please login',
  signin: 'Login',
  chgpwd: 'Change password',
  newPassword: 'New password',
  reNewPassword: 'Confirm password',
}