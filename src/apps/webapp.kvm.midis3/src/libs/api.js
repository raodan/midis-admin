import _ from 'lodash'

export function ProcessApi(http, processName, devId, makeUrl) {
  this.devId = devId

  this.makeUrls = function() {
    this.cfgUrl = makeUrl(processName, this.devId, 'cfg')
    this.stateUrl = makeUrl(processName, this.devId, 'state')
    this.cmdUrl = makeUrl(processName, this.devId, 'cmd')
    this.itemsUrl = makeUrl(processName, this.devId, 'items')
  }
  this.makeUrls()

  this.setDevId = function(devId) {
    this.devId = devId
    this.makeUrls()
  }

  this.getCfg = function() {
    return http(this.cfgUrl, 'GET')
  }

  this.setCfg = function(req) {
    return http(this.cfgUrl, 'PUT', req)
  }

  this.getState = function() {
    return http(this.stateUrl, 'GET')
  }

  this.sendCmd = function(action, paramlist) {
    var req = {
      action: action
    }
    if (typeof paramlist != 'undefined') {
      req.paramlist = paramlist
    }
    return http(this.cmdUrl, 'POST', req)
  }

  function makeItemsUrl(baseUrl, listname, id, query={}) {
    var url = baseUrl + '?listName=' + listname
    if (typeof id != 'undefined') {
      url += '&_id=' + id
    }
    for (let key in query) {
      url += '&' + key + '=' + query[key]
    }
    return url
  }

  this.getItems = function(listname, query={}) {
    let url = makeItemsUrl(this.itemsUrl, listname, undefined, query)
    return http(url, 'GET')
  }

  this.getItem = function(listname, id) {
    let url = makeItemsUrl(this.itemsUrl, listname, id)
    return http(url, 'GET')
  }

  this.delItem = function(listname, id) {
    let url = makeItemsUrl(this.itemsUrl, listname, id)
    return http(url, 'DELETE')
  }

  this.setItem = function(listname, req) {
    let url = makeItemsUrl(this.itemsUrl, listname)
    return http(url, 'PUT', req)
  }
}

export function StorageApi(http) {
  function makeFileUrl(type, path) {
    return `/api/storage/file?type=${type}&path=${path}`
  }

  this.getFile = function(type, path) {
    let url = makeFileUrl(type, path)
    return http(url, 'GET')
  }

  this.uploadFile = function(type, path, data, upload) {
    let url = makeFileUrl(type, path)
    return http(url, 'PUT', data, {
      headers: {
        'Content-Type': 'application/octet-stream',
        withCredentials: true
      }, 
      timeout: 3 * 60 * 1000,
      upload: upload})
  }
}

export function RestApi(http, category) {
  this.urlPrefix = `/api/${category}/`

  function makeResUrl(baseUrl, resName, query={}) {
    var url = baseUrl + resName
    if (_.size(query) > 0) {
      url += '?'
    }
    for (let key in query) {
      url += '&' + key + '=' + query[key]
    }
    return url
  }

  this.getStore = function(query={}) {
    let url = makeResUrl(this.urlPrefix, 'store', query)
    return http(url, 'GET')
  }

  this.sendCmd = function(action, paramlist) {
    return http(this.urlPrefix + action, 'POST', paramlist)
  }
}

export function codeStr(ctx, errno) {
  var msg = ctx.$t('message.error.unknown')
  if (errno > 0) {
    var code = `h${errno.toString(16)}`
    msg = ctx.$t(`code.${code}`)
  } else {
    msg = ctx.$t('message.error.code', {code: errno})
  }

  return msg
}

export function codeMsg(ctx, data) {
  var msg = ctx.$t('message.error.unknown')
  if (data) {
    if (data.code > 0) {
      var code = `h${data.code.toString(16)}`
      msg = ctx.$t(`message.code.${code}`)
    } else {
      msg = `${ctx.$t('message.error.code', {code: data.code})}(${data.msg})`
    }
  } 
  
  return msg 
}

export function mcnCodeMsg(ctx, data) {
  var msg = ctx.$t('message.error.unknown')
  if (data) {
    if (data.code > 0) {
      var code = `h${data.code.toString(16)}`
      msg = ctx.$t(`message.code.${code}`)
    } else {
      msg = `${ctx.$t('message.error.code', {code: data.code})}(${data.msg})`
    }
  } 
  
  return msg 
}