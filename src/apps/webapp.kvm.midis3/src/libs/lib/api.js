// import axios from 'axios'
import debugCfg from "../debugCfg.js"
import ebus from '../ebus.js'
// var nats = require('websocket-nats')
var nats = require('./websocket-nats/index.js')


var useScheme = 'http'
var hostname = window.location.hostname
if (debugCfg.debugOn) {
  hostname = debugCfg.ctServerIp
}
var config = {
  baseURL: `${useScheme}://${hostname}:${debugCfg.ctServerPort}`,
  withCredentials: debugCfg.debugOn ? true : false,
  timeout: 3000
}
export var websocket = window.location.protocol === 'https:' ? 'wss' : 'ws';

export function NatsApi() {

  this.Publish = function(uri, req, callback){
    var sid = this.conn.publish(uri, req,  function (msg) {
     callback(msg)
    }.bind(this)) 
  }

  //连接
  this.CreatConnect = function (optin) {

    return new Promise(function (resolve, reject) {
      var host = ''
      if(optin && optin.url){
        host = optin.url
      } else{
        host = hostname
      }
      this.conn = nats.connect({
        reconnect: false,
        // verbose: true,
        url: websocket + '://' + host + ':9080/_ws/stbp'
      },function(res){
        resolve('connct')
        // ebus.emit('connct')
      },function(res){
        ebus.emit('disconnct')
        reject('timeout');
        // console.warn('request timeout...');
      }).on('error', e => {
        console.warn('catch error:', e);
      })
      setTimeout(function () {        
        reject('timeout');
      }, 3000)
    }.bind(this));
    
  }
  //断开连接
  this.DisConnect = function () {
    try {
      if (this.conn) {
        this.conn.close()
        this.conn = null
      }
    } catch (err) {
      console.warn('Close Connection Error!', err);
    }      

    
  }
  //订阅
  this.onSubscribe = function (topic, callback) {
    var sid = this.conn.subscribe(topic, function (msg) {
      callback(msg)
      console.log('msg:', msg)
    }.bind(this))   
  }
  //取消订阅
  this.onUnsubscribe = function (sid) {
    this.conn.unsubscribe(sid)
  }
  //发送请求
  this.onRequest = function (subject, opt_msg, opt_options, clientId, callback) {
    console.log('send:', subject)
    console.log('option:', opt_msg)
    return new Promise(function (resolve, reject) {
      this.conn.request(subject, opt_msg, opt_options, clientId, function(msg, reply){ 
        console.log(subject + ':', msg);               
        resolve(msg)
        if(typeof(callback) == 'function'){
          callback(msg, reply)
        }        
      }.bind(this));
      setTimeout(function () {
        reject('timeout');
        // console.warn('request timeout: ', subject);
      }, 4000)
    }.bind(this));   
    
  }

  this.isConnected = function () {
    if (this && this.conn && this.conn.isConnected()) {      
      return true
    } else {
      console.log('DisConnected')
      return false
    }
  }
}


export var api = new NatsApi() //通用
export var getHostName = function () {
  return hostname;
}
