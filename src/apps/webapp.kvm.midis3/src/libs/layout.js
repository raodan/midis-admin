import _ from 'lodash'
import Konva from 'konva'
import ebus from '@/libs/ebus.js'
import * as lcd from '@/libs/lcd.js'
import {printObj, copyObj} from '@/libs/utils.js'

export function VLayout() {
  this.setMode = function(mode) {
    let stage = this.stage
    let userdata = stage.userdata

    if (mode === 'newWindow') {      
      // lcd.delAllWindows(stage, userdata.layoutLayer)
      this.updateByWindowList([]);
      lcd.addNewWindowEvent(stage, userdata.layoutLayer)
      lcd.delSetWindowEvent(stage)
    } else if (mode === 'setWindow') {
      lcd.delNewWindowEvent(stage)
      lcd.addSetWindowEvent(stage)
    }
  } 
  
  this.checkWindowOverlay = function() {
    return lcd.checkWindowOverlay(this.stage)
  }

  this.getWindowList = function() {
    return lcd.getWindowList(this.stage)
  }

  this.getWindowListCnt = function() {
    return lcd.getWindowListCnt(this.stage)
  }

  this.updateByWindowList = function(windowList) {
    let stage = this.stage
    let userdata = stage.userdata

    lcd.delAllWindows(stage, userdata.layoutLayer)

    let sortedWindows = _.sortBy(windowList, ['layer'])
    _.forEach(sortedWindows, function(item) {
      lcd.openWindow(stage, userdata.layoutLayer, item)
    })

    this.setMode('setWindow')
  }

  function initUserDataWithCfg(userdata, cfg) {
    
    cfg.unitXCnt = cfg.multiScreenX * 12//横向最小格子的总数
    cfg.unitYCnt = cfg.multiScreenY * 12//纵向最小格子的总数
    cfg.unitWidth = (cfg.width - (cfg.screenPitchX + 2) * (cfg.multiScreenX - 1)) / cfg.unitXCnt//横向最小格子的高度
    cfg.unitHeight = (cfg.height - (cfg.screenPitchY + 2) * (cfg.multiScreenY - 1)) / cfg.unitYCnt//纵向最小格子的高度

    userdata.cfg = cfg
  }

  this.init = function(container, cfg) {
    if (this.stage) {
      ebus.off('reset-lcd')
      ebus.off('layout-new-window-end')      
      this.stage.destroy()
      this.stage = undefined
    }
    //创建一个width * height的画布，所有图形都在此基础上画
    this.stage = new Konva.Stage({
      container: container,
      width: cfg.width,
      height: cfg.height
    })

    var userdata = {}
    initUserDataWithCfg(userdata, cfg)
    userdata.windows = []
    userdata.dftLayer = new Konva.Layer()
    userdata.layoutLayer = new Konva.Layer()
    this.stage.userdata = userdata
    this.stage.add(userdata.dftLayer, userdata.layoutLayer)

    ebus.on('layout-reset-lcd', function() {
      lcd.updateWindows(this.stage, userdata.layoutLayer)
    }.bind(this))

    lcd.drawLayoutGrid(userdata.dftLayer, userdata.cfg)

    ebus.on('layout-new-window-end', function() {
      this.setMode('setWindow')
    }.bind(this))
  }

  this.uninit = function() {
    ebus.off('reset-lcd')
    ebus.off('layout-new-window-end')
    this.stage && this.stage.destroy()
    this.stage = undefined
  }
}