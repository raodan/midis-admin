import swal from 'sweetalert'

export default {
  alert (...args) {
    swal(...args)
  },
  close () {
    swal.close()
  },
  showWait (title, text, waitSec) {
    var cfg = {
      title: title,
      text: text,
      type: "info",
      showConfirmButton: false      
    }
    if (waitSec > 0) {
      cfg.timer = waitSec * 1000
    }
    swal(cfg)
  }
}