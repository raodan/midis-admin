import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'id',
    width: 'auto'
  }, {
    field: 'name',
    width: 'auto'
  }, {
    field: 'lcdTopoId',
    width: 'auto'
  }, {
    field: 'srcListType',
    width: 'auto',
    show: function(item) {
      return ctx.$t('options.' + item.srcListType)
    }
  }, {
    field: 'interval',
    width: 'auto'
  }, {
    field: 'enable',
    width: 'auto',
    show: function(item) {
      if (item.enable) {
        return ctx.$t('common.yes')
      } else {
        return ctx.$t('common.no')
      }
    }
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}