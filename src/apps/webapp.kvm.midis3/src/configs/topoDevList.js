import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'topoId',
    width: '5%'
  }, {
    field: 'devSN',
    width: '25%'
  }, {
    field: 'devName',
    width: '20%'
  }, {
    field: 'devType',
    width: '10%'
  }, {
    field: 'devModel',
    width: '20%'
  }, {
    field: 'devTag',
    width: 'auto'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}