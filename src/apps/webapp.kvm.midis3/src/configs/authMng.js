import _ from 'lodash'

export function Tabs(ctx) {
  var tabs = [{
    field: 'user',
    url: '/authmng'
  }, {
    field: 'secpolicy',
    url: '/authmng/secpolicy'
  }]

  _.forEach(tabs, (item) => {
    let keypath = 'tabs.' + item.field
    item.name = ctx.$t(keypath)
  })

  return _.filter(tabs, (item) => {
    if ((ctx.supportSecPolicy === false) 
      && (item.field === 'secpolicy')) {
      return false
    } else {
      return true
    }
  })
}