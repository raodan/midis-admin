import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'manufacture',
    width: 'auto'
  }, {
    field: 'productModelsStr',
    width: 'auto'
  }, {
    field: 'mainPathFmt',
    width: '30%'
  }, {
    field: 'subPathFmt',
    width: '30%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}