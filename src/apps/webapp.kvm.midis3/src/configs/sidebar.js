import _ from 'lodash'
import {userRoleCfg} from './userRoleCfg.js'

export function Menus(ctx) {
  var list = [{
    field: "kvm",
    name: "kvm",
    icon: 'menu-icon fa fa-server fa-lg',
    url: '/kvm'
  },
]

  var listOfDemo = [{
    field: "scriptplugin",
    icon: 'menu-icon fa fa-file-code-o fa-lg',
    url: '/scriptplugin'
  }]

  // let mainVersion = ctx.platform.softwareVersion[0]
  // if (parseInt(mainVersion) >= 2) {
  //   list =_.concat(list, listOfDemo)
  // }

  // _.forEach(list, (item) => {
  //   let keypath = 'menus.' + item.field
  //   item.name = ctx.$t(keypath)
  // })

  // //嵌入式平台不支持某些应用
  // if (!ctx.platform.devModel.includes('pc')) {
  //   list = _.filter(list, (item) => {
  //     return item.field != 'scriptplugin'
  //   })
  // }

  // if (ctx.client.role && userRoleCfg[ctx.client.role]) {
  //   let cfg = userRoleCfg[ctx.client.role]
  //   if (cfg.includes !== '*') {
  //     list = _.filter(list, (item) => {
  //       return cfg.includes.includes(item.field)
  //     })
  //   }

  //   if (cfg.exculdes.length > 0) {
  //     list = _.filter(list, (item) => {
  //       return !cfg.exculdes.includes(item.field)
  //     })
  //   }
  // }

  return list
}

export function Sites(ctx) {
  var list = [{
    field: 'company',
    url: 'http://www.tendzone.com/'
  }, {
    field: "about",
    url: 'http://www.tendzone.com/'
  }, {
    field: "support",
    url: 'http://www.tendzone.com/'
  }]

  _.forEach(list, (item) => {
    let keypath = 'sites.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}