import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'name',
    width: 'auto'
  }, {
    field: 'addr',
    width: 'auto'
  }, {
    field: 'topoId',
    width: 'auto'
  }, {
    field: 'upCallCount',
    width: 'auto'
  }, {
    field: 'upCallBand',
    width: 'auto'
  }, {
    field: 'downCallCount',
    width: 'auto'
  }, {
    field: 'downCallBand',
    width: 'auto'
  }, {
    field: 'status',
    width: 'auto',
    show: function(item) {
      if (item.status === '') {
        return ctx.$t('common.unknown')
      } else {
        return ctx.$t(`statusOpt.${item.status}`)
      }
    }    
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}