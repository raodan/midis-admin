export let userRoleCfg = {
  webSysAdmin: {
    exculdes: ['log'],
    includes: '*',
    dftUrl: '/'
  },
  webSecAdmin: {
    exculdes: [],
    includes: ['authmng', 'log'],
    dftUrl: '/authmng'
  },
  webAuditor: {
    exculdes: [],
    includes: ['authmng', 'log'],
    dftUrl: '/authmng'
  },
  webUser: {
    exculdes: ['system', 'license', 'scriptplugin', 'authmng', 'log'],
    includes: '*',
    dftUrl: '/monitor'
  }
}