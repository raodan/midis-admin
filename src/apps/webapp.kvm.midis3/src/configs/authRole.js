import _ from 'lodash'

export function List(ctx) {
  var list = [{
    value: 'webSysAdmin'
  }, {
    value: 'webSecAdmin'
  }, {
    value: 'webAuditor'
  }, {
    value: 'webUser'
  }]

  _.forEach(list, (item) => {
    let keypath = 'role.' + item.value
    item.text = ctx.$t(keypath)
  })

  return list 
}