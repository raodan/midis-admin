import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: '5%'
  }, {
    field: 'topoId',
    width: '12%'
  },{
    field: 'name',
    width: '15%'
  }, {
    field: 'alias',
    width: '15%'
  }, {
    field: 'recStatus',
    width: '12%',
    show: function(item) {
      if(item.recStatus === 'recording') {
        return ctx.$t('list.recording')
      } else  if(item.recStatus === 'stoped') {
        return ctx.$t('list.stoped')
      }
    }
  }, {
    field: 'liveStatus',
    width: '12%',
    show: function(item) {
      if(item.liveStatus === 'playing') {
        return ctx.$t('list.playing')
      } else  if(item.liveStatus === 'stoped') {
        return ctx.$t('list.stoped')
      }
    }
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}