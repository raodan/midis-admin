import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: '5%'
  }, {
    field: 'roomName',
    width: '15%'
  },{
    field: 'scene',
    width: '15%'
  }, {
    field: '_live',
    width: '15%',
    show: function(item) {
      if (item.liveStatus === 'playing') {
        return '<a href="tzdb://' + item.liveUrl.substr(7) + '" >' + ctx.$t('list._live') + '</a>'
      } else {
        return ''
      }
    }
  }, {
    field: 'status',
    width: '15%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}