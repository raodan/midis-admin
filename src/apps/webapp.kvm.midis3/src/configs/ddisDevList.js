import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'ipaddr',
    width: '10%'
  }, {
    field: 'devModel',
    width: '20%'
  }, {
    field: 'swVersion',
    width: '20%'
  }, {
    field: 'devSN',
    width: '25%'
  }, {
    field: 'mac',
    width: '10%'
  }, {
    field: 'link',
    width: '5%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}