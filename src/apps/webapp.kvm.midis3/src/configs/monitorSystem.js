import _ from 'lodash'

export function Tabs(ctx) {
  var tabs = [{
    field: 'ipc',
    url: '/monitor'
  }, {
    field: 'vendor',
    url: '/monitor/vendor'
  }, {
    field: 'plat',
    url: '/monitor/plat'
  }]

  _.forEach(tabs, (item) => {
    let keypath = 'tabs.' + item.field
    item.name = ctx.$t(keypath)
  })

  return tabs
}