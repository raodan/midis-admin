import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'date',
    width: '15%'
  }, {
    field: 'ip',
    width: '15%'
  }, {
    field: 'username',
    width: '15%'
  }, {
    field: 'role',
    width: '15%'
  }, {
    field: 'action',
    width: 'auto'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}