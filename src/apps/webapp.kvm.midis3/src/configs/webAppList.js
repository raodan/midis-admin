import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'all'
  }, {
    field: 'multimedia'
  }, {
    field: 'control'
  }, {
    field: 'tool'
  }, {
    field: 'debug'
  }, {
    field: 'other'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}