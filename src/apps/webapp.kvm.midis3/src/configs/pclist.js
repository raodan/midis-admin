import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: '5%'
  }, {
    field: 'name',
    width: '10%'
  }, {
    field: 'hostname',
    width: '10%'
  }, {
    field: 'ipaddr',
    width: '15%'
  }, {
    field: 'macaddr',
    width: '15%'
  }, {
    field: 'state',
    width: '10%',
    show: function(item) {
      if (!item.state) {
        return ctx.$t('common.unknown')
      } else {
        return ctx.$t('local.' + item.state)
      }
    }
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}