import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: '10%'
  }, {
    field: 'name',
    width: '20%'
  }, {
    field: 'ptzStatus',
    width: '10%',
    show: function(item) {
      if (item.ptzStatus === 'enable' || item.ptzStatus === 'disable') {
        return ctx.$t(`onvif.${item.ptzStatus}`)
      } else {
        return ctx.$t(`onvif.checking`)
      }
    }
  }, {
    field: 'tag',
    width: '25%'
  }, {
    field: 'ipaddr',
    width: '15%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}