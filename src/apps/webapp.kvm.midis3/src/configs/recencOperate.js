import _ from 'lodash'

export function List(ctx) {
  var list = [
    {
      field: 'name',
      width: '16%',
    },
    {
      field: 'port',
      width: '16%',
    }, {
      field: 'recState',
      width: '28%',
    }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list
}