import Vue from 'vue'
import App from './App'
import router from './router'
import VueI18n from 'vue-i18n'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import debugCfg from "@/libs/debugCfg"
import axios from 'axios'
import toast from '@/libs/toast.js'
import store from './store/index.js'

// 引入样式
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'sweetalert/dist/sweetalert.css'
import './external/css/octicons.css'
import './external/css/rdash.min.css'
import './main.css'

require('@/libs/sortable.js');

Vue.config.productionTip = false

var hostname = window.location.hostname;
// var baseURL = '/'
if (debugCfg.debugOn) {
  hostname = debugCfg.ctServerIp;
  // baseURL = '/'
}
axios.defaults.baseURL = '/';
// axios.defaults.baseURL = baseURL;
// axios.defaults.baseURL = `http://${hostname}:9080`
// post请求头
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
axios.defaults.withCredentials = true
// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 对响应错误做点什么
  if(error.response.status == 401 || error.response.status == 502){
    
    // toast.code(this, this.$t('message.willbeRestart'));
    //未登录，跳到登录界面
    router.push("/login");
  }
  return Promise.reject(error);
});

//多语言
Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: 'zh-cn',
  messages: {
    'en': require('./locales/en.js'),
    'zh-cn': require('./locales/cn.js'),
    ...zhLocale,
    ...enLocale,
  }, 
})

Vue.use(ElementUI, { zhLocale })

import {
  alert as VsAlert,
  carousel as VsCarousel,
  slider as VsSlider,
  accordion as VsAccordion,
  affix as VsAffix,
  aside as VsAside,
  checkboxBtn as VsCheckboxBtn,
  checkboxGroup as VsCheckboxGroup,
  datepicker as VsDatepicker,
  dropdown as VsDropdown,
  modal as VsModal,
  option as VsOption,
  panel as VsPanel,
  popover as VsPopover,
  progressbar as VsProgressbar,
  radioGroup as VsRadioGroup,
  radioBtn as VsRadioBtn,
  select as VsSelect,
  tab as VsTab,
  tabset as VsTabset,
  tooltip as VsTooltip,
  typeahead as VsTypeahead,
  navbar as VsNavbar,
  spinner as VsSpinner
} from 'vue-strap'

Vue.component('vs-alert', VsAlert)
Vue.component('vs-carousel', VsCarousel)
Vue.component('vs-slider', VsSlider)
Vue.component('vs-accordion', VsAccordion)
Vue.component('vs-affix', VsAffix)
Vue.component('vs-aside', VsAside)
Vue.component('vs-checkbox-btn', VsCheckboxBtn)
Vue.component('vs-checkbox-group', VsCheckboxGroup)
Vue.component('vs-datepicker', VsDatepicker)
Vue.component('vs-dropdown', VsDropdown)
Vue.component('vs-modal', VsModal)
Vue.component('vs-option', VsOption)
Vue.component('vs-panel', VsPanel)
Vue.component('vs-popover', VsPopover)
Vue.component('vs-progressbar', VsProgressbar)
Vue.component('vs-radio-group', VsRadioGroup)
Vue.component('vs-radio-btn', VsRadioBtn)
Vue.component('vs-select', VsSelect)
Vue.component('vs-tab', VsTab)
Vue.component('vs-tabset', VsTabset)
Vue.component('vs-tooltip', VsTooltip)
Vue.component('vs-typeahead', VsTypeahead)
Vue.component('vs-navbar', VsNavbar)
Vue.component('vs-spinner', VsSpinner)


import RdWidget from '@/components/RdWidget.vue'
import RdWidgetHeader from '@/components/RdWidgetHeader.vue'
import RdWidgetBody from '@/components/RdWidgetBody.vue'
import RdWidgetFooter from '@/components/RdWidgetFooter.vue'
import RdSidebar from '@/components/RdSidebar.vue'
import RdHeader from '@/components/RdHeader.vue'

Vue.component('rd-widget', RdWidget)
Vue.component('rd-widget-header', RdWidgetHeader)
Vue.component('rd-widget-body', RdWidgetBody)
Vue.component('rd-widget-footer', RdWidgetFooter)
Vue.component('rd-sidebar', RdSidebar)
Vue.component('rd-header', RdHeader)

import MyTabs from '@/components/MyTabs.vue'
import MyNavbar from '@/components/MyNavbar.vue'
import MyTable from '@/components/MyTable.vue'
import MyTableAuth from '@/components/MyTableAuth.vue'

Vue.component('my-tabs', MyTabs)
Vue.component('my-navbar', MyNavbar)
Vue.component('my-table', MyTable)
Vue.component('my-auth-table', MyTableAuth)

import vueToastr from 'vue-toastr';
require('vue-toastr/src/vue-toastr.scss');
Vue.use(vueToastr);
Vue.component('vue-toastr',vueToastr);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
})
