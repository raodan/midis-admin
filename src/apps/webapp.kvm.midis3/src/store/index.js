import Vue from 'vue'
import Vuex from 'vuex'
import "babel-polyfill"

Vue.use(Vuex)


const store=new Vuex.Store({
  state: {
    kvmCfg: {

    }
  },
  mutations: {
    CHANGE_KVMCFG (state, res) {
      
      state.kvmCfg = res
    }
  },
  actions: {
    change_kvmcfg (context, res) {
      context.commit('CHANGE_KVMCFG', res)
    }
  },
  getters: {
    getkvmcfg: state => {
      return state.kvmCfg
    }
  }
	
})


export default store