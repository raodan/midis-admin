import Vue from 'vue'
import ebus from 'libs/ebus.js'
import * as utils from 'libs/utils.js'

Vue.config.devtools = true
Vue.config.debug = true

import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.use(Mint)

import 'font-awesome/css/font-awesome.min.css'
import 'external/css/octicons.css'

//多语言
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
Vue.config.lang = function() {
  let args = utils.getQueryStringArgs()
  if (args && args['lang']) {
    return args['lang']
  } else {
    return 'en'
  }
}()
import locales from 'locales/global.js'
Object.keys(locales).forEach(function(lang) {
  Vue.locale(lang, locales[lang])
})

//http
import Resource from 'vue-resource'
Vue.use(Resource)

Vue.http.interceptors.push({
  request: function(request) {
    return request
  },
  response: function(response) {
    if ((response.status === 401)
      || (!response.ok)) {
      ebus.emit('lose-connection', response.statusText)
    }    
    return response
  }
})

//component
import MyTree2 from 'components/MyTree2.vue'

Vue.component('my-tree2', MyTree2)

import AppView from './App.vue'

new Vue({
  el: '#root',
  components: {
    'app-view': AppView
  }
})