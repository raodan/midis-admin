export default {
  en: {
    webapp: {
      split: 'Split',
      clear: 'Clear',
      areaName: 'Area name',
      currMode: 'Current mode',
      toolview: 'Show tool',
      save2server: 'Save to server',
      nodefault: '\'Default\' or \'默认\' is forbidden'
    },
    toolview: {
      modes: 'Show modes',
      areas: 'Show areas'
    },
    list: {
      name: 'Name',
      areaCnt: 'Area count',
      index: 'Index',
      vId: 'Vitual Topo ID',
    }
  },
  cn: {
    webapp: {
      split: '快速分屏',
      clear: '清除全部区域',
      areaName: '区域名称',
      currMode: '当前模式',
      toolview: '工具视图',
      save2server: '保存到服务器',
      nodefault: '禁止输入\'默认\'、\'Default\' '
    },
    toolview: {
      modes: '模式视图',
      areas: '区域视图'
    },
    list: {
      name: '名称',
      areaCnt: '区域个数',
      index: '序号',
      vId: '虚拟TopoID',
    }
  }  
}