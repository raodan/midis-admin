

import enAuth from './enAuth'
import enKvm from './enKvm'
import enCommon from './enCommon'
import enLcd from './enLcd'
import enCode from './enCode'
import enMcnCode from './enMcnCode'
import enError from './enError'

export const message = {
  //菜单
  system: 'System Manage',
  syssetting: 'System Setting',
  sysstorage: 'System Storage',
  runninginfo: 'Running Info',
  netsetting: 'Net Setting',

  audio: 'License Settings',
  video: 'Monitor System',

  media: 'Media Manage',
  inputdevsetting: 'Inputdev Setting',
  inputchannelsetting: 'Inputchannel Setting',
  outputchannelsetting: 'Outputchannel Setting',
  outputdevsetting: 'Outputdev Setting',
  multicast: 'multicast',

  control: 'Media Settings',
  maintain: 'Cascade Settings',
  kvm: 'Center Control Manage',
  comsetting: 'COM setting',
  irsetting: 'IR setting',
  kvmDetail: 'KVM',

  debug: 'Debug',
  packetcapture: 'PacketCapture',
  webdav: 'webdav',
  swagger: 'swagger',


  homepage: 'home page',
  help: 'HELP',
  shortcut: 'shortcut',
  translate: '中文',
  save: 'save',
  refresh: 'refresh',
  search: 'search',
  add: 'add',
  name: 'name',
  type: 'type',
  shutdown: 'Shutdown',
  reboot: 'Reboot',
  rebooting: 'Restart...',
  success: 'Success',
  addSuccess: 'Add Success',
  delSuccess: 'Delete Success',
  updateSuccess: 'Update Success',
  fail: 'Fail',
  fastOperation: 'Fast Operation',
  operation: 'operation',
  guid: 'Guid',
  language: 'en',
  ok: 'OK',
  cancel: 'cancel',
  upload: 'Upload',
  youAreLost: 'you Are Lost',
  pageNotExist: 'page Not Exist',
  takeMeHome: 'Back',
  backup: 'Restore factory setting',
  tip: 'tip',
  //登录相关
  login: 'login',
  username: 'username',
  password: 'password',
  oldpassword: 'old password',
  newpassword: 'newpassword',
  confirmpassword: 'confirm password',
  signout: 'signout',
  rememberpwd: 'remember password',
  loginPage: 'Login Page',
  changePwd: 'Change Password',

  areyousure: 'Are you sure?',
  willbeshutdown: 'The device will be shutdown, are you sure?',
  uploading: 'uploading...',
  willbeRestart: 'The device will be restart',
  import: 'import',
  export: 'export',
  send: 'send',

  yes: 'Yes',
  no: 'No',
  sub: 'Sub',
  unSub: 'UnSub',
  audio: 'audio',
  video: 'video',
  timeout: 'time out',
  formatErr: '格式错误',

  navigation: 'navigation',
  dashboard: 'dashboard',
  home: 'Home',
  prevPage: 'PrevPage',
  nextPage: 'PrevPage',
  noChange: 'Not Changed',
  back: 'back',
  edit: 'edit',
  

  auth: enAuth,
  kvm: enKvm,
  common: enCommon,
  lcd: enLcd,
  code: enCode,
  error: enError,
  mcnCode: enMcnCode,
}
