export default {
  debugOn: process.env.NODE_ENV != 'production',
  ctServerPort: 9080,
  ctServerIp: '192.168.21.100',
  // ctServerIp: '192.168.21.5',
  // ctServerIp: '192.168.30.107',
  // ctServerIp: '10.10.37.3',
}