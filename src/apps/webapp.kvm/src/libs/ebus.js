//event bus
import Vue from 'vue'

var bus = new Vue()

export default {
  emit (event, ...args) {
    bus.$emit(event, ...args)
  },
  on (event, cb) {
    bus.$on(event, cb)
  },
  off (event) {
    bus.$off(event)
  },
  once (event, cb) {
    bus.$once(event, cb)
  }
}