import {codeMsg} from '@/libs/api.js'

export default {
  e (ctx, title, msg) {
    ctx.$toastr.e(msg, title)
  },

  s (ctx, title, msg) {
    ctx.$toastr.s(msg, title)
  },

  w (ctx, title, msg) {
    ctx.$toastr.w(msg, title)
  },

  i (ctx, title, msg) {
    ctx.$toastr.i(msg, title)
  },

  code (ctx, data) {
    let msg = codeMsg(ctx, data)
    ctx.$toastr.e(msg, ctx.$t('message.common.fail'))
  },
  mcnCode (ctx, res) {
    var message = ctx.$t('message.error.unknown')
    var parsedCode = typeof(res.code) == 'number'? res.code.toString().replace('-', 'n') : res
    message = ctx.$te('message.mcnCode.' + parsedCode) ? ctx.$t('message.mcnCode.' + parsedCode) : message
    ctx.$toastr.e(message, ctx.$t('message.common.fail'))
  }
}