import _ from 'lodash'
export function List(ctx) {
  var list = [   
    {
      field: 'license',
      width: '20%'
    },
    {
      field: 'diskFreeSpace',
      width: '20%'
    },
    {
      field: 'cpuRatio',
      width: '20%'
    },
    {
      field: 'memoryRatio',
      width: '20%'
    }
  ]


  _.forEach(list, (item) => {
    let keyPath = 'list.' + item.field
    item.name = ctx.$t(keyPath)
  })

  return list
}
