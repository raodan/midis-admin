import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: 'auto'
  }, {
    field: 'name',
    width: 'auto'
  }, {
    field: 'ipaddr',
    width: 'auto'
  }, {
    field: 'online',
    width: 'auto',
    show: function(item) {
      if (typeof item.online !== 'undefined') {
        if (item.online === 1) {
          return ctx.$t('local.online')
        } else {
          return ctx.$t('local.offline')
        }
      } else {
        return ctx.$t('local.manualAdd')
      }
    }
  }, {
    field: 'tag',
    width: 'auto'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}