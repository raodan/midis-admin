import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'upCallCount',
    value: ''
  }, {
    field: 'upCallBand',
    value: ''
  }, {
    field: 'downCallCount',
    value: ''
  }, {
    field: 'downCallBand',
    value: ''  
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}