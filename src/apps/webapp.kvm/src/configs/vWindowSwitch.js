import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'lcdName',
    width: '10%'
  }, {
    field: 'model',
    width: '10%',
    show: function (item) {
      if(!item.modeName){
        return ctx.$t('list.default')
      }else{
        return item.modeName
      }
    }
  }, {
    field: 'area',
    width: '10%',
    show: function (item) {
      if(!item.areaName){
        return ctx.$t('list.default')
      }else{
        return item.areaName
      }
    }
  }, {
    field: 'windowId',
    width: '15%'
  },   {
    field: 'srcSetId',
    width: '15%'
  }, {
    field: 'srcName',
    width: '15%',
    show: function (item) {
      if(item.srcSetId && !item.srcName){
        return ctx.$t('list.notfound')
      }else{
        return item.srcName
      }
    }
  }, {
    field: 'enable',
    width: '15%',
    show: function (item) {
      if (item.enable == 1 && item.srcSetId != '') {
        return ctx.$t('common.yes')
      } else if (item.enable == 0 && item.srcSetId != '') {
        return ctx.$t('common.no')
      }else  {
        return ctx.$t('list.stateNoEffect')
      }
    }
  }, {
    field: 'interval',
    width: '15%',
    show: function (item) {
      if (item.srcSetId == '') {
        return ctx.$t('list.stateNoEffect')
      } else{
        return item.interval
      }
    }
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}