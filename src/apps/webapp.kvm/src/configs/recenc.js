import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: '5%'
  }, {
    field: 'name',
    width: '20%'
  },{
    field: 'ipaddr',
    width: '20%'
  }, {
    field: 'runState',
    width: '20%',
    show: function(item) {
      if (!item.runState) {
        return ctx.$t('common.unknown')
      } else {
        return ctx.$t('local.' + item.runState)
      }
    }
  }, 
  // {
  //   field: 'recState',
  //   width: '10%',
  //   show: function(item) {
  //     if (!item.recState) {
  //       return ctx.$t('common.unknown')
  //     } else {
  //       return ctx.$t('local.' + item.recState)
  //     }      
  //   }
  // }
]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}