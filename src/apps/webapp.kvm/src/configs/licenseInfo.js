import _ from 'lodash'

export function List(ctx) {
  function showValid(item, column) {
    return column.value < 0 ? ctx.$t('common.invalid') : column.value
  }

  function showEnable(item, column) {
    return column.value === 0 ? ctx.$t('common.disable') : ctx.$t('common.enable')
  }

  function showLimited(item, column) {
    return column.value === '' ? ctx.$t('common.unlimited') : column.value
  }

  function showLimit(item, column) {
    if (column.value === -1) {
      return ctx.$t('common.nosupport')
    } else if (column.value === 0) {
      return ctx.$t('common.nolimit')
    } else {
      return column.value
    }
  }

  function showSupport(item, column) {
    if (column.value === -1) {
      return ctx.$t('common.nosupport')
    } else if (column.value === 0) {
      return ctx.$t('common.nolimit')
    } else {
      return ctx.$t('common.support')
    }
  }

  var list = [{
    field: 'licensor',
    columns: [{
      value: ''
    }, {
      value: ''
    }, {
      value: ''
    }]
  }, {
    field: 'authTime',
    columns: [{
      value: ''
    }, {
      value: ''
    }, {
      value: ''
    }]
  }, {
    field: 'projectNo',
    columns: [{
      value: ''
    }, {
      value: ''
    }, {
      value: ''
    }]
  }, {
    field: 'projectDesc',
    columns: [{
      value: ''
    }, {
      value: ''
    }, {
      value: ''
    }]
  }, {
    field: 'maxUIConnections',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxDecOfLcdWall',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxListCntOfIpc',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxRecordChannel',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxPlayChannel',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxListCntOfSoftEnc',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxChannelOfSuperHdScreen',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxListCntOfProxy',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxListCntOfMtsrc',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxConnCntOfKVM',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxListCntOfCapStreamMediaDev',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxChannelOfCascade',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfFounder108es',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfFounder108ds',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfFounderWzhy',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfFounder102e',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfFounder102e4k',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfFounder302e',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfMcn102e4k',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfMcn102e',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfMcn100d4k',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfMcn102d',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxNumOfRecEnc',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'maxListCntOfRProxy',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showValid
    }, {
      value: 0,
      show: showLimit
    }]
  }, {
    field: 'enableCluster',
    columns: [{
      value: ''
    }, {
      value: 0,
      show: showEnable
    }, {
      value: 0,
      show: showSupport
    }]
  }, {
    field: 'trialBeginTime',
    columns: [{
      value: ''
    }, {
      value: '',
      show: showLimited
    }, {
      value: ''
    }]
  }, {
    field: 'trialEndTime',
    columns: [{
      value: ''
    }, {
      value: '',
      show: showLimited
    }, {
      value: ''
    }]
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.columns[0].value = ctx.$t(keypath)
  })

  return list 
}