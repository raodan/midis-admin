
import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'topoId',
    width: '15%'
  }, {
    field: 'ipaddr',
    width: '20%'
  }, {
    field: 'devType',
    width: '20%'
  }, {
    field: 'status',
    width: '20%'
  }, {
    field: 'link',
    width: '10%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 

}