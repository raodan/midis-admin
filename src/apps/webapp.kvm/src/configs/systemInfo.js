import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'macaddr',
    value: ''
  }, {
    field: 'devSN',
    value: ''
  }, {
    field: 'devModel',
    value: ''
  }, {
    field: 'softwareVersion',
    value: ''
  }, {
    field: 'buildDate',
    value: ''  
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}