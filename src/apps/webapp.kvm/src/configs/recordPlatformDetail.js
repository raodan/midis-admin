import _ from 'lodash'
export function List1(ctx) {
  var list = [        
    {
      field: 'version',
      value: ''
    },
    {
      field: 'license',
      value: ''    
    },
    {
      field: 'diskSpace',
      value: ''    
    },
    {
      field: 'diskFreeSpace',
      value: ''    
    },
    {
      field: 'cpuModel',
      value: ''
    },
    {
      field: 'cpuRatio',
      value: ''
    },
    {
      field: 'memory',
      value: ''   
    },
    {
      field: 'memoryRatio',
      value: ''   
    },
    {
      field: 'system',
      value: ''   
    }
  ] 

  _.forEach(list, (item) => {
    let keyPath = 'list.' + item.field
    item.name = ctx.$t(keyPath)
  })

  return list
}
