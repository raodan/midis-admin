import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'callId',
    width: 'auto'
  }, {
    field: 'name',
    width: 'auto'
  }, {
    field: 'streamSrc',
    width: 'auto',
    show: function(item) {
      var found = _.find(ctx.streamsrc.list, function(src) {
        return ((src.topoId === item.srcTopoId) 
          && (src.portName === item.srcPortName))
      })      
      if (found) {
        return found.name
      } else {
        return ctx.$t('error.invalidStream') + '[' + item.srcTopoId + '-' + item.srcPortName + ']'
      }
    }    
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}