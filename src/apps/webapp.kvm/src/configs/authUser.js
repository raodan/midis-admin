import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'username',
    width: '15%'
  }, {
    field: 'role',
    width: '20%',
    show: function(item) {
      if (item.role.startsWith('web')) {
        return ctx.$t(`role.${item.role}`)
      } else {
        return item.role
      }
    }
  }, {
    field: 'status',
    width: '15%',
    show: function(item) {
      if (item.spStatus === 'enable') {
        return ctx.$t('common.toBeEnable')
      } else if (item.spStatus === 'disable') {
        return ctx.$t('common.toBeDisable')
      } else {
        if (item.enable) {
          return ctx.$t('common.enable')
        } else {
          return ctx.$t('common.disable')
        }        
      }
    }
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}