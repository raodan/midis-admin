import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import manage from '@/views/manage'
import login from '@/views/Login'
import kvm from '@/views/kvm/kvm'
import seatCfg from '@/views/kvm/seatCfg'
import srcList from '@/views/kvm/srcList'
import shortcuts from '@/views/kvm/shortcuts'

Vue.use(Router)

export default new Router({
  routes: [{
      path: "/login",
      component: login
    }, {
      path: '/',
      name: 'manage',
      component: manage,
      children: [{
          path: '/',
          name: '/',
          redirect: '/kvm',
          component: kvm,
        },
        {
          path: 'kvm',
          name: 'kvm',
          component: kvm,
          children: [{
              path: '/',
              name: '/',
              redirect: 'seatCfg',
              component: seatCfg,
            },
            {
              path: 'seatCfg',
              name: 'seatCfg',
              component: seatCfg,
            },
            {
              path: 'srcList',
              name: 'srcList',
              component: srcList,
            },
            {
              path: 'shortcuts',
              name: 'shortcuts',
              component: shortcuts,
            }
          ]
        }
      ]
    },
    {
      path: 'hello',
      name: 'hello',
      component: HelloWorld
    },
    {
      path: '*',
      name: 'any',
      component: login
    }
  ]
})
