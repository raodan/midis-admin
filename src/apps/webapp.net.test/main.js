import Vue from 'vue'
import ebus from 'libs/ebus.js'
import * as utils from 'libs/utils.js'

Vue.config.devtools = true
Vue.config.debug = true

import 'bootstrap/dist/css/bootstrap.min.css'
import 'external/css/rdash.min.css'
import 'sweetalert/dist/sweetalert.css'

import isValidIp from "libs/utils.js"
import api from "libs/api.js"
import {http} from 'share/common/api.js'
import async from 'async'
import swal from 'sweetalert'

import{
  modal as VsModal
} from 'vue-strap'

Vue.component('vs-modal', VsModal)


//http
import Resource from 'vue-resource'
Vue.use(Resource)

Vue.http.interceptors.push({
  request: function(request) {
    return request
  },
  response: function(response) {
    return response
  }
})

//多语言
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
Vue.config.lang = function() {
  let args = utils.getQueryStringArgs()
  if (args && args['lang']) {
    return args['lang']
  } else {
    return 'en'
  }
}()
import locales from 'locales/global.js'
Object.keys(locales).forEach(function(lang) {
  Vue.locale(lang, locales[lang])
})

//validator
import Validator from 'vue-validator'
Vue.use(Validator)
Vue.validator('ip', utils.checkValidIp)

import AppView from './App.vue'

new Vue({
  el: '#root',
  components: {
    'app-view': AppView
  }
})