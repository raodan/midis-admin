export default {
  en: {
    nettest: {
      addrOfTheClient: 'Client address',
      netTest: 'Network testing',
      serverAddr: 'Server address',
      serverIp: 'Server IP',
      clientIp: 'Client IP',
      testType: 'Test type',
      testTime: 'Test time',
      testBandwidth: 'Test the bandwidth',
      startService: 'Start the service',
      startTest: 'Start testing',
      allStop: 'Stop All',
      plzInputAgain: 'Please input again',
      serviceNotIn: 'The Server IP is not in the list',
      clientNotIn: 'The Client IP is not in the list',
      underTest: 'Under testing ...',
      testResult: 'Test results',
      errorSerAddr: 'Invalid server address',
      errorCliAddr: 'Invalid client address'
    }
  },
  cn: {
    nettest: {
      addrOfTheClient: '客户端地址',
      netTest: '网络测试',
      serverAddr: '服务地址',
      serverIp: '服务器IP',
      clientIp: '客户端IP',
      testType: '测试类型',
      testTime: '测试时长',
      testBandwidth: '测试带宽',
      startService: '启动服务',
      startTest: '开始测试',
      allStop: '全部停止',
      plzInputAgain: '请重新输入',
      serviceNotIn: '服务IP不在列表',
      clientNotIn: '客服端IP不在列表',
      underTest: '测试中...',
      testResult: '测试结果',
      errorSerAddr: '无效服务地址',
      errorCliAddr: '无效的客户端地址'
    }
  }  
}