export default{
  en:{
    manufacture:{
      Mmodels: 'Manufacturers Model',
      manufacturers: 'Manufacturers',
      name: 'Model',
      version: 'Version'
    }
  },
  cn:{
    manufacture:{
      Mmodels: '厂家型号',
      manufacturers: '厂家',
      name: '型号规格',
      version: '版本'
    }
  }
}