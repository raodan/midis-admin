export default {
  background: {
    image: "",
    bgColor: '#4095BF',
    opacity: 50,
    show: 0
  },
  text: {
    color: "#1133ff",
    align: "center",
    alignV: "center",
    fontSize: 100,
    fontName: "宋体",
    content: "东微欢迎你！",
    show: 1
  },
  scroll: {
    enable: 0,
    textWidth: 0,
    interval: 100,
  },
  showRect: {
    left: 0,
    top: 0,
    right: 1280,
    bottom: 100
  }
}