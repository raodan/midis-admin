export default {
  en: {
    webapp: {
      deviceIp: 'Device IP',
      subtitleOfWindow: 'Subtitles Of Window',
      subtitleOfVpOut: 'Subtitles Of VpOut',
      fontsize: 'Font Size',
      color: 'Color',
      type: 'Type',
      content: 'Content',
      preview: 'Preview'
    }
  },
  cn: {
    webapp: {
      deviceIp: '设备IP',
      subtitleOfWindow: '窗口字幕',
      subtitleOfVpOut: '输出字幕',
      fontsize: '字号',
      color: '颜色',
      type: '类型',
      content: '内容',
      preview: '预览'
    }
  }  
}