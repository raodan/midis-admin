import Vue from 'vue'
import ebus from 'libs/ebus.js'
import * as utils from 'libs/utils.js'

Vue.config.devtools = true
Vue.config.debug = true

import 'bootstrap/dist/css/bootstrap.min.css'
import 'external/css/rdash.min.css'
import 'sweetalert/dist/sweetalert.css'

//多语言
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
Vue.config.lang = function() {
  let args = utils.getQueryStringArgs()
  if (args && args['lang']) {
    return args['lang']
  } else {
    return 'cn'
  }
}()
import locales from 'locales/global.js'
Object.keys(locales).forEach(function(lang) {
  Vue.locale(lang, locales[lang])
})

//http
import Resource from 'vue-resource'
Vue.use(Resource)

Vue.http.interceptors.push({
  request: function(request) {
    return request
  },
  response: function(response) {
    return response
  }
})

//validator
import Validator from 'vue-validator'
Vue.use(Validator)

Vue.validator('ip', utils.checkValidIp)

//component
import MyNavbar from 'components/MyNavbar.vue'
import MyTable from 'components/MyTable.vue'

Vue.component('my-navbar', MyNavbar)
Vue.component('my-table', MyTable)

import {
  modal as VsModal,
} from 'vue-strap'
Vue.component('vs-modal', VsModal)

import { 
  Slider as VcSlider,
  Compact as VcCompact,
} from 'vue-color'
Vue.component('vc-compact', VcCompact)
Vue.component('vc-slider', VcSlider)

import 'vue-toastr/dist/vue-toastr.min.css'
import vueToastr from 'vue-toastr'
Vue.component('vue-toastr', vueToastr)

import MySubtitleDetail from './MySubtitleDetail.vue'
Vue.component('my-subtitle-detail', MySubtitleDetail)

import AppView from './App.vue'

new Vue({
  el: '#root',
  components: {
    'app-view': AppView
  },
  methods: {
    initToast () {
      var toast = this.$refs.toast

      toast.defaultTimeout = 3000
      toast.defaultType = "error"
      toast.defaultPosition = "toast-bottom-right"
    }
  },
  ready () {
    this.initToast()
  }
})