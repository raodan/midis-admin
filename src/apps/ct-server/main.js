import _ from 'lodash'
import Vue from 'vue'
import ebus from 'libs/ebus.js'
import toast from 'libs/toast.js'
import debugCfg from 'share/debugCfg.js'
import settings from 'share/server/settings.js'
import * as utils from 'libs/utils.js'

// require('./mock.js');//引入mock

Vue.config.devtools = true
Vue.config.debug = true

import 'babel-polyfill'

// 引入样式
import 'bootstrap/dist/css/bootstrap.min.css'
import 'external/css/rdash.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'sweetalert/dist/sweetalert.css'
import 'external/css/octicons.css'
import './main.css'


//多语言
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
Vue.config.lang = function() {
  if (debugCfg.debugOn
    && settings.lang !== '') {
    return settings.lang
  }
  let lang = navigator.language.slice(-2).toLowerCase()
  //if (['cn', 'en'].includes(lang)) {
  if (_.includes(['cn', 'en'], lang)) {
    return lang
  } else {
    return 'en'
  }
}()
import locales from 'locales/global.js'
Object.keys(locales).forEach(function(lang) {
  Vue.locale(lang, locales[lang])
})

//http
import Resource from 'vue-resource'
Vue.use(Resource)


// Vue.config.baseURL = '/api/';

Vue.http.interceptors.push({
  request: function(request) {
    //规避数组参数不按JSON发送的问题
    if (Object.getPrototypeOf(request.data) == Array.prototype) {
      request.data = JSON.stringify(request.data)
    }
    //utils.printObj(request)
    return request
  },
  response: function(response) {
    //utils.printObj(response)

    if (response.status === 401) {
      ebus.emit('lose-connection', response.statusText)
    }
    return response
  }
})

//validator
import Validator from 'vue-validator'
Vue.use(Validator)

Vue.validator('strong', utils.checkStrong)
Vue.validator('ipn', utils.isValidIp)  //允许为空
Vue.validator('ip', utils.checkValidIp)
Vue.validator('mac', utils.checkValidMac)
Vue.validator('port', utils.isValidPort)
Vue.validator('multiIp', utils.checkValidMultiIp)
Vue.validator('name', utils.isValidName)
Vue.validator('id', utils.isValidId)
Vue.validator('rtspurl', utils.isValidRtspUrl)
Vue.validator('callnumber', utils.isValidCallNumber)
Vue.validator('special', utils.hasNoSpacial)

//router
import Router from 'vue-router'
Vue.use(Router)
var router = new Router({
  history: false,
  saveScrollPosition: true
})

import LoginView from './views/Login.vue'
import DashboardView from './views/Dashboard.vue'
import SystemView from './views/System.vue'
import LicenseView from './views/License.vue'
import AuthMngView from './views/AuthMng.vue'
import DevMngView from './views/DevMng.vue'
import LogMngView from './views/LogMng.vue'
import MultimediaView from './views/Multimedia.vue'
import RecordMngView from './views/RecordMng.vue'
import RecordPlatformView from './views/RecordPlatform.vue'
import RecordPlatRoomView from './views/RecordPlatRoom.vue'
import ScriptPluginView from './views/ScriptPlugin.vue'
import MonitorSystemView from './views/MonitorSystem.vue'
import PcShareView from './views/PcShare.vue'
import WebAppView from './views/WebApp.vue'
import NotFoundView from './views/NotFound.vue'

import DdisDevList from './views/DdisDevList.vue'
import TopoDevList from './views/TopoDevList.vue'
import TopoBindsEdit from './views/TopoBindsEdit.vue'
import MonitorPlat from './views/MonitorPlat.vue'
import CapStreamResolution from './views/CapStreamResolution.vue'
import IpcTree from './views/MonitorTree.vue'
import SrcIpcam from './views/SrcIpcam.vue'
import IpcamVendor from './views/IpcamVendor.vue'
import LogLogin from './views/LogLogin.vue'
import LogOperation from './views/LogOperation.vue'
import LogAlarm from './views/LogAlarm.vue'
import SrcSoftEnc from './views/SrcSoftEnc.vue'
import SrcSuperHdScreen from './views/SrcSuperHdScreen.vue'
import LcdWindow from './views/LcdWindow.vue'
import VidLayout from './views/VidLayout.vue'
import VLayout from './views/VLayout.vue'
import VSwitch from './views/VSwitch.vue'
import VSwitchDetail from './views/VSwitchDetail.vue'
import VLayoutWindow from './views/VLayoutWindow.vue'
import VCodestreamSet from './views/VCodestreamSet.vue'
import VWindowSwitch from './views/VWindowSwitch.vue'
import VSrcSetsDetail from './views/VSrcSetsDetail.vue'
import VLcdPreset from './views/VLcdPreset.vue'
import mcuSetting from './views/mcuSetting.vue'//暂改
import RecordRoom from './views/RecordRoom.vue'
import AuthUser from './views/AuthUser.vue'
import AuthSecPolicy from './views/AuthSecPolicy.vue'

import CascadeMngView from './views/CascadeMng.vue'
import CascadeLDev from './views/CascadeLDev.vue'
import CascadeLShareSrc from './views/CascadeLShareSrc.vue'
import CascadeRDev from './views/CascadeRDev.vue'

import RecEnc from './views/RecEnc.vue'
import RecEncOperateDetail from './views/RecordEncOperateDetail.vue' 
import SetSrcDetail from './views/SetSrcDetail.vue'
import RecordPlatformChild from './views/RecordPlatformChild.vue'
import PcList from './views/PcList.vue'

import ScenesMngView from './views/ScenesMng.vue'
import ScenesList from './views/ScenesList.vue'
import ScenesTemplateList from './views/ScenesTemplateList.vue'

router.map({
  '/login': {
    component: LoginView
  },
  '/': {
    component: DashboardView,
    auth: true,
    subRoutes: {
      '/': {
        component: SystemView,
        name: 'System'
      },
      '/license': {
        component: LicenseView,
        name: 'License'
      },
      '/monitor': {
        component: MonitorSystemView,
        name: 'Monitor System',
        subRoutes: {
          '/': {
            component: SrcIpcam,
          },
          '/vendor': {
            component: IpcamVendor,
          },
          '/plat': {
            component: MonitorPlat,
          },
          '/ipctree': {
            component: IpcTree,
          },
          '/capStremResolution': {
            component: CapStreamResolution,
          }
        }
      },
      '/pcshare': {
        component: PcShareView,
        name: 'PC Share',
        subRoutes: {
          '/': {
            component: SrcSoftEnc,
          },
          '/pclist': {
            component: PcList,
          },
          '/superhdscreen': {
            component: SrcSuperHdScreen
          }
        }
      },
      '/multimedia': {
        component: MultimediaView,
        name: 'Multimedia',
        subRoutes: {
          '/': {
            component: LcdWindow,
          },
          '/vidlayout': {
            component: VidLayout,
          },
          '/vlayout': {
            component: VLayout,
          },
          '/vswitch': {
            component: VSwitch,
          },
          '/vswitch-detail': {
            component: VSwitchDetail,
          },
          '/vlayout-window': {
            component: VLayoutWindow,
          },
          '/vcodestream-set': {
            component: VCodestreamSet,
          },
          '/vwindow-switch': {
            component: VWindowSwitch,
          },
          '/vsrcsets-detail': {
            component: VSrcSetsDetail,
          },
          '/vlcd-preset': {
            component: VLcdPreset,
          },
          '/mcu-setting': {  //暂改
            component: mcuSetting
          }
        }
      },
      '/recordmng': {
        component: RecordMngView,
        name: 'Record',
        subRoutes: {
          '/': {
            component: RecordRoom,
          },
          '/recenc': {
            component: RecEnc,
          },
          '/recenc-operation-detail': {
            component: RecEncOperateDetail,
          },
          '/recenc-platform': {
            component: RecordPlatformView,
          },
          '/recenc-platroom': {
            component: RecordPlatRoomView,
          },
          '/record-platroom-setsrc': {
            component: SetSrcDetail,
          },
          '/record-platform-child': {
            component: RecordPlatformChild,
          }
        }
      },
      '/cascade': {
        component: CascadeMngView,
        name: 'Cascade',
        subRoutes: {
          '/': {
            component: CascadeLDev,
          },
          '/lsharesrc': {
            component: CascadeLShareSrc,
          },
          'rdev': {
            component: CascadeRDev,
          }
        }
      },
      '/devmng': {
        component: DevMngView,
        name: 'Device',
        subRoutes: {
          '/': {
            component: DdisDevList,
          },
          '/topo': {
            component: TopoDevList,
          },
          '/topo-binds-edit': {
            component: TopoBindsEdit,
          }
        }
      },
      '/scenes': {
        component: ScenesMngView,
        name: 'Scenes',
        subRoutes: {
          '/': {
            component: ScenesList,
          },
          '/template': {
            component: ScenesTemplateList,
          },
        }
      },
      '/webapp': {
        component: WebAppView,
        name: 'WebApp'
      },
      '/scriptplugin': {
        component: ScriptPluginView,
        name: 'Script'
      },
      '/log': {
        component: LogMngView,
        name: 'Log',
        subRoutes: {
          '/': {
            component: LogLogin,
          },
          '/operation': {
            component: LogOperation,
          },
          '/alarm': {
            component: LogAlarm,
          }
        }
      },
      '/authmng': {
        component: AuthMngView,
        name: 'Auth',
        subRoutes: {
          '/': {
            component: AuthUser,
          },
          '/secpolicy': {
            component: AuthSecPolicy,
          }
        }
      }
    }
  },
  // not found handler
  '*': {
    component: NotFoundView
  }
})
router.beforeEach(function(transition) {
  if ((transition.to.auth) 
    && (!transition.to.router.app.$store.state.auth.authed)) {
    transition.redirect('/login')
  } else {
    //console.log('from:' + transition.from)
    //console.log('to:' + transition.to)
    transition.next()
  }
})

import {
  alert as VsAlert,
  carousel as VsCarousel,
  slider as VsSlider,
  accordion as VsAccordion,
  affix as VsAffix,
  aside as VsAside,
  checkboxBtn as VsCheckboxBtn,
  checkboxGroup as VsCheckboxGroup,
  datepicker as VsDatepicker,
  dropdown as VsDropdown,
  modal as VsModal,
  option as VsOption,
  panel as VsPanel,
  popover as VsPopover,
  progressbar as VsProgressbar,
  radioGroup as VsRadioGroup,
  radioBtn as VsRadioBtn,
  select as VsSelect,
  tab as VsTab,
  tabset as VsTabset,
  tooltip as VsTooltip,
  typeahead as VsTypeahead,
  navbar as VsNavbar,
  spinner as VsSpinner
} from 'vue-strap'

Vue.component('vs-alert', VsAlert)
Vue.component('vs-carousel', VsCarousel)
Vue.component('vs-slider', VsSlider)
Vue.component('vs-accordion', VsAccordion)
Vue.component('vs-affix', VsAffix)
Vue.component('vs-aside', VsAside)
Vue.component('vs-checkbox-btn', VsCheckboxBtn)
Vue.component('vs-checkbox-group', VsCheckboxGroup)
Vue.component('vs-datepicker', VsDatepicker)
Vue.component('vs-dropdown', VsDropdown)
Vue.component('vs-modal', VsModal)
Vue.component('vs-option', VsOption)
Vue.component('vs-panel', VsPanel)
Vue.component('vs-popover', VsPopover)
Vue.component('vs-progressbar', VsProgressbar)
Vue.component('vs-radio-group', VsRadioGroup)
Vue.component('vs-radio-btn', VsRadioBtn)
Vue.component('vs-select', VsSelect)
Vue.component('vs-tab', VsTab)
Vue.component('vs-tabset', VsTabset)
Vue.component('vs-tooltip', VsTooltip)
Vue.component('vs-typeahead', VsTypeahead)
Vue.component('vs-navbar', VsNavbar)
Vue.component('vs-spinner', VsSpinner)

import { 
  Compact as VcCompact,
  Material as VcMaterial,
  Slider as VcSlider,
  Swatches as VcSwatches,
  Photoshop as VcPhotoshop,
  Sketch as VcSketch,
  Chrome as VcChrome
} from 'vue-color'

Vue.component('vc-compact', VcCompact)
Vue.component('vc-material', VcMaterial)
Vue.component('vc-slider', VcSlider)
Vue.component('vc-swatches', VcSwatches)
Vue.component('vc-photoshop', VcPhotoshop)
Vue.component('vc-sketch', VcSketch)
Vue.component('vc-chrome', VcChrome)

import RdWidget from 'components/RdWidget.vue'
import RdWidgetHeader from 'components/RdWidgetHeader.vue'
import RdWidgetBody from 'components/RdWidgetBody.vue'
import RdWidgetFooter from 'components/RdWidgetFooter.vue'
import RdSidebar from 'components/RdSidebar.vue'
import RdHeader from 'components/RdHeader.vue'

Vue.component('rd-widget', RdWidget)
Vue.component('rd-widget-header', RdWidgetHeader)
Vue.component('rd-widget-body', RdWidgetBody)
Vue.component('rd-widget-footer', RdWidgetFooter)
Vue.component('rd-sidebar', RdSidebar)
Vue.component('rd-header', RdHeader)

import MySignIn from 'components/MySignIn.vue'
import MyLanguageSelect from 'components/MyLanguageSelect.vue'
import MyList from 'components/MyList.vue'
import MyList2 from 'components/MyList2.vue'
import MyTable from 'components/MyTable.vue'
import MyCanSelectedTable from 'components/MyCanSelectedTable.vue'
import MyTree from 'components/MyTree.vue'
import MyTreeDragabled from 'components/MyTreeDragabled.vue'
import MyTreeObject from 'components/MyTreeObject.vue'
import MyTree1 from 'components/MyTree1.vue'
import MyNavbar from 'components/MyNavbar.vue'
import MyFileUpload from 'components/MyFileUpload.vue'
import MyTabs from 'components/MyTabs.vue'
import MyVidPreview from 'components/MyVidPreview.vue'
import MyPagination from 'components/MyPagination.vue'

Vue.component('my-sign-in', MySignIn)
Vue.component('my-language-select', MyLanguageSelect)
Vue.component('my-list', MyList)
Vue.component('my-list2', MyList2)
Vue.component('my-table', MyTable)
Vue.component('my-selected-table', MyCanSelectedTable)
Vue.component('my-tree', MyTree)
Vue.component('my-dragabled-tree', MyTreeDragabled)
Vue.component('my-tree-object', MyTreeObject)
Vue.component('my-tree1', MyTree1)
Vue.component('my-navbar', MyNavbar)
Vue.component('my-file-upload', MyFileUpload)
Vue.component('my-tabs', MyTabs)
Vue.component('my-vid-preview', MyVidPreview)
Vue.component('my-pagination', MyPagination)

import 'vue-toastr/dist/vue-toastr.min.css'
import vueToastr from 'vue-toastr'
Vue.component('vue-toastr', vueToastr)

import AppView from './App.vue'
router.start(AppView, '#root')

