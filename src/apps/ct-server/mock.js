let Mock  = require('mockjs');
let Random = Mock.Random;

let data = []//接收生成的数据

Random.extend({
    devs: function(data) {
      var devDatas = [
        {model: "decoder.hi3536.v5",name:"MCN-400D",type: "cdev.decoder"},
        // {model: "server.media.pcv2",name:"M-MD-SERVER of e-Media1.5",type:  "server.media"},
        // {model: "peripheral.lcdwall.0201",name:"lcdwall_0201",type: "peripheral"},
        // {model: "codec.founder100d",name:"FOUNDER-100D",type: "decoder"},
        {model: "edu-fd.tcn100s",name:"TCN",type: "edu-fd"},
        // {model: "virtual.switch.net512",name:"netswitch",type: "virtual.switch.net"},
        // {model: "server.media.pcv2",name:"M-MD-SERVER of e-Media1.5",type:  "server.main"},   
        // {model: "codec.founder100e",name: "FOUNDER-100E", type: "encoder"},   
        // {model: "codec.founder100es",name: "FOUNDER-100ES", type: "encoder"}, 
        // {model: "codec.founder100ds",name: "FOUNDER-100DS", type: "decoder"}, 
        // {model: "codec.founder100scr",name: "FOUNDER-100SCR", type: "decoder"}, 
        // {model: "codec.founder102e",name: "FOUNDER-102E", type: "encoder.102e"}, 
        // {model: "codec.founder102e4k",name: "FOUNDER-102E-4K", type: "encoder.102e"}, 
        {model: "codec.founder108es",name: "FOUNDER-108ES", type: "encoder"}, 
        {model: "codec.founder108ds",name: "FOUNDER-108DS", type: "codec"}, 
        {model: "codec.founder300es",name: "FOUNDER-300ES", type: "encoder"}, 
        {model: "codec.founder300ds",name: "FOUNDER-300DS", type: "decoder"}, 
        {model: "codec.founder302e",name: "FOUNDER-302E", type: "encoder.102e"}, 
        {model: "encoder.hi3531a.v1",name: "MCN-102E-4K", type: "cdev.encoder"}, 
        {model: "encoder.hi3531a.v2",name: "MCN-102E", type: "cdev.encoder"}, 
        {model: "encoder.hi3531a.v3",name: "MCN-100E-4K", type: "cdev.encoder"}, 
        {model: "encoder.hi3531a.v4",name: "MCN-100E", type: "cdev.encoder"}, 
        {model: "encoder.hi3531d.v3",name: "MCN-300E-HDMI", type: "cdev.encoder"}, 
        {model: "encoder.hi3531d.v4",name: "MCN-300E-SDI", type: "cdev.encoder"}, 
        {model: "encoder.hi3531d.v14",name: "MCN-104E", type: "cdev.encoder"}, 
        {model: "decoder.hi3531d.v3",name: "MCN-300D-HDMI", type: "cdev.decoder"}, 
        {model: "decoder.hi3531d.v4",name: "MCN-300D-SDI", type: "cdev.decoder"}, 
        {model: "decoder.hi3536.v1",name: "MCN-100D-4K", type: "cdev.decoder"}, 
        {model: "decoder.hi3536.v2",name: "MCN-102D", type: "cdev.decoder"}, 
        {model: "decoder.hi3536.v3",name: "MCN-100D", type: "cdev.decoder"}, 
        {model: "decoder.hi3536.v4",name: "MCN-110D", type: "cdev.decoder"}, 
        {model: "decoder.hi3536.v6",name: "MCN-400D-4K", type: "cdev.decoder"}, 
        {model: "codec.hi3531d.v1",name: "SKY-100E", type: "cdev.codec"}, 
        {model: "codec.hi3531d.v3",name: "FOUNDER-304E", type: "cdev.codec"}, 
        {model: "codec.hi3531d.v4",name: "FOUNDER-308E", type: "cdev.codec"}, 
        {model: "codec.hi3531d.v12",name: "ZCKD-100", type: "cdev.codec"}, 
        {model: "transcoder.hi3536.v1",name: "MCU-1090", type: "cdev.transcoder"}, 
        {model: "decoder.hi3531d.v15",name: "PTN-DEC8", type: "cdev.decoder"}, 
        {model: "encoder.hi3531d.v16",name: "PTN-ENC4", type: "cdev.encoder"},

        ]
        return this.pick(devDatas)
    }
//   devmodel: function(date) {
//       var devModels = [
//         "decoder.hi3536.v5","server.media.pcv2","peripheral.lcdwall.0201", "codec.founder100d",
//         "edu-fd.tcn100s","virtual.switch.net512"
//       ]
//       return this.pick(devModels)
//   },
//   devname: function(date) {
//     var devNames = [
//       'MCN-400D',"M-CT-SERVER of e-Media1.5","lcdwall_0201", "lcd",
//       "FOUNDER-100D","TCN"
//     ]
//     return this.pick(devNames)
// },
})



// let devs = [
//   {'devModel': "decoder.hi3536.v5",
//   'devName': "MCN-400D(2)",
//   'devSN': null,
//   'devTag': null,
//   'devType': "cdev.decoder",
//   'topoId': 2}
// ] // 定义随机值
for(let i = 900; i < 1000; i ++) { // 可自定义生成的个数
  let curdev = Random.devs()
  let template = {
    'topoId': i,
    'devTag': null,
    'devModel': curdev.model,
    'devName':  curdev.name +'(' + i + ')',
    'devType': curdev.type,
    'devSN': Random.guid()
  }
  data.push(template)
}


Mock.mock('/data/index', 'post', data)//根据模板生成模拟数据