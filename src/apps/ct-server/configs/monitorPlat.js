import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: 'auto'
  }, {
    field: 'name',
    width: 'auto'
  }, {
    field: 'username',
    width: 'auto'
  }, {
    field: 'ipaddr',
    width: 'auto'
  }, {
    field: 'port',
    width: 'auto'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}