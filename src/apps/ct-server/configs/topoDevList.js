import _ from 'lodash'

export function List(ctx) {
  var devModelsSummary = ctx.$store.state.devmng.devModelsSummary.list  
  var list = [{
    field: 'topoId',
    width: '5%'
  }, {
    field: 'devSN',
    width: '20%'
  }, {
    field: 'devName',
    width: '15%'
  }, {
    field: 'devType',
    width: '10%'
  }, {
    field: 'devModel',
    width: '15%'
  }, {
    field: 'devModelName',
    width: '15%',
    show: function(item) {
      var res = _.find(devModelsSummary, dev => {
        if(dev.devModel.includes('*')) {
         return item.devModel === dev.devModel.slice(0,dev.devModel.length-1);
        }
        return item.devModel === dev.devModel
      })
      if (res) {
        return res.modelName
      } else{
        return item.devModel
      }
    }
  }, {
    field: 'devTag',
    width: 'auto'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })
  
  return list 
}