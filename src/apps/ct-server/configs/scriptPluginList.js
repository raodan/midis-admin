import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'typeStr',
    width: 'auto'
  }, {
    field: 'pluginId',
    width: 'auto'
  }, {
    field: 'name',
    width: 'auto'
  }, {
    field: 'version',
    width: 'auto'
  }, {
    field: 'provider',
    width: 'auto'
  }, {
    field: 'tag',
    width: 'auto'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}