import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: '35%'
  }, {
    field: 'name',
    width: '35%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}