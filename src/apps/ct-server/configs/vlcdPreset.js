import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: '_id',
    width: '10%',
    name: "ID"
  }, {
    field: 'name',
    width: '20%',
    name: ctx.$t('common.name')
  }, {
    field: 'multiScreen',
    width: '20%',
    name: ctx.$t('list.multiScreen'),
    show: function(item) {      
      return item.layout.multiScreenX + 'x' + item.layout.multiScreenY
    }
  }, {
    field: 'operationMode',
    width: '20%',    
    name: ctx.$t('list.operationMode'),
    show: function(item) {
      if (item.operationMode == '') {
        return ctx.$t('list.freeMode')
      } else if (item.operationMode == 'wLayout') {
        return ctx.$t('list.layoutMode')
      } else {
        return ctx.$t('list.freeMode')
      }
    }

  }]
  return list
}