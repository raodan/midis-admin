import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'id',
    width: 'auto'
  }, {
    field: 'name',
    width: 'auto'
  }, {
    field: 'srcSetType',
    width: 'auto',
    show: function (item) {
      if(item.srcSetType=='custom'){
        return ctx.$t('list.custom')
      } else if(item.srcSetType=='monitorSrc'){
        return ctx.$t('list.monitorSrc')
      } else if(item.srcSetType=='treeSrc'){
        return ctx.$t('list.treeSrc')
      }else{
        return ctx.$t('list.streamType')
      }
      
    }
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list

}