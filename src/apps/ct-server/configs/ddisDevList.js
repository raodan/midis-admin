import _ from 'lodash'

export function List(ctx) {
  var devModelsSummary = ctx.$store.state.devmng.devModelsSummary.list  
  var list = [{
    field: 'ipaddr',
    width: '10%'
  }, {
    field: 'devModel',
    width: '15%'
  }, {
    field: 'devModelName',
    width: '15%',
    show: function(item) {
      var res = _.find(devModelsSummary, dev => {
        if(dev.devModel.includes('*')) {
        return item.devModel === dev.devModel.slice(0,dev.devModel.length-1);
        }
        return item.devModel === dev.devModel
      })
      if (res) {
        return res.modelName
      } else{
        return item.devModel 
      }
    }
  }, {
    field: 'swVersion',
    width: '10%'
  }, {
    field: 'devSN',
    width: '25%'
  }, {
    field: 'mac', 
    width: '10%'
  }, {
    field: 'link',
    width: '5%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })
  return list 
}