import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'fullscreen',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }]
  }, {
    field: 'splitScreenBy2',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 3,
        bottomHoriz: 6,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 3,
        bottomHoriz: 12,
        bottomVerti: 9
      }
    }]
  }, {
    field: 'splitScreenBy2Stretch',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 6,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }]
  }, {
    field: 'piplt',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 2,
        topVerti: 2,
        bottomHoriz: 4,
        bottomVerti: 4
      }
    }]
  }, {
    field: 'piplb',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 2,
        topVerti: 8,
        bottomHoriz: 4,
        bottomVerti: 10
      }
    }]
  }, {
    field: 'piprt',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 2,
        bottomHoriz: 10,
        bottomVerti: 4
      }
    }]
  }, {
    field: 'piprb',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 8,
        bottomHoriz: 10,
        bottomVerti: 10
      }
    }]
  }, {
    field: 'splitScreenBy3',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 3,
        bottomHoriz: 6,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 6,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }]
  }, {
    field: 'splitScreenBy4',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 6,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 6,
        bottomHoriz: 6,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 6,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }]
  }, {
    field: 'splitScreenBy6',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 8,
        bottomVerti: 8
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 4
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 4,
        bottomHoriz: 12,
        bottomVerti: 8
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 8,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 8,
        bottomHoriz: 4,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 4,
        topVerti: 8,
        bottomHoriz: 8,
        bottomVerti: 12
      }
    }]
  }, {
    field: 'splitScreenBy8',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 9,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 3
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 3,
        bottomHoriz: 12,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 6,
        bottomHoriz: 12,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 9,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 9,
        bottomHoriz: 3,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 3,
        topVerti: 9,
        bottomHoriz: 6,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 9,
        bottomHoriz: 9,
        bottomVerti: 12
      }
    }]
  }, {
    field: 'splitScreenBy9',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 4,
        bottomVerti: 4
      }
    }, {
      coordinate: {
        topHoriz: 4,
        topVerti: 0,
        bottomHoriz: 8,
        bottomVerti: 4
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 4
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 4,
        bottomHoriz: 4,
        bottomVerti: 8
      }
    }, {
      coordinate: {
        topHoriz: 4,
        topVerti: 4,
        bottomHoriz: 8,
        bottomVerti: 8
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 4,
        bottomHoriz: 12,
        bottomVerti: 8
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 8,
        bottomHoriz: 4,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 4,
        topVerti: 8,
        bottomHoriz: 8,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 8,
        topVerti: 8,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }]
  }, {
    field: 'splitScreenBy16',
    windowList: [{
      coordinate: {
        topHoriz: 0,
        topVerti: 0,
        bottomHoriz: 3,
        bottomVerti: 3
      }
    }, {
      coordinate: {
        topHoriz: 3,
        topVerti: 0,
        bottomHoriz: 6,
        bottomVerti: 3
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 0,
        bottomHoriz: 9,
        bottomVerti: 3
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 0,
        bottomHoriz: 12,
        bottomVerti: 3
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 3,
        bottomHoriz: 3,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 3,
        topVerti: 3,
        bottomHoriz: 6,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 3,
        bottomHoriz: 9,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 3,
        bottomHoriz: 12,
        bottomVerti: 6
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 6,
        bottomHoriz: 3,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 3,
        topVerti: 6,
        bottomHoriz: 6,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 6,
        bottomHoriz: 9,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 6,
        bottomHoriz: 12,
        bottomVerti: 9
      }
    }, {
      coordinate: {
        topHoriz: 0,
        topVerti: 9,
        bottomHoriz: 3,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 3,
        topVerti: 9,
        bottomHoriz: 6,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 6,
        topVerti: 9,
        bottomHoriz: 9,
        bottomVerti: 12
      }
    }, {
      coordinate: {
        topHoriz: 9,
        topVerti: 9,
        bottomHoriz: 12,
        bottomVerti: 12
      }
    }]
  }]

  _.forEach(list, (item) => {
    let keypath = 'buildInLayout.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}