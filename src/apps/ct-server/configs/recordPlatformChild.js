
import _ from 'lodash'

export function List(ctx) {
  var list = [{
    field: 'ipaddr',
    width: '20%'
  }, {
    field: 'devType',
    width: '20%'
  }, {
    field: 'link',
    width: '10%'
  }]

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 

}