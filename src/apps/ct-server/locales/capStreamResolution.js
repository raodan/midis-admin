export default {
  en: {
    list: {
      ipc: 'IP Camera',
      vendor: 'IP Camera Vendor',
      plat: 'Monitor Platform',
      tree: 'Media Sources Tree',
      capStremResolution: 'CapStream Cfg',
      resolution: 'Resolution',
      framerate: 'Framerate',
    }
  },
  cn: {
    list: {
      ipc: '网络摄像机',
      vendor: '网络摄像机厂商',
      plat: '监控平台',
      tree: '媒体源树',
      capStremResolution: '抓包流配置',
      resolution: '分辨率',
      framerate: '帧率',
    }
  }  
}