export default {
  en: {
    title: 'Record Room',
    list: {
      _id: 'ID',
      roomName: 'Room Name',
      scene: 'Scene',
      _live: 'Live',
      status: 'Status',
      topoIdOfMediaDev: 'Media Server',
    },
    layout: 'Layout',
    iFrameInterval: 'I frame interval',
    disabled: 'Disabled',
    seconds: 'Seconds',
    minutes: 'Minutes',
    hours: 'Hours',
  },
  cn: {
    title: '录播房间',
    list: {
      _id: 'ID',
      roomName: '房间名',
      scene: '场景',
      _live: '直播',
      status: '状态',
      topoIdOfMediaDev: '媒体服务器',
    },
    layout: '布局',
    iFrameInterval: 'I帧间隔',
    disabled: '不启用',
    seconds: '秒',
    minutes: '分钟',
    hours: '小时',
  }  
}