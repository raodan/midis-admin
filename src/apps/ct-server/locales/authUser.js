export default {
  en: {
    list: {
      username: 'User Name',
      role: 'Role',
      status: 'Status',
      password: 'Password',
      newPassword: 'New Password',
    },
    operation: {
      changePassword: 'Change Password',
      requestEnable: 'Request Enable',
      requestDisable: 'Request Disable',
      approveEnable: 'Confirm Enable',
      approveDisable: 'Confirm Disable',
    }
  },
  cn: {
    list: {
      username: '用户名',
      role: '角色',
      status: '状态',
      password: '密码',
      newPassword: '新密码',
    },
    operation: {
      changePassword: '修改密码',
      requestEnable: '申请启用',
      requestDisable: '申请禁用',
      approveEnable: '确认启用',
      approveDisable: '确认禁用',
    }
  }  
}