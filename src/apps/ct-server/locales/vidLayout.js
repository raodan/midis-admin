export default {
  en: {
    title: 'Video Layout',
    list: {
      _id: 'ID',
      name: 'Name',
      multiScreen: 'Multi Screen',
      layout: 'Scene'
    },
    switch: {
      title: 'Timing Switch',
      params: {
        interval: 'Interval'
      }
    },
    autoswitch: {
      title: 'Auto Switch',
      params: {
        interval: 'Interval(seconds)',
        list: 'List',
        status: 'Status',
        currLayoutName: 'Current',
        switchCount: 'Count',
        abortCode: 'Abort Code'
      },
      status: {
        stopped: 'Stopped',
        switching: 'Switching',
        aborted: 'Aborted'
      }
    }
  },
  cn: {
    title: '视频布局',
    list: {
      _id: 'ID',
      name: '名称',
      multiScreen: '适用宽高',
      layout: '场景'
    },
    switch: {
      title: '定时切换',
      params: {
        interval: '时间间隔'
      }
    },
    autoswitch: {
      title: '自动切换',
      params: {
        interval: '时间间隔（秒）',
        list: '切换列表',
        status: '状态',
        currLayoutName: '当前布局',
        switchCount: '切换次数',
        abortCode: '中止原因'
      },
      status: {
        stopped: '已停止',
        switching: '切换中',
        aborted: '已中止'
      }
    }
  }  
}