export default {
  en: {
    list: {
      title: 'Set Window Switch',
      windowId: 'Window ID',
      srcSetId: 'SrcSet ID',
      enable: 'Enable',
      interval: 'Interval',
      openWindow: 'Open Window',
      quit: 'Quit Open Window',      
      srcset: 'Source Set',      
    }
  },
  cn: {
    list: {
      title: '设置窗口码流轮巡',
      windowId: '窗口ID',
      srcSetId: '码流集合ID',
      enable: '启用状态',
      interval: '定时间隔',
      openWindow: '开窗',
      quit: 'Quit Open Window',      
      srcset: '码流集合',
    }
  }  
}