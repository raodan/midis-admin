export default {
  en: {
    form: {
      maxTrySignInFailTimes: 'Maximum Login Attempts',
      forceUpdatePassword: {
        enable: 'Regularly Change Password',
        daysOfPeriod: 'Password Update Period',
        applyToRoles: 'Strategy to Roles'
      },
      enableSameIpWithSameUserLogin: 'Allow the same IP and user name to log in at the same time'
    }
  },
  cn: {
    form: {
      maxTrySignInFailTimes: '最大连续登陆尝试次数',
      forceUpdatePassword: {
        enable: '定期更换密码',
        daysOfPeriod: '密码更换时长',
        applyToRoles: '策略适用角色'
      },
      enableSameIpWithSameUserLogin: '允许相同IP相同用户客户端同时登陆'
    }
  }  
}