export default {
  en: {
    list: {
      manufacture: 'Manufacturer',
      productModelsStr: 'Product Model',
      mainPathFmt: 'Main Stream Path',
      subPathFmt: 'Sub Stream Path'
    }
  },
  cn: {
    list: {
      manufacture: '厂商',
      productModelsStr: '产品型号',
      mainPathFmt: '主码流路径',
      subPathFmt: '次码流路径'
    }
  }  
}