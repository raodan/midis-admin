export default {
  en: {
    list: {
      _id: 'ID',
      name: 'Name',
      ipaddr: 'IP',
      macaddr: 'Mac Address',
      hostname: 'Host Name',
      state: 'State'
    },
    local: {
      online: 'Online',
      offline: 'Offline'
    }
  },
  cn: {
    list: {
      _id: 'ID',
      name: '名称',
      ipaddr: 'IP地址',
      macaddr: '物理地址',
      hostname: '主机名',
      state: '状态'
    },
    local: {
      online: '在线',
      offline: '离线'
    }
  }  
}