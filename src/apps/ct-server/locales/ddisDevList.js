export default {
  en: {
    list: {
      devModel: 'Device Model',
      devModelName: 'Device Model Name',
      devType: 'Device Type',
      swVersion: 'Version',
      devSN: 'Serial Number',
      ipaddr: 'IP Address',
      mask: 'Subnet Mask',
      gateway: 'Gateway',
      mac: 'MAC Address',
      link: 'Link',
      search: 'Plus Search',
      clear: 'New Search'
    }
  },
  cn: {
    list: {
      devModel: '型号',
      devType: '类型',
      swVersion: '版本号',
      devSN: '序列号',
      ipaddr: 'IP地址',
      mask: '子网掩码',
      gateway: '默认网关',
      mac: 'MAC地址',
      link: '链接',
      search: '追加搜索',
      clear: '全新搜索',
      devModelName: '型号名',
    }
  }  
}