export default {
  en: {
    list: {
      _id: 'ID',
      id: 'Switch ID',
      name: 'Name',
      lcdTopoId: 'LCD Topo ID',
      windowId: 'Channel ID',
      srcSetType: 'Source List Type',
      srcSetId: 'Source Set ID',
      srcName: 'Source Set Name',
      enable: 'Enable',     
      stateStart: 'Yes',
      stateStop: 'No',
      stateNoEffect: 'No Effect', 
      interval: 'Interval(S)',
      notfound: 'Not Found',   
      lcdName: 'LCD',
      model: 'Model',
      area: 'Area',
      default: 'Default'
    },
    options: {
      custom: 'Custom',
      streamType: 'Stream Type'
    },
    streamTypeFilter: {
      srcType: 'Source Type',
      matchedNames: 'Include Name'
    },
    layout: {
      skipSwitch: 'Skip Switch',
      playAudio: 'Play Audio',
      preview: 'Preview'
    }
  },
  cn: {
    list: {
      _id: 'ID',
      id: '轮巡ID',
      name: '名称',
      lcdTopoId: 'LCD',
      windowId: '通道ID',
      srcSetType: '码流源列表类型',
      srcSetId: '码流集合ID',
      srcName: '码流集合名称',
      enable: '是否启用',
      stateStart: '是',
      stateStop: '否',
      stateNoEffect: '无效',
      interval: '轮巡间隔(秒)',
      notfound: '未找到',
      lcdName: 'LCD',
      model: '模式',
      area: '区域',
      default: '默认'
    },
    options: {
      custom: '自定义码流集合',
      streamType: '码流源类型集合'
    },
    streamTypeFilter: {
      srcType: '码流类型',
      matchedNames: '包含名称'
    },
    layout: {
      skipSwitch: '不参与轮巡窗口',
      playAudio: '播放音频窗口',
      preview: '预览'
    }
  }  
}