export default {
  en: {
    title: 'Record Encoder',
    list: {
      _id: 'ID',
      name: 'Name',
      ipaddr: 'IP',
      macaddr: 'Mac Address',
      username: 'User Name',
      password: 'Password',
      manufacture: 'Manufacture',
      productModel: 'Product Model',
      topoIdOfMediaDev: 'Media Server',
      runState: 'Running State',
      recState: 'Recording State',
      streams: 'Streams',
      port: 'Port',
      unknown: 'UnKnown'
    },
    local: {
      channel: 'Channel{index}',
      enProxy: 'Enable',
      on: 'On',
      off: 'Off',
      idle: 'Idle',
      recording: 'Recording',
      paused: 'Paused',
    }
  },
  cn: {
    title: '录播一体机',
    list: {
      _id: 'ID',
      name: '名称',
      ipaddr: 'IP地址',
      macaddr: '物理地址',
      username: '用户名',
      password: '密码',
      manufacture: '厂商',
      productModel: '产品型号',
      topoIdOfMediaDev: '媒体服务器',
      runState: '运行状态',
      recState: '录制状态',
      streams: '媒体流',
      port: '端口',
      unknown: '未知'
    },
    local: {
      channel: '通道{index}',
      enProxy: '启用代理',
      on: '运行中',
      off: '已关机',
      idle: '空闲',
      recording: '录制中',
      paused: '已暂停',
    }
  }  
}