export default {
  en: {
    title: 'Source Sets',
    list: {
      _id: 'ID',
      id: 'Source Sets ID',
      name: 'Name',
      lcdTopoId: 'LCD',
      srcSetType: 'Source List Type',
      custom: 'Custom Type',
      monitorSrc: 'MonitorSrc Type',
      streamType: 'Stream Type',
      treeSrc: 'Tree Source Type',

    },
    local: {
     
    }
  },
  cn: {
    title: '码流集合',
    list: {     
      _id: 'ID',
      id: '码流集合ID',
      name: '名称',
      lcdTopoId: 'LCD',
      srcSetType: '码流源列表类型',
      custom: '自定义码流集合',
      monitorSrc: '监控平台源类型集合',
      streamType: '码流源类型集合',
      treeSrc: '媒体源树码流集合',
    },
    local: {
     
    }
  }  
}