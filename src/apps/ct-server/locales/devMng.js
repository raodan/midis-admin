export default {
  en: {
    tabs: {
      ddis: 'Device Information',
      topo: 'Topo Information'
    }
  },
  cn: {
    tabs: {
      ddis: '设备信息',
      topo: '拓扑信息'
    }
  }  
}