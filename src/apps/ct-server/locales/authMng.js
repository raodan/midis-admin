export default {
  en: {
    tabs: {
      user: 'User List',
      secpolicy: 'Security Strategy',
    }
  },
  cn: {
    tabs: {
      user: '用户列表',
      secpolicy: '安全策略',
    }
  }  
}