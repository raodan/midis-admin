export default {
  en: {
    list: {
      _id: 'ID',
      name: 'Name',
      ipaddr: 'IP Address',
      port: 'Port',
      online: 'State',
      tag: 'Tag',
      mainUrl: 'Main Stream URL',
      previewUrl: 'Sub Stream URL'
    },
    local: {
      online: 'Online',
      offline: 'Offline',
      manualAdd: 'Manual Add'
    }
  },
  cn: {
    list: {
      _id: 'ID',
      name: '名称',
      ipaddr: 'IP地址',
      port: '端口号',
      online: '状态',
      tag: '标签',
      mainUrl: '主码流地址',
      previewUrl: '次码流地址'
    },
    local: {
      online: '在线',
      offline: '离线',
      manualAdd: '手动添加'
    }
  }  
}