export default {
  en: {
    list: {
      _id: 'ID',
      name: 'Name',
      ipaddr: 'IP Address',
      port: 'Port',
      tag: 'Tag',
      mainUrl: 'Main Stream URL',
      previewUrl: 'Sub Stream URL',
      realResX: 'Width',
      realResY: 'Height',
      custom: 'Custom'
    },
    lerror: {
      realRex: 'Too small width or height'
    },
    local: {
      baseUrl: 'Super high resolution channel{index} stream URL',
    }
  },
  cn: {
    list: {
      _id: 'ID',
      name: '名称',
      ipaddr: 'IP地址',
      port: '端口号',
      tag: '标签',
      mainUrl: '主码流地址',
      previewUrl: '次码流地址',
      realResX: '宽',
      realResY: '高',
      custom: '自定义'
    },
    lerror: {
      realRex: '宽高过小'
    },
    local: {
      baseUrl: '超高分通道{index}码流地址',
    }
  }  
}