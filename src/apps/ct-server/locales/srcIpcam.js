export default {
  en: {
    list: {
      _id: 'ID',
      name: 'Name',
      manufacture: 'Manufacturer',
      productModel: 'Product Model',
      ipaddr: 'IP Address',
      port: 'Port',
      username: 'User Name',
      password: 'Password',
      channel: 'Channel',
      channelRange: 'Channel Range',
      ptzStatus: 'PTZ Status',
      onvifPort: 'Onvif Port',
      subStatus: 'Sub Stream Status',
      tag: 'Tag',
      useCustomUrl: 'Custom URL',
      mainUrl: 'Main Stream URL',
      mainRes: 'Main Stream Resolution',
      previewUrl: 'Sub Stream URL',
      audioUrl: 'Extra Audio URL',
      soapVersion: 'Soap Version',
      proxyEnable: 'Enable Proxy',
      topoIdOfMediaDev: 'Media Server For Proxy',
      codec: 'Main Stream Vedio Encode'
    },
    onvif: {
      enable: 'Support',
      disable: 'Not Support',
      auto: 'Auto Check',
      checking: 'Checking',
    }
  },
  cn: {
    list: {
      _id: 'ID',
      name: '名称',
      manufacture: '厂商',
      productModel: '产品型号',
      ipaddr: 'IP地址',
      port: '端口号',
      username: '用户名',
      password: '密码',
      channel: '通道号',
      channelRange: '起止通道号',
      ptzStatus: '云台控制',
      onvifPort: 'Onvif端口号',
      subStatus: '启用次码流',
      tag: '标签',
      useCustomUrl: '自定义URL',
      mainUrl: '主码流地址',
      mainRes: '主码流分辨率',
      previewUrl: '次码流地址',
      audioUrl: '外部音频地址',
      soapVersion: 'Soap版本号',
      proxyEnable: '启用代理',
      topoIdOfMediaDev: '代理用媒体服务器',
      codec: '主码流视频编码'
    },
    onvif: {
      enable: '支持',
      disable: '不支持',
      auto: '自动检测',
      checking: '检测中',
    }
  }  
}