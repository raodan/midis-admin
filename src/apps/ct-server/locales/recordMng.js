export default {
  en: {
    tabs: {
      recordroom: 'Record Room',
      recenc: 'Record Encoder',
      recordplatform: 'Record Platform',
      recordplatRoom: 'Record PlatRoom'
    }
  },
  cn: {
    tabs: {
      recordroom: '录播房间',
      recenc: '录播一体机',
      recordplatform: '录播平台',
      recordplatRoom: '录播平台房间'
    }
  }  
}