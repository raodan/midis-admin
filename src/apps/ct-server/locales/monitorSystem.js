export default {
  en: {
    tabs: {
      ipc: 'IP Camera',
      vendor: 'IP Camera Vendor',
      plat: 'Monitor Platform',
      tree: 'Media Sources Tree',
      capStremResolution: 'CapStream Cfg',
    }
  },
  cn: {
    tabs: {
      ipc: '网络摄像机',
      vendor: '网络摄像机厂商',
      plat: '监控平台',
      tree: '媒体源树',
      capStremResolution: '抓包流配置',
    }
  }  
}