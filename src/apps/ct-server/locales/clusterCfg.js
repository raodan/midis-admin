export default {
  en: {
    title: 'Main Server Backup',
    form: {
      virtualIpaddr: 'Virtual IP Address',
      virtualIpNetmask: 'Virtual Subnet Mask'
    }
  },
  cn: {
    title: '主服务器备份',
    form: {
      virtualIpaddr: '虚拟IP地址',
      virtualIpNetmask: '虚拟IP子网掩码'
    }
  }  
}