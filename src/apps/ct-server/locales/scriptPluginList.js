export default {
  en: {
    list: {
      typeStr: 'Type',
      pluginId: 'Plugin ID',
      name: 'Name',
      version: 'Version',
      provider: 'Provider',
      tag: 'Tag',
    },
    types: {
      thirdparty: 'Third Party',
      native: 'Native'
    }
  },
  cn: {
    list: {
      typeStr: '类别',
      pluginId: '插件ID',
      name: '名称',
      version: '版本',
      provider: '作者',
      tag: '标签',
    },
    types: {
      thirdparty: '第三方',
      native: '原生'
    }
  }  
}