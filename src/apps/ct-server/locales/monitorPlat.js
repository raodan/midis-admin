export default {
  en: {
    list: {
      _id: 'ID',
      name: 'Name',
      ipaddr: 'IP Address',
      port: 'Port',
      username: 'User Name',
      password: 'Password',
      pushNum: 'Push Stream Number',
      topoIdOfMediaDev: 'Media Server',
      vendor: 'Vendor',
      mainDftRes: 'Main Stream Default Resolution',
      codec: 'Main Stream Vedio Encode',
    }
  },
  cn: {
    list: {
      _id: 'ID',
      name: '名称',
      ipaddr: 'IP地址',
      port: '端口号',
      username: '用户名',
      password: '密码',
      pushNum: '推送流数量',
      topoIdOfMediaDev: '媒体服务器',
      vendor: '厂商',
      mainDftRes: '主码流默认分辨率',
      codec: '主码流视频编码',
    }
  }  
}