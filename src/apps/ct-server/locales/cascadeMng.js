export default {
  en: {
    tabs: {
      ldev: 'Device Info',
      lsharesrc: 'Local Share Sources',
      rdev: 'Remote Device',
    }
  },
  cn: {
    tabs: {
      ldev: '设备信息',
      lsharesrc: '发布源',
      rdev: '下级信息',
    }
  }  
}