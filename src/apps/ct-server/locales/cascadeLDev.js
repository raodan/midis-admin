export default {
  en: {
    title: 'Infomation List',
    list: {
      upCallCount: 'Tx Count',
      upCallBand: 'Tx Bandwidth',
      downCallCount: 'Rx Count',
      downCallBand: 'Rx Bandwidth'
    }
  },
  cn: {
    title: '信息列表',
    list: {
      upCallCount: '上行呼叫路数',
      upCallBand: '上行带宽',
      downCallCount: '下行呼叫路数',
      downCallBand: '下行带宽'
    }
  }
}