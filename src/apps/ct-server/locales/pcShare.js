export default {
  en: {
    tabs: {
      softenc: 'Desktop Sharing',
      pclist: 'PC List',
      superhdscreen: 'Super HD Screen'
    }
  },
  cn: {
    tabs: {
      softenc: '桌面共享',
      pclist: '电脑列表',
      superhdscreen: '超高分辨率'
    }
  }  
}