export default {
  en: {
    list: {
      _id: 'ID',
      name: 'Name',
      addr: 'IP',
      topoId: 'Topo ID',
      upCallCount: 'Tx Count',
      upCallBand: 'Tx Bandwidth',
      downCallCount: 'Rx Count',
      downCallBand: 'Rx Bandwidth',
      status: 'Status',
    },
    statusOpt: {
      online: 'Online',
      offline: 'Offline'
    }
  },
  cn: {
    list: {
      _id: 'ID',
      name: '中心服务名称',
      addr: '中心服务IP地址',
      topoId: '级联设备拓扑ID',
      upCallCount: '上行呼叫路数',
      upCallBand: '上行带宽',
      downCallCount: '下行呼叫路数',
      downCallBand: '下行带宽',
      status: '状态',
    },
    statusOpt: {
      online: '在线',
      offline: '离线'
    }
  }  
}