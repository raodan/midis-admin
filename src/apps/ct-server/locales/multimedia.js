export default {
  en: {
    tabs: {
      lcdwindow: 'LCD Window',
      vidlayout: 'LCD Scene',
      vlayout: 'Layout',
      vswitch: 'Switch',
      vlayoutwindow: 'Layout Window',
      vcodestreamset: 'CodeStream Set',
      vwindowswitch: 'Channel Switch',
      vlcdpreset: 'LCD Preset',
      mcuSetting: 'Mcu CapStream Setting',
    }
  },
  cn: {
    tabs: {
      lcdwindow: '开窗管理',
      vidlayout: '窗口场景',
      vlayout: '布局管理',
      vswitch: '轮巡管理',
      vlayoutwindow: '布局开窗',
      vcodestreamset: '码流集合',
      vwindowswitch: '通道轮巡',
      vlcdpreset: '窗口预设',
      mcuSetting: 'MCU抓包流设置',
    }
  }  
}