export default {
  en: {
    list: {
      _id: 'ID',
      name: 'Scenes Name',
      add: 'Add',
      canSelected: 'Option',
      selected: 'Selection',
      addSelect: 'Add the selection',
      addAll: 'Add All',
      removeSelect: 'Remove the selection',
      removeAll: 'Remove All'
    }
  },
  cn: {
    list: {
      _id: 'ID',
      name: '名称',
      add: '添加',
      canSelected: '可选项',
      selected: '已选项',
      addSelect: '添加选中项',
      addAll: '添加全部',
      removeSelect: '移除选中项',
      removeAll: '移除全部'
    }
  }  
}