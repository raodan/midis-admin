export default {
  en: {
    list: {
      _id: 'ID',
      callId: 'No.',
      name: 'Name',
      topoIdOfMediaDev: 'Media Server',
      streamSrc: 'Stream Source'
    }
  },
  cn: {
    list: {
      _id: 'ID',
      callId: '呼叫号码',
      name: '名称',
      topoIdOfMediaDev: '媒体服务器',
      streamSrc: '码流源',
    }
  }  
}