export default {
  en: {
    list: {
      devModel: 'Device Model',
      devModelName: 'Device Model Name',
      devType: 'Device Type',
      topoId: 'Topo ID',
      devSN: 'Serial Number',
      devName: 'Device Name',
      devTag: 'Device Tag'
    },
    navbar: {
      topoBindsEdit: 'Topo Binding Edit'
    }
  },
  cn: {
    list: {
      devModel: '型号',
      devType: '类型',
      topoId: '拓扑ID',
      devSN: '序列号',
      devName: '设备名称',
      devTag: '设备标签',
      devModelName: '型号名',
    },
    navbar: {
      topoBindsEdit: '绑定编辑'
    }
  }  
}