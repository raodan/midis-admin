export default {
  en: {
    list: {
      _id: 'ID',
      srcid: 'Source Set ID',
      id: 'Switch ID',
      name: 'Name',
      lcdTopoId: 'LCD Topo ID',
      layout: 'Layout',
      srcListType: 'Source List Type',
      interval: 'Interval',
      enable: 'Enable',
      srcSetType: 'Source Set Type'
    },
    options: {
      custom: 'Custom',
      monitorSrc: 'MonitorSrc Type',
      streamType: 'Stream Type',
      mediaSrcTree: 'Media Src Tree'
    },
    streamTypeFilter: {
      srcType: 'Source Type',
      matchedNames: 'Include Name',
      limitLen: 'Only allow input 128 chars'
    },
    layout: {
      skipSwitch: 'Skip Switch',
      playAudio: 'Play Audio',
      preview: 'Preview'
    },
    tip:{
      funcCancel: 'The page has been abandoned',
      plsUseOther: 'Please trun to page \'Layout Window\' '

    }
  },
  cn: {
    list: {
      _id: 'ID',
      srcid: '码流集合ID',
      id: '轮巡ID',
      name: '名称',
      lcdTopoId: 'LCD',
      layout: '布局',
      srcListType: '码流源列表类型',
      interval: '轮巡间隔（秒）',
      enable: '是否启用',
      srcSetType: '码流源列表类型'
    },
    options: {
      custom: '自定义码流集合',
      monitorSrc: '监控平台源类型集合',
      streamType: '码流源类型集合',
      mediaSrcTree: '媒体源树码流集合'
    },
    streamTypeFilter: {
      srcType: '码流类型',
      matchedNames: '包含名称',
      limitLen: '最多输入128个字符'
    },
    layout: {
      skipSwitch: '不参与轮巡窗口',
      playAudio: '播放音频窗口',
      preview: '预览'
    },
    tip:{
      funcCancel: '此功能已废弃',
      plsUseOther: '请使用布局开窗功能'
    }
  }  
}