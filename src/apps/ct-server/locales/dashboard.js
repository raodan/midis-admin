export default {
  en: {
    menus: {
      system: 'System Settings',
      license: 'License Settings',
      monitor: 'Monitor System',
      pcshare: 'PC Management',
      multimedia: 'Media Settings',
      cascade: 'Cascade Settings',
      recordmng: 'Record Settings',
      devmng: 'Device Settings',
      webapp: 'App Settings',
      scriptplugin: 'Plugin Script Settings',
      log: 'System Logs',
      authmng: 'User Settings',
      scenes: 'Scenes'
    },
    sites: {
      company: 'Tendzone',
      about: 'About',
      support: 'Support'
    },
    usermenus: {
      signout: 'Log Out',
      resetFactory: 'Reset To Factory',
      clearTopo: 'Clear Topo',
      clearLicense: 'Clear License',
      resetServer: 'Reset Server',
      restart: 'Restart Service',
      backupTopo: 'Backup Topo',
      recoveryTopo: 'Recovery Topo',
      backupIni: 'Backup Configuration ',
      recovryIni: 'Recovery Configuration ',
    }
  },
  cn: {
    menus: {
      system: '系统设置',
      license: '授权管理',
      monitor: '监控系统',
      pcshare: '电脑管理',
      multimedia: '媒体管理',
      cascade: '级联管理',
      recordmng: '录播管理',
      devmng: '设备管理',
      webapp: '应用管理',
      scriptplugin: '插件管理',
      log: '系统日志',
      authmng: '用户管理',
      scenes: '场景管理'
    },
    sites: {
      company: 'tendzone',
      about: '关于',
      support: '技术支持'
    },
    usermenus: {
      signout: '登出',
      resetFactory: '恢复出厂设置',
      clearTopo: '清空拓扑',
      clearLicense: '清空授权',
      resetServer: '重置服务设置',
      restart: '重启服务程序',
      backupTopo: '一键备份拓扑',
      recoveryTopo: '一键还原拓扑',
      backupIni: '一键备份配置',
      recovryIni: '一键还原配置',
    }
  }  
}