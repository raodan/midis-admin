export default {
  en: {
    titil: 'Record Platform',
    list: {
      Id: 'ID',
      version: 'Version',
      diskSpace: 'DiskSpace(MB)',
      cpuModel: 'CpuModel',
      memory: 'Memory(MB)',
      license: 'License',
      diskFreeSpace: 'DiskFreeSpace(MB)',
      cpuRatio: 'CPU Tatio(%)',
      memoryRatio: 'Memory Ratio(%)',
      topoId: 'Topo ID',
      ipaddr: "IP",
      username: "UserName",
      password: "PassWord",
      devType: "DevType",
      status: "Status",
      link: 'Link',
      system: 'System Warnnings'
    },
    local: {
      TopoId: 'TopoId',
      Link: 'Link',
      Restart: 'Restart',
      Child: '子级',
      Detail: 'Detail',
      Warnning: 'Warnning',
    }
  },
  cn: {
    titil: '录播平台',
    list: {
      Id: 'ID',
      version: '版本信息',
      diskSpace: '磁盘空间(MB)',
      cpuModel: 'CPU型号',
      memory: '内存大小(MB)',
      license: '有效期天数',
      diskFreeSpace: '硬盘剩余(MB)',
      cpuRatio: 'CPU使用百分比(%)',
      memoryRatio: '内存使用百分比(%)',
      topoId: '拓扑ID',
      ipaddr: "IP地址",
      username: "用户名",
      password: "密码",
      devType: "设备类型",
      status: "状态",
      link: '链接',
      system: '报警信息'
    },
    local: {
      TopoId: '拓扑ID',
      Link: '链接',
      Restart: '强制重启',
      Child: '子级',
      Detail: '详细信息',
      Warnning: '报警信息',
    }
  }
}