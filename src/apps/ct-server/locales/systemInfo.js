export default {
  en: {
    title: 'System Infomation',
    list: {
      macaddr: 'MAC Address',
      devSN: 'Serial Number',
      devModel: 'Device Model',
      softwareVersion: 'Version',
      buildDate: 'Built Date'
    }
  },
  cn: {
    title: '系统信息',
    list: {
      macaddr: 'MAC地址',
      devSN: '序列号',
      devModel: '设备型号',
      softwareVersion: '软件版本号',
      buildDate: '编译时间'
    }
  }  
}