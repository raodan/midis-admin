export default {
  en: {
    list: {
      _id: 'ID',
      id: 'Layout ID',
      name: 'Name',
      multiScreen: 'Multi Screen',
      layout: 'Layout'
    },
    local: {      
      outofRang: 'Screen({x},{y}) exceeds limit of {limit}',
      fastSplit: 'Fast division',
      rotate: 'Rotate',
    }
  },
  cn: {
    list: {
      _id: 'ID',
      id: '布局ID',
      name: '名称',
      multiScreen: '布局宽高',
      layout: '布局'
    },
    local: {      
      outofRang: '屏幕({x},{y})超过最大{limit}的限制',
      fastSplit: '快速划分',
      rotate: '旋转角度',
    }
  }  
}