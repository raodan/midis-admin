export default {
  en: {
    titil: 'Record Platroom',
    list: {
      _id: 'ID',
      topoId: 'Topo ID',
      classRoomId: 'Room ID',
      name: 'Name',
      alias: 'Alias',
      recStatus: 'RecSatus',
      liveStatus: 'LiveStatus',
      playing: 'Playing',
      recording: 'Recording',
      stoped: 'Stop',
      srcType: 'Type'
    },
    local: {
      setSource: 'Set Source',
      RecordName: 'RecordName:',
      layout: 'Layout',
    }
  },
  cn: {
    titil: '录播平台房间',
    list: {
      _id: 'ID',
      topoId: '拓扑ID',
      classRoomId: '房间ID',
      name: '名称',
      alias: '录制别名',
      recStatus: '录制状态',
      liveStatus: '直播状态',
      playing: '直播中',
      recording: '录制中',
      stoped: '停止',
      srcType: '类型'
    },
    local: {
      setSource: '源配置',
      RecordName: '录制别名：',
      layout: '布局',
    }
  }
}