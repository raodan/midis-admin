export default {
  en: {
    title: 'System Infomation',
    form: {
      category: 'Type',
      tag: 'Tag'
    },
    webapp: {
      provider: 'Provider',
      link: 'Link',
      version: 'Version',
    },
    list: {
      all: 'All',
      multimedia: 'Multimedia',
      control: 'Environment Control',
      tool: 'Tool',
      debug: 'Debug',
      other: 'Other'
    }
  },
  cn: {
    title: '系统信息',
    form: {
      category: '类别',
      tag: '标签'
    },
    webapp: {
      provider: '作者',
      link: '链接',
      version: '版本号',
    },
    list: {
      all: '全部',
      multimedia: '多媒体类',
      control: '环境控制类',
      tool: '工具类',
      debug: '调试类',
      other: '其它'
    }
  }  
}