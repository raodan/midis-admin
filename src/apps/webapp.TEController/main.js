import Vue from 'vue'
import * as utils from 'libs/utils.js'

Vue.config.devtools = true
Vue.config.debug = true

import 'bootstrap/dist/css/bootstrap.min.css'
import 'external/css/rdash.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'sweetalert/dist/sweetalert.css'

//多语言
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
Vue.config.lang = function() {
  let args = utils.getQueryStringArgs()
  if (args && args['lang']) {
    return args['lang']
  } else {
    return 'en'
  }
}()
import locales from 'locales/global.js'
Object.keys(locales).forEach(function(lang) {
  Vue.locale(lang, locales[lang])
})

//http
import Resource from 'vue-resource'
Vue.use(Resource)

Vue.http.interceptors.push({
  request: function(request) {
    return request
  },
  response: function(response) {
    return response
  }
})

//progress
import VueProgress from 'vue-progressbar'
Vue.use(VueProgress)

//validator
import Validator from 'vue-validator'
Vue.use(Validator)

Vue.validator('ip', utils.checkValidIp)

//component
import { 
  Compact as VcCompact,
  Material as VcMaterial,
  Slider as VcSlider,
  Swatches as VcSwatches,
  Photoshop as VcPhotoshop,
  Sketch as VcSketch,
  Chrome as VcChrome
} from 'vue-color'

Vue.component('vc-compact', VcCompact)
Vue.component('vc-material', VcMaterial)
Vue.component('vc-slider', VcSlider)
Vue.component('vc-swatches', VcSwatches)
Vue.component('vc-photoshop', VcPhotoshop)
Vue.component('vc-sketch', VcSketch)
Vue.component('vc-chrome', VcChrome)

import MySignIn2 from 'components/MySignIn2.vue'
import MyNavbar from 'components/MyNavbar.vue'
import MyImgUpload from 'components/MyImgUpload1.vue'

Vue.component('my-sign-in2', MySignIn2)
Vue.component('my-navbar', MyNavbar)
Vue.component('my-img-upload', MyImgUpload)

import {
  modal as VsModal,
} from 'vue-strap'
Vue.component('vs-modal', VsModal)

import 'vue-toastr/dist/vue-toastr.min.css'
import vueToastr from 'vue-toastr'
Vue.component('vue-toastr', vueToastr)

import vueProgress from 'vue-progressbar/vue-progressbar.vue'
Vue.component('vue-progress', vueProgress)

import AppView from './App.vue'

new Vue({
  el: '#root',
  components: {
    'app-view': AppView
  },
  methods: {
    initToast () {
      var toast = this.$refs.toast

      toast.defaultTimeout = 3000
      toast.defaultType = "error"
      toast.defaultPosition = "toast-bottom-right"
    }
  },
  ready () {
    this.initToast()
  }
})