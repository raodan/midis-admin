export default{
  en:{
    recfiles:{
      searchTerm: 'Search conditions',
      downloadLink: 'Download linkage',
      serverAddr: 'Server address',
      recordsFile: 'Recorded files',
      scanFilter: 'Scene filter',
      roomName: 'Room name',
      startTime: 'Start time',
      endTime: 'End time',
      scene: 'Scenes',
      shichang: 'Runtime',
      daxiao: 'Size',
      dianbo: 'Play',
      caozuo: 'Operation',
      lianjie: 'Linkage',
      yilianjie: 'Connected',
      copyPath: 'Copy the path to the clipboard',
      tianjia: 'Please add the service address',
      zuoce: 'On the left to add'
    }
  },
  cn:{
    recfiles:{
      searchTerm: '搜索条件',
      downloadLink: '下载链接',
      serverAddr: '服务地址',
      recordsFile: '录播文件',
      scanFilter: '场景过滤',
      roomName: '房间名',
      startTime: '起始时间',
      endTime: '结束时间',
      scene: '场景',
      shichang: '时长',
      daxiao: '总大小',
      dianbo: '点播',
      caozuo: '操作',
      lianjie: '链接',
      yilianjie: '已连接',
      copyPath: '复制路径到剪切板',
      tianjia: '请先添加服务地址',
      zuoce: '左侧添加'
    }
  }
}