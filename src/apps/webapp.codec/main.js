import Vue from 'vue'
import * as utils from 'libs/utils.js'

Vue.config.devtools = true
Vue.config.debug = true

import 'bootstrap/dist/css/bootstrap.min.css'

//多语言
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
Vue.config.lang = function() {
  let args = utils.getQueryStringArgs()
  if (args && args['lang']) {
    return args['lang']
  } else {
    return 'en'
  }
}()
import locales from 'locales/global.js'
Object.keys(locales).forEach(function(lang) {
  Vue.locale(lang, locales[lang])
})

//http
import Resource from 'vue-resource'
Vue.use(Resource)

Vue.http.interceptors.push({
  request: function(request) {
    return request
  },
  response: function(response) {
    return response
  }
})

//validator
import Validator from 'vue-validator'
Vue.use(Validator)

Vue.validator('ip', utils.checkValidIp)

//component
import MyNavbar from 'components/MyNavbar.vue'
import MyTable from 'components/MyTable.vue'
import MyAddIpDetail from 'components/MyAddIpDetail.vue'
import MyFileUpload from 'components/MyFileUpload.vue'

Vue.component('my-navbar', MyNavbar)
Vue.component('my-table', MyTable)
Vue.component('my-file-upload', MyFileUpload)

import {
  alert as VsAlert,
  carousel as VsCarousel,
  slider as VsSlider,
  accordion as VsAccordion,
  affix as VsAffix,
  aside as VsAside,
  checkboxBtn as VsCheckboxBtn,
  checkboxGroup as VsCheckboxGroup,
  datepicker as VsDatepicker,
  dropdown as VsDropdown,
  modal as VsModal,
  option as VsOption,
  panel as VsPanel,
  popover as VsPopover,
  progressbar as VsProgressbar,
  radioGroup as VsRadioGroup,
  radioBtn as VsRadioBtn,
  select as VsSelect,
  tab as VsTab,
  tabset as VsTabset,
  tooltip as VsTooltip,
  typeahead as VsTypeahead,
  navbar as VsNavbar,
  spinner as VsSpinner
} from 'vue-strap'

Vue.component('vs-alert', VsAlert)
Vue.component('vs-carousel', VsCarousel)
Vue.component('vs-slider', VsSlider)
Vue.component('vs-accordion', VsAccordion)
Vue.component('vs-affix', VsAffix)
Vue.component('vs-aside', VsAside)
Vue.component('vs-checkbox-btn', VsCheckboxBtn)
Vue.component('vs-checkbox-group', VsCheckboxGroup)
Vue.component('vs-datepicker', VsDatepicker)
Vue.component('vs-dropdown', VsDropdown)
Vue.component('vs-modal', VsModal)
Vue.component('vs-option', VsOption)
Vue.component('vs-panel', VsPanel)
Vue.component('vs-popover', VsPopover)
Vue.component('vs-progressbar', VsProgressbar)
Vue.component('vs-radio-group', VsRadioGroup)
Vue.component('vs-radio-btn', VsRadioBtn)
Vue.component('vs-select', VsSelect)
Vue.component('vs-tab', VsTab)
Vue.component('vs-tabset', VsTabset)
Vue.component('vs-tooltip', VsTooltip)
Vue.component('vs-typeahead', VsTypeahead)
Vue.component('vs-navbar', VsNavbar)
Vue.component('vs-spinner', VsSpinner)

import 'vue-toastr/dist/vue-toastr.min.css'
import vueToastr from 'vue-toastr'
Vue.component('vue-toastr', vueToastr)

import AppView from './App.vue'

new Vue({
  el: '#root',
  components: {
    'app-view': AppView
  },
  methods: {
    initToast () {
      var toast = this.$refs.toast

      toast.defaultTimeout = 3000
      toast.defaultType = "error"
      toast.defaultPosition = "toast-bottom-right"
    }
  },
  ready () {
    this.initToast()
  }
})