export default{
  en:{
    log:{
      logInfo: 'Log information',
      sheBei: 'Device address',
      serverAddr: 'Device type',
      serverIp: 'Server IP',
      logType: 'Log type:',
      logExport: 'Export log:',
      logCheck: 'View',
      startPackage: 'Start packaging ',
      logDowload: 'Download',
      logType1: 'Trace: debug trace',
      logType2: 'Trace: prompt and warning',
      logType3: 'Trace: error',
      logType4: 'Trace: slave',
      logTishi1: 'Please select a log type before clicking the button',
      logTishi2: 'Click the button to download linkage',
      packaging: 'packaging...'
    }
  },
  cn:{
    log:{
      logInfo: '日志信息',
      sheBei: '设备地址',
      serverAddr: '设备类型',
      serverIp: '设备IP',
      logType: '日志类型:',
      logExport: '日志导出:',
      logCheck: '查看',
      startPackage: '开始打包',
      logDowload: '下载',
      logType1: 'trace:调试trace',
      logType2: 'trace:提示和告警',
      logType3: 'trace:错误',
      logType4: 'trace:从端',
      logTishi1: '点击按钮前请选择日志类型',
      logTishi2: '点击按钮获得下载链接',
      packaging: '打包中...'
    }
  }
}