import Vue from 'vue'
import debugCfg from 'share/debugCfg.js'
var ssl1 = window.location.protocol === 'https:' ? 'https' : 'http';
if (debugCfg.debugOn) {
	ssl1 = 'https';
}
export var ssl = ssl1;

//不能放在main.js中，循环依赖会导致build不过
export function http(host, uri, method, data, {timeout = 30000, headers = {}, upload = {}, beforeSend = {}} = {}) {
  var options = {
    url: host + encodeURI(uri),
    method: method,
    timeout: timeout, 
    data: data,
    headers: headers,
    beforeSend: beforeSend,
    upload: upload,
    xhr: {
      withCredentials: true
    }
  }
  return Vue.http(options)
}