export default {
  en: {
    list: {
      name: 'Name',
      topoId: 'Topo ID',
      portName: 'Port',
      srcType: 'Type',
      mainUrl: 'Main Stream URL',
      previewUrl: 'Sub Stream URL',
      tag: 'Tag',
      displayName: 'DisplayName'
    }
  },
  cn: {
    list: {
      name: '名称',
      topoId: '拓扑ID',
      portName: '端口',
      srcType: '类型',
      mainUrl: '主码流地址',
      previewUrl: '次码流地址',
      tag: '标签',
      displayName: '显示名称'
    }
  }  
}