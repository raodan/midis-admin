import {
  SET_WEBAPPS,
  SET_WEBAPP_STATE,
  REFRESH_WEBAPPS_START,
  REFRESH_WEBAPPS_END,
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false,
}

// mutations
const mutations = {
  [SET_WEBAPPS] (state, val) {
    state.list = val
    state.init = true
  },
  [SET_WEBAPP_STATE] (state, id, val) {
    var item = _.find(state.list, {_id: id})
    if (item) {
      item.state = val
    }
  },
  [REFRESH_WEBAPPS_START] (state) {
    state.loading = true
  },
  [REFRESH_WEBAPPS_END] (state) {
    state.loading = false
  },
}

export default {
  state,
  mutations
}