import {
  ADD_STREAM_SRCS,
  SET_STREAM_SRC_CHECKED,
  SET_STREAM_SRC_ITEM_CHECKED,
  SET_STREAM_SRC_ALL_CHECKED,
  SET_STREAM_SRC_CUR_PAGE_CHECKED,
  SET_STREAM_SRC_ALL_UNCHECKED,
  SET_LOAD_STREAMSRCS_START,
  SET_LOAD_STREAMSRCS_END,
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_LOAD_STREAMSRCS_START] (state) {    
    state.loading = true
  },
  [SET_LOAD_STREAMSRCS_END] (state) {    
    state.loading = false
  },
  [ADD_STREAM_SRCS] (state, srcType, val) {
    console.log(srcType + 'Start:', new Date().toString())
    var streams = _.filter(state.list, (item) => {
      return item.srcType != srcType
    })
    state.list = _.concat(streams, val)
    console.log(srcType + 'End:', new Date().toString())
  },
  [SET_STREAM_SRC_CHECKED] (state, id, val) {
    var item = _.find(state.list, {_id: id})
    if (item) {
      item._checked = val
    }
  },
  [SET_STREAM_SRC_ITEM_CHECKED] (state, srcs, checked) {
    var count = 0; 
    _.forEach(state.list, item => {
      var res = _.find(srcs, src => {        
        return src.srcTopoId == item.topoId && src.srcPortName == item.portName
      })
      if (res) {        
        item._checked = checked
        count ++
      }
      if(srcs.length == count){
        return
      }
    })
    
  },
  [SET_STREAM_SRC_ALL_UNCHECKED] (state) {
    _.forEach(state.list, (item) => {
      item._checked = false
    })
  },
  [SET_STREAM_SRC_CUR_PAGE_CHECKED] (state, srcs, curIndex, count) {
    _.forEach(state.list, (item, index) => {            
      if(index < curIndex || index > curIndex + count){
        return 
      } else{
        var res = _.find(srcs, src => { 
          return src.topoId == item.topoId && src.portName == item.portName
        })
        if (res) { 
                 
          item._checked = true
        }
      }
    })
  },
  [SET_STREAM_SRC_ALL_CHECKED] (state) {
    _.forEach(state.list, (item) => {
      item._checked = true
    })
  },
}

export default {
  state,
  mutations
}