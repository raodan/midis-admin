import {
  SET_PLATFORM,
  SET_LICENSE_INFO,
	SET_LANGUAGE,
	SET_LOAD_HTTPSTATE
} from '../mutation-types'

// initial state
const state = {
  platform: {
    hardwareVersion: '',
    softwareVersion: '1.x.0',
    devType: 'server.main',
    devModel: '',
    limit: {}
  },
  state: {
    init: false,
    licenseInfo: {}
	},
	httpstate: {
		init: false,
		http: false
	},
  language: 'en'
}

// mutations
const mutations = {
  [SET_PLATFORM] (state, val) {
    state.platform = val
  },
  [SET_LICENSE_INFO] (state, val) {
    state.state.licenseInfo = val
    state.state.init = true
  },
  [SET_LANGUAGE] (state, val) {
    state.language = val
	},
	[SET_LOAD_HTTPSTATE] (state, val) {
		state.httpstate.http = val
		state.httpstate.init = true
	}
}

export default {
  state,
  mutations
}