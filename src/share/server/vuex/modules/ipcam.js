import {
  SET_IPCAMS,
  REFRESH_IPCAMS_START,
  REFRESH_IPCAMS_END,
  SET_IPCAM_VENDORS,
  REFRESH_IPCAM_VENDORS_START,
  REFRESH_IPCAM_VENDORS_END,
  SET_IPCAM_CHECKED,
  SET_IPCSTREAM_SRC_CHECKED,
  SET_IPCSTREAM_SRC_ALL_UNCHECKED,
  SET_IPCAM_ALL_UNCHECKED
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  ipcam: {
    list: [],
    loading: false,
    init: false
  },
  vendor: {
    list: [],
    loading: false,
    init: false
  }
}

// mutations
const mutations = {
  //ipcams
  [SET_IPCAMS] (state, val) {
    state.ipcam.list = val
    state.ipcam.init = true
  },
  [SET_IPCSTREAM_SRC_CHECKED] (state, id, val) {
    var item = _.find(state.ipcam.list, {_id: id})
    if (item) {
      item._checked = val
    }
  },
  [SET_IPCSTREAM_SRC_ALL_UNCHECKED] (state) {
    _.forEach(state.ipcam.list, (item) => {
      item._checked = false
    })
  },
  [REFRESH_IPCAMS_START] (state) {
    state.ipcam.loading = true
  },
  [REFRESH_IPCAMS_END] (state) {
    state.ipcam.loading = false
  },
  //ipcam vendors
  [SET_IPCAM_VENDORS] (state, val) {
    state.vendor.list = val
    state.vendor.init = true
  },
  [REFRESH_IPCAM_VENDORS_START] (state) {
    state.vendor.loading = true
  },
  [REFRESH_IPCAM_VENDORS_END] (state) {
    state.vendor.loading = false
  },
  [SET_IPCAM_CHECKED] (state, id, val) {
    var item = _.find(state.ipcam.list, {_id: id})
    if (item) {
      item._checked = val
    }
  },
  [SET_IPCAM_ALL_UNCHECKED] (state) {
    _.forEach(state.ipcam.list, (item) => {
      item._checked = false
    })
  },
}

export default {
  state,
  mutations
}