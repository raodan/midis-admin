import {
  SET_MTSRCS,
  REFRESH_MTSRCS_START,
  REFRESH_MTSRCS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_MTSRCS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_MTSRCS_START] (state) {
    state.loading = true
  },
  [REFRESH_MTSRCS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}