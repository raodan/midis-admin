import {
  SET_AUTHED,
  SET_AUTH_USERNAME,
  SET_AUTH_REQ, 
  SET_AUTH_CLIENT,

  SET_USERS,
  REFRESH_USERS_START,
  REFRESH_USERS_END,
} from '../mutation-types'

// initial state
const state = {
  username: '',
  req: {},
  authed: false,
  client: {},
  user: {
    list: [],
    loading: false,
    init: false
  }
}

// mutations
const mutations = {
  [SET_AUTHED] (state, val) {
    state.authed = val
  },
  [SET_AUTH_USERNAME] (state, val) {
    state.username = val
  },
  [SET_AUTH_REQ] (state, val) {
    state.req = val
  },
  [SET_AUTH_CLIENT] (state, val) {
    state.client = val
  },

  //users
  [SET_USERS] (state, val) {
    state.user.list = val
    state.user.init = true
  },
  [REFRESH_USERS_START] (state) {
    state.user.loading = true
  },
  [REFRESH_USERS_END] (state) {
    state.user.loading = false
  },
}

export default {
  state,
  mutations
}