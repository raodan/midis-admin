import {
  SET_VID_LAYOUTS,
  REFRESH_VID_LAYOUTS_START,
  REFRESH_VID_LAYOUTS_END,
  SET_VID_LAYOUT_CHECKED,
  SET_VID_LAYOUT_ALL_UNCHECKED
} from '../mutation-types'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_VID_LAYOUTS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_VID_LAYOUTS_START] (state) {
    state.loading = true
  },
  [REFRESH_VID_LAYOUTS_END] (state) {
    state.loading = false
  },
  [SET_VID_LAYOUT_CHECKED] (state, id, val) {
    var item = _.find(state.list, {_id: id})
    if (item) {
      item._checked = val
    }
  },
  [SET_VID_LAYOUT_ALL_UNCHECKED] (state) {
    _.forEach(state.list, (item) => {
      item._checked = false
    })
  }
}

export default {
  state,
  mutations
}