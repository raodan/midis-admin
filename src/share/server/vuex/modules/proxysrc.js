import {
  SET_PROXYSRCS,
  REFRESH_PROXYSRCS_START,
  REFRESH_PROXYSRCS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_PROXYSRCS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_PROXYSRCS_START] (state) {
    state.loading = true
  },
  [REFRESH_PROXYSRCS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}