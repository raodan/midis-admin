import {
  SET_LCD_TOPO_DEV,
  SET_LCD_MODES,
  SET_LCD_MODE_ID,
  SET_LCD_V_TOPO_ID,
  SET_LCD_SWITCH_LAYOUT,
  SET_LCD_MULTISCREEN,
  SET_LCD_BASE,
} from '../mutation-types'

// initial state
const state = {
  topoDev: {
    topoId: -1
  },
  modes: [],
  modeId: 0,
  switchLayout: {
    enable: 0,
    interval: 5
  },
  virtualTopoId: -1,
  multiScreenX: -1,
  multiScreenY: -1,
  base: {}
}

// mutations
const mutations = {
  [SET_LCD_TOPO_DEV] (state, val) {
    state.topoDev = val
  },
  [SET_LCD_MODES] (state, val) {
    state.modes = val
  },
  [SET_LCD_MODE_ID] (state, val) {
    state.modeId = val
  },
  [SET_LCD_V_TOPO_ID] (state, val) {
    state.virtualTopoId = val
  },
  [SET_LCD_SWITCH_LAYOUT] (state, val) {
    state.switchLayout.enable = val.enable
    state.switchLayout.interval = val.interval
  },
  [SET_LCD_MULTISCREEN] (state, multiScreenX, multiScreenY) {
    state.multiScreenX = multiScreenX
    state.multiScreenY = multiScreenY
  },
  [SET_LCD_BASE] (state, base) {
    state.base = base
  }
}

export default {
  state,
  mutations
}