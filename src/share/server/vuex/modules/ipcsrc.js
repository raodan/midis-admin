import {
  SET_IPCSRCS,
  REFRESH_IPCSRCS_START,
  REFRESH_IPCSRCS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_IPCSRCS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_IPCSRCS_START] (state) {
    state.loading = true
  },
  [REFRESH_IPCSRCS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}