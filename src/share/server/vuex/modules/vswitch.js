import {
  SET_VSWITCHS,
  REFRESH_VSWITCHS_START,
  REFRESH_VSWITCHS_END,
  SET_VSWITCH_CFG,
  SET_VSWITCH_LCD_TOPO_DEV,
  SET_VSWITCH_LCD_MODES,
  SET_VSWITCH_LCD_MODE_ID,
  SET_VSWITCH_LCD_V_TOPO_ID,  
  SET_VSWITCH_LCD_MULTISCREEN,
  SET_VSRCSETS_CFG,
} from '../mutation-types'

// initial state
const state = {
  init: false,
  list: [],
  loading: false,
  cfg: {
    _id: -1,
    id: 'default',
    name: 'default',
    lcdTopoId: -1,
    realLcdTopoId: -1,
    vLayout: {
      id: '',
      multiScreenX: 1,        
      multiScreenY: 1,
      windowList: '[]'           
    },
    srcListType: 'custom',
    srcTypeList: '[]',
    customSrcList: '[]',
    enable: 0,
    interval: 10 
  },
  topoDev: {
    topoId: -1
  },
  modes: [],
  modeId: 0,
  virtualTopoId: -1,
  multiScreenX: -1,
  multiScreenY: -1,
}

function updateLcd(state) {
  if ((state.virtualTopoId > 0)
    && (state.modes.length > 0)) {
    for (var i = 0; i < state.modes.length; i++) {
      let mode = state.modes[i]
      if (mode && mode.areas && mode.areas.length > 0) {
        for (var j = 0; j < mode.areas.length; j++) {
          let area = mode.areas[j]
          if (area.virtualTopoId === state.virtualTopoId) {
            state.modeId = i + 1
            return
          }
        }
      }
    }
  }
}

// mutations
const mutations = {
  [SET_VSWITCHS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_VSWITCHS_START] (state) {
    state.loading = true
  },
  [REFRESH_VSWITCHS_END] (state) {
    state.loading = false
  },
  [SET_VSWITCH_CFG] (state, val) {
    state.cfg._id = val._id
    state.cfg.id = val.id
    state.cfg.name = val.name
    state.cfg.lcdTopoId = val.lcdTopoId
    state.cfg.realLcdTopoId = val.realLcdTopoId
    state.cfg.mainWindowId = val.mainWindowId
    state.cfg.srcListType = val.srcListType
    state.cfg.srcTypeList = val.srcTypeList
    state.cfg.customSrcList = val.customSrcList
    state.cfg.enable = val.enable
    state.cfg.interval = val.interval

    state.cfg.vLayout.id = val.vLayout.id
    state.cfg.vLayout.multiScreenX = val.vLayout.multiScreenX
    state.cfg.vLayout.multiScreenY = val.vLayout.multiScreenY
    state.cfg.vLayout.windowList = val.vLayout.windowList    
  },
  [SET_VSRCSETS_CFG] (state, val){
    state.cfg = val
  },
  [SET_VSWITCH_LCD_TOPO_DEV] (state, val) {
    state.topoDev = val
  },
  [SET_VSWITCH_LCD_MODES] (state, val) {
    state.modes = val
    updateLcd(state)
  },
  [SET_VSWITCH_LCD_MODE_ID] (state, val) {
    state.modeId = val
  },
  [SET_VSWITCH_LCD_V_TOPO_ID] (state, val) {
    state.virtualTopoId = val
    updateLcd(state)
  },
  [SET_VSWITCH_LCD_MULTISCREEN] (state, multiScreenX, multiScreenY) {
    state.multiScreenX = multiScreenX
    state.multiScreenY = multiScreenY
  },
}

export default {
  state,
  mutations
}