import {
  SET_RECORD_ROOMS,
  REFRESH_RECORD_ROOMS_START,
  REFRESH_RECORD_ROOMS_END,
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  room: {
    init: false,
    list: [],
    loading: false,
  }
}

// mutations
const mutations = {
  [SET_RECORD_ROOMS] (state, val) {
    state.room.list = val
    state.room.init = true
  },
  [REFRESH_RECORD_ROOMS_START] (state) {
    state.room.loading = true
  },
  [REFRESH_RECORD_ROOMS_END] (state) {
    state.room.loading = false
  },
}

export default {
  state,
  mutations
}