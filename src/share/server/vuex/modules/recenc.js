import {
  SET_RECENCS,
  REFRESH_RECENCS_START,
  REFRESH_RECENCS_END,
  SET_RECENC_VENDORS,
  SET_RECENC_CHECKED,
  SET_RECENC_ALL_UNCHECKED,
  SET_RECENC_OPERATETION_DETAIL,
  SET_RECORD_PLATFORM,
  REFRESH_PLATFORM_START,
  REFRESH_PLATFORM_END,
  SET_RECORD_PLATROOM,
  REFRESH_PLATROOM_START,
  REFRESH_PLATROOM_END,
  SEND_PLATFORMDATA_TOCHILD,
  SET_VSRCSETS,
  REFRESH_VSRCSETS_START,
  REFRESH_VSRCSETS_END,
  SET_WLAYOUTS,
  REFRESH_WLAYOUT_START,
  REFRESH_WLAYOUT_END,
  SET_WINDOWSWITCH,
  REFRESH_WINDOWSWITCH_START,
  REFRESH_WINDOWSWITCH_END,
  SET_SRCNAME,
  SET_LCDSTATE,
  REFRESH_LCDSTATE_START,
  REFRESH_LCDSTATE_END,
  
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  recenc: {
    init: false,
    list: [],
    loading: false,
  },
  platform: {
    init: false,
    list: [],
    loading: false,
  },
  platroom:{
    init: false,
    list: [],
    loading: false,
  },
  vendor: {
    init: false,
    list: []
  },
  platformchild: {
    init: false,
    list: []
  },
  srcsets:{
    init: false,
    list: [],
    loading: false,
  },
  wlayout:{
    init: false,
    list: [],
    loading: false,
  },
  windowswitch:{
    init: false,
    list: [],
    loading: false,
  },
  cfg: {
      _id: -1,
      name: 'default',
      recChnCnt: '',
      manufacture: '',
      productModel: '',
      streams: [],
      topoIdOfMediaDev: -1,
      recChnStates: [],
  },
  lcdstate:{
    state:''
  }
}

// mutations
const mutations = {
  //recencs
  [SET_RECENCS] (state, val) {
    state.recenc.list = val
    state.recenc.init = true
  },
  [REFRESH_RECENCS_START] (state) {
    state.recenc.loading = true
  },
  [REFRESH_RECENCS_END] (state) {
    state.recenc.loading = false
  },
  //platforms
  [SET_RECORD_PLATFORM] (state, val) {
    
    state.platform.list = val
    state.platform.init = true
  },
  [REFRESH_PLATFORM_START] (state) {
    state.platform.loading = true
  },
  [REFRESH_PLATFORM_END] (state) {
    state.platform.loading = false
  },
  //platrooms
  [SET_RECORD_PLATROOM] (state, val) {
    
    state.platroom.list = val
    state.platroom.init = true
  },
  [REFRESH_PLATROOM_START] (state) {
    state.platroom.loading = true
  },
  [REFRESH_PLATROOM_END] (state) {
    state.platroom.loading = false
  },
  //send data to child
  [SEND_PLATFORMDATA_TOCHILD] (state, val) {
    state.platformchild.list = val
  },
  //recencVendorCfgs
  [SET_RECENC_VENDORS] (state, val) {
    state.vendor.list = val
    state.vendor.init = true
  },
  [SET_RECENC_CHECKED] (state, id, val) {
    var item = _.find(state.recenc.list, {_id: id})
    if (item) {
      item._checked = val
    }
  },
  [SET_RECENC_ALL_UNCHECKED] (state) {
    _.forEach(state.recenc.list, (item) => {
      item._checked = false
    })
  },
  [SET_RECENC_OPERATETION_DETAIL] (state, val) {
    state.cfg._id = val._id
    state.cfg.name = val.name
    state.cfg.recChnCnt = val.recChnCnt
    state.cfg.manufacture = val.manufacture
    state.cfg.productModel = val.productModel
    state.cfg.streams = val.streams
    state.cfg.topoIdOfMediaDev = val.topoIdOfMediaDev
    state.cfg.recChnStates = val.recChnStates
  },
  [SET_VSRCSETS] (state, val){
    
    state.srcsets.list = val
    state.srcsets.init = true
  },
  [REFRESH_VSRCSETS_START] (state) {
    state.srcsets.loading = true
  },
  [REFRESH_VSRCSETS_END] (state) {
    state.srcsets.loading = false
  },
  [SET_WLAYOUTS] (state, val) {
    state.wlayout.list = val
    state.wlayout.init = true
  },
  [REFRESH_WLAYOUT_START] (state) {
    state.wlayout.loading = true
  },
  [REFRESH_WLAYOUT_END] (state) {
    state.wlayout.loading = false
  },
  [SET_WINDOWSWITCH] (state, val){
    state.windowswitch.list = val
    state.windowswitch.init = true
  },
  [REFRESH_WINDOWSWITCH_START] (state) {
    state.windowswitch.loading = true
  },
  [REFRESH_WINDOWSWITCH_END] (state) {
    state.windowswitch.loading = false
  },
  [SET_LCDSTATE] (state, val) {
    
    state.lcdstate.state = val
  },
  [REFRESH_LCDSTATE_START] (state) {
    state.lcdstate.loading = true
  },
  [REFRESH_LCDSTATE_END] (state) {
    state.lcdstate.loading = false
  },
  [SET_SRCNAME](id,val){
    
    var item = _.find(state.wlayout.list, {srcSetId: id})
    if (item) {
      item.srcName = val
    }
  }
}

export default {
  state,
  mutations
}