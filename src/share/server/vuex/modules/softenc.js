import {
  SET_SOFTENCS,
  REFRESH_SOFTENCS_START,
  REFRESH_SOFTENCS_END,
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false,
}

// mutations
const mutations = {
  [SET_SOFTENCS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_SOFTENCS_START] (state) {
    state.loading = true
  },
  [REFRESH_SOFTENCS_END] (state) {
    state.loading = false
  },
}

export default {
  state,
  mutations
}