import {
  SET_ENCODERS,
  REFRESH_ENCODERS_START,
  REFRESH_ENCODERS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_ENCODERS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_ENCODERS_START] (state) {
    state.loading = true
  },
  [REFRESH_ENCODERS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}