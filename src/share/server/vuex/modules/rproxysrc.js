import {
  SET_RPROXYSRCS,
  REFRESH_RPROXYSRCS_START,
  REFRESH_RPROXYSRCS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_RPROXYSRCS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_RPROXYSRCS_START] (state) {
    state.loading = true
  },
  [REFRESH_RPROXYSRCS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}