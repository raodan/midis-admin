import {
  SET_CAPSTREAMS,
  REFRESH_CAPSTREAMS_START,
  REFRESH_CAPSTREAMS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_CAPSTREAMS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_CAPSTREAMS_START] (state) {
    state.loading = true
  },
  [REFRESH_CAPSTREAMS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}