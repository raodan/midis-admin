import {
  SET_ENCODER102ES,
  REFRESH_ENCODER102ES_START,
  REFRESH_ENCODER102ES_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_ENCODER102ES] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_ENCODER102ES_START] (state) {
    state.loading = true
  },
  [REFRESH_ENCODER102ES_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}