import {
  SET_MONITORS,
  REFRESH_MONITORS_START,
  REFRESH_MONITORS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  list: [],
  loading: false,
  init: false
}

// mutations
const mutations = {
  [SET_MONITORS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_MONITORS_START] (state) {
    state.loading = true
  },
  [REFRESH_MONITORS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}