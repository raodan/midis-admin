import {
  SET_CODECS,
  REFRESH_CODECS_START,
  REFRESH_CODECS_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_CODECS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_CODECS_START] (state) {
    state.loading = true
  },
  [REFRESH_CODECS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}