import {
  SET_MEDIA_TOPODEVS
} from '../mutation-types'

// initial state
const state = {
  topoDevs: {
    init: false,
    list: []
  }
}

// mutations
const mutations = {
  [SET_MEDIA_TOPODEVS] (state, val) {
    console.log('media.val',val)
    state.topoDevs.list = val
    state.topoDevs.init = true
  }
}

export default {
  state,
  mutations
}