import {
  SET_PCLIST,
  REFRESH_PCLIST_START,
  REFRESH_PCLIST_END
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_PCLIST] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_PCLIST_START] (state) {
    state.loading = true
  },
  [REFRESH_PCLIST_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}