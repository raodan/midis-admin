import {
  SET_CASCADE_TOPO_DEV,
  SET_CASCADE_LDEVS,
  SET_CASCADESRCS,
  REFRESH_CASCADESRCS_START,
  REFRESH_CASCADESRCS_END,  
  SET_ENCODERMCNS,
  REFRESH_ENCODERMCNS_START,
  REFRESH_ENCODERMCNS_END,  
  SET_ENCODERTCNS,
  REFRESH_ENCODERTCNS_START,
  REFRESH_ENCODERTCNS_END,
  SET_TCNIPCS,
  REFRESH_TCNIPCS_START,
  REFRESH_TCNIPCS_END
} from '../mutation-types'

// initial state
const state = {
  topoDev: {
    topoId: -1
  },
  lDevs: {
    init: false,
    list: []
  },
  src: {
    init: false,
    list: [],
    loading: false    
  },
  mcns: {
    init: false,
    list: [],
    loading: false    
  },
  tcns: {
    init: false,
    list: [],
    loading: false    
  },
  tcnIpcs: {
    init: false,
    list: [],
    loading: false    
  }
}

// mutations
const mutations = {
  [SET_CASCADE_TOPO_DEV] (state, val) {
    state.topoDev = val
  },
  [SET_CASCADE_LDEVS] (state, val) {
    state.lDevs.list = val
    state.lDevs.init = true
  },
  [SET_CASCADESRCS] (state, val) {
    state.src.list = val
    state.src.init = true
  },
  [REFRESH_CASCADESRCS_START] (state) {
    state.src.loading = true
  },
  [REFRESH_CASCADESRCS_END] (state) {
    state.src.loading = false
  },
  [SET_ENCODERMCNS] (state, val) {
    state.mcns.list = val
    state.mcns.init = true
  },
  [REFRESH_ENCODERMCNS_START] (state) {
    state.mcns.loading = true
  },
  [REFRESH_ENCODERMCNS_END] (state) {
    state.mcns.loading = false
  },
  [SET_ENCODERTCNS] (state, val) {
    state.tcns.list = val
    state.tcns.init = true
  },
  [REFRESH_ENCODERTCNS_START] (state) {
    state.tcns.loading = true
  },
  [REFRESH_ENCODERTCNS_END] (state) {
    state.tcns.loading = false
  },
  [SET_TCNIPCS] (state, val) {
    state.tcnIpcs.list = val
    state.tcnIpcs.init = true
  },
  [REFRESH_TCNIPCS_START] (state) {
    state.tcnIpcs.loading = true
  },
  [REFRESH_TCNIPCS_END] (state) {
    state.tcnIpcs.loading = false
  }
}

export default {
  state,
  mutations
}