import {
  SET_VLAYOUTS,
  REFRESH_VLAYOUTS_START,
  REFRESH_VLAYOUTS_END
} from '../mutation-types'

// initial state
const state = {
  init: false,
  list: [],
  loading: false
}

// mutations
const mutations = {
  [SET_VLAYOUTS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_VLAYOUTS_START] (state) {
    state.loading = true
  },
  [REFRESH_VLAYOUTS_END] (state) {
    state.loading = false
  }
}

export default {
  state,
  mutations
}