import {
  SET_SUPERHDSCREENS,
  REFRESH_SUPERHDSCREENS_START,
  REFRESH_SUPERHDSCREENS_END,
} from '../mutation-types'

import _ from 'lodash'

// initial state
const state = {
  init: false,
  list: [],
  loading: false,
}

// mutations
const mutations = {
  [SET_SUPERHDSCREENS] (state, val) {
    state.list = val
    state.init = true
  },
  [REFRESH_SUPERHDSCREENS_START] (state) {
    state.loading = true
  },
  [REFRESH_SUPERHDSCREENS_END] (state) {
    state.loading = false
  },
}

export default {
  state,
  mutations
}