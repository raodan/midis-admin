import {
  SET_DDIS_DEVS,
  REFRESH_DDIS_DEVS_START,
  REFRESH_DDIS_DEVS_END,
  SET_TOPO_SUMMARY,
  SET_TOPO_BINDS,
  SET_DEVMODEL_SUMMARY,
} from '../mutation-types'

// initial state
const state = {
  ddis: {
    list: [],
    loading: false,
    init: false
  },
  topoSummary: {
    init: false,
    list: []
  },
  topoBinds: {
    init: false,
    list: []
  },
  devModelsSummary: {
    init: false,
    list: []
  }
}

// mutations
const mutations = {
  //ddis
  [SET_DDIS_DEVS] (state, val) {
    state.ddis.list = val
    state.ddis.init = true
  },
  [REFRESH_DDIS_DEVS_START] (state) {
    state.ddis.loading = true
  },
  [REFRESH_DDIS_DEVS_END] (state) {
    state.ddis.loading = false
  },
  //topoSummary
  [SET_TOPO_SUMMARY] (state, val) {
    state.topoSummary.list = val
    state.topoSummary.init = true
  },
  //topoBinds
  [SET_TOPO_BINDS] (state, val) {
    state.topoBinds.list = val
    state.topoBinds.init = true
  },
  //devModelsSummary
  [SET_DEVMODEL_SUMMARY] (state, val) {
    state.devModelsSummary.list = val
    state.devModelsSummary.init = true
  },
}

export default {
  state,
  mutations
}