import _ from 'lodash'
import ebus from 'libs/ebus.js'
import {multimediaApi, mediaPhyApi, devmngApi, ssl} from '../../../webapp/api.js'
import * as types from '../mutation-types'

//vid layout
function makeVidLayout(vidLayout) {
  vidLayout._checked = false
  vidLayout.multiScreen = vidLayout.multiScreenX + 'x' + vidLayout.multiScreenY
}

export function doLoadVidLayouts({dispatch, state}, {init = false} = {}) {
  if (init && state.vidlayout.init) {
    return
  }
  dispatch(types.REFRESH_VID_LAYOUTS_START)
  multimediaApi.getItems('vidLayouts').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var vidLayouts = res.data.data.vidLayouts
      _.forEach(vidLayouts, (item) => {
        makeVidLayout(item)
      })
      dispatch(types.SET_VID_LAYOUTS, vidLayouts)
    }
    dispatch(types.REFRESH_VID_LAYOUTS_END)
  })
}

export function setSrcName({dispatch}, id, val) {
  dispatch(types.SET_SRCNAME, id, val)
}

export function setVidLayoutChecked({dispatch}, id, val) {
  dispatch(types.SET_VID_LAYOUT_CHECKED, id, val)
}

export function setVidLayoutAllUnchecked({dispatch}) {
  dispatch(types.SET_VID_LAYOUT_ALL_UNCHECKED)
}

//vlayout
function makeVLayout(vLayout) {
  vLayout.multiScreen = vLayout.multiScreenX + 'x' + vLayout.multiScreenY
}

export function doLoadVLayouts({dispatch, state}, {init = false} = {}) {
  if (init && state.vlayout.init) {
    return
  }
  dispatch(types.REFRESH_VLAYOUTS_START)
  multimediaApi.getItems('vLayouts').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var vLayouts = res.data.data.vLayouts
      _.forEach(vLayouts, (item) => {
        makeVLayout(item)
      })
      dispatch(types.SET_VLAYOUTS, vLayouts)
    }
    dispatch(types.REFRESH_VLAYOUTS_END)
  })
}

//vswitch
export function doLoadVSwitchs({dispatch, state}, {init = false} = {}) {
  if (init && state.vswitch.init) {
    return
  }
  dispatch(types.REFRESH_VSWITCHS_START)
  multimediaApi.getItems('vSwitchs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var vSwitchs = res.data.data.vSwitchs
      dispatch(types.SET_VSWITCHS, vSwitchs)
    }
    dispatch(types.REFRESH_VSWITCHS_END)
  })
}

export function setVSwitchCfg({dispatch}, val) {
  dispatch(types.SET_VSWITCH_CFG, val)
}

export function setVSrcsetsCfg({dispatch}, val) {
  dispatch(types.SET_VSRCSETS_CFG, val)
}


export function setVSwitchLcdTopoDev({dispatch}, val) {
  dispatch(types.SET_VSWITCH_LCD_TOPO_DEV, val)
}

export function updateVSwitchLcdModes({dispatch, state}) {
  let topoDev = state.vswitch.topoDev
  if (topoDev.topoId < 0) {
    return
  }
  if (!topoDev.devModel.includes('lcdwall')) {
    return
  }
  mediaPhyApi.setDevId(topoDev.topoId)
  mediaPhyApi.sendCmd('getModes').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var modes = []
      if (res.data.data.modes) {
        modes = res.data.data.modes
      }
      dispatch(types.SET_VSWITCH_LCD_MODES, modes)
    }
  })  
}

export function setVSwitchLcdModeId({dispatch, state}, val) {
  dispatch(types.SET_VSWITCH_LCD_MODE_ID, val)
}

export function setVSwitchLcdVTopoId({dispatch}, val) {
  dispatch(types.SET_VSWITCH_LCD_V_TOPO_ID, val)
}

export function setVSwitchLcdMultiScreen({dispatch, state}, x, y) {
  dispatch(types.SET_VSWITCH_LCD_MULTISCREEN, x, y)
}

export function updateVSwitchLcdMultiScreen({dispatch, state}) {
  var lcdTopoId = state.vswitch.topoDev.topoId
  if (lcdTopoId < 0) {
    return
  }  
  mediaPhyApi.setDevId(lcdTopoId)
  mediaPhyApi.getState().then(function(res) {
    if (res && res.data && res.data.code === 0) {
      let _display = res.data.data._display
      if (_display) {
        dispatch(types.SET_VSWITCH_LCD_MULTISCREEN, _display.multiScreenX, _display.multiScreenY)
      }
    }
  })
}

//lcd window
export function setLcdTopoDev({dispatch}, val) {
  dispatch(types.SET_LCD_TOPO_DEV, val)
}

export function updateLcdModes({dispatch, state}) {
  let topoDev = state.lcd.topoDev
  if (topoDev.topoId < 0) {
    return
  }
  // if (!topoDev.devModel.includes('lcdwall') && !topoDev.devModel.includes('lcd.dyn')) {
    if (!topoDev.devModel.includes('lcdwall')) {
    return
  }
  mediaPhyApi.setDevId(topoDev.topoId)
  mediaPhyApi.sendCmd('getModes').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var modes = []
      if (res.data.data.modes) {
        modes = res.data.data.modes
      }
      dispatch(types.SET_LCD_MODES, modes)
      ebus.emit('lcd-mode-updated')
    }
  })  
}

export function setLcdModeId({dispatch, state}, val) {
  dispatch(types.SET_LCD_MODE_ID, val)
}

export function setLcdVTopoId({dispatch}, val) {
  dispatch(types.SET_LCD_V_TOPO_ID, val)
}

export function getLcdCfg({dispatch, state}, keepModeId) {
  var lcdTopoId = state.lcd.virtualTopoId > 0 
    ? state.lcd.virtualTopoId : state.lcd.topoDev.topoId
  
  if (lcdTopoId < 0) {
    return
  }
  mediaPhyApi.setDevId(lcdTopoId)
  mediaPhyApi.getCfg().then(function(res) {
    if (res && res.data && res.data.code === 0) {
      let _info = res.data.data._info
      if (!keepModeId) {
        _info.hasOwnProperty('modeId') && dispatch(types.SET_LCD_MODE_ID, _info.modeId)
        ebus.emit('lcd-mode-updated')
      }
      _info.hasOwnProperty('switchLayout') && dispatch(types.SET_LCD_SWITCH_LAYOUT, _info.switchLayout)
    }
  }) 
}

export function setLcdMultiScreen({dispatch, state}, x, y) {
  dispatch(types.SET_LCD_MULTISCREEN, x, y)
}

export function updateLcdMultiScreen({dispatch, state}) {
  var lcdTopoId = state.lcd.topoDev.topoId
  if (lcdTopoId < 0) {
    return
  }  
  mediaPhyApi.setDevId(lcdTopoId)
  mediaPhyApi.getState().then(function(res) {
    if (res && res.data && res.data.code === 0) {
      let _display = res.data.data._display
      let _base = res.data.data._info
      if (_display) {
        dispatch(types.SET_LCD_MULTISCREEN, _display.multiScreenX, _display.multiScreenY)
      }
      if (_base) {
        dispatch(types.SET_LCD_BASE, _base)
      }
    }
  })
}

//recenc
export function doLoadRecEncs({dispatch, state}, {init = false} = {}) {
  if (init && state.recenc.recenc.init) {
    return
  }
  dispatch(types.REFRESH_RECENCS_START)
  multimediaApi.getItems('recencs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      let recencs = res.data.data.recencs
      dispatch(types.SET_RECENCS, recencs)
    }
    dispatch(types.REFRESH_RECENCS_END)
  })
}

//ddis dev
function makePlatformLink(item) {  
   item.link = `<a href="${ssl}://${item.ipaddr}/${item.username}#/admin/index" target="_blank">Home</a>`   
  //  item.link = `<a href="http://${item.ipaddr}/admin#/admin/auto?u=${item.username}&p=${item.password} " target="_blank">Home</a>` 
 }
 function makePlatformChildLink(item) {
  
   item.link = `<a href="${ssl}://${item.ipaddr}/${item.userName}#/admin/index" target="_blank">Home</a>` 
  // item.link = `<a href="http://${item.ipaddr}/admin#/admin/auto?u=${item.userName}&p=${item.password} " target="_blank">Home</a>` 
 }
//platform
export function doLoadPlatforms({dispatch, state}, {init = false} = {}) {
  if (init && state.recenc.platform.init) {
    return
  }
  dispatch(types.REFRESH_PLATFORM_START)
  multimediaApi.getItems('recPlatDevs').then(function(res) {  
     
    if (res && res.data && res.data.code === 0) {  
           
      let recPlatDevs = res.data.data.recPlatDevs
      _.forEach(recPlatDevs, (item) => {
        makePlatformLink(item)
      })
      dispatch(types.SET_RECORD_PLATFORM, recPlatDevs)
      
    }
    dispatch(types.REFRESH_PLATFORM_END)
  })
}

//platroom
export function doLoadPlatrooms({dispatch, state}, {init = false} = {}) {
  if (init && state.recenc.platroom.init) {
    return
  }
  dispatch(types.REFRESH_PLATROOM_START)
  multimediaApi.getItems('classRooms').then(function(res) {
    
    if (res && res.data && res.data.code === 0) {
      let classRooms = res.data.data.classRooms
      dispatch(types.SET_RECORD_PLATROOM, classRooms)
    }
    dispatch(types.REFRESH_PLATROOM_END)
  })
}

//srcsets
export function doLoadSrcsets({dispatch, state}, {init = false} = {}) {
  if (init && state.recenc.srcsets.init) {
    return
  }
  dispatch(types.REFRESH_SRCSETS_START)
  multimediaApi.getItems('srcSets').then(function(res) {
    
    if (res && res.data && res.data.code === 0) {
      let srcSets = res.data.data.srcSets
      dispatch(types.SET_VSRCSETS, srcSets)
    }
    dispatch(types.REFRESH_SRCSETS_END)
  })
}


//windowswitch
export function doLoadWindowSwitch({dispatch, state}, {init = false} = {}) {
  if (init && state.recenc.windowswitch.init) {
    return
  }
  dispatch(types.REFRESH_WINDOWSWITCH_START)
  multimediaApi.getItems('wSwitchs').then(function(res) {
    
    if (res && res.data && res.data.code === 0) {
      let wSwitchs = res.data.data.wSwitchs
      dispatch(types.SET_WINDOWSWITCH, wSwitchs)
    }
    dispatch(types.REFRESH_WINDOWSWITCH_END)
  })
}

//windowLayout
export function doLoadWindowLayout({dispatch, state}, {init = false} = {}) {
  var lcdTopoId = state.lcd.virtualTopoId > 0 
  ? state.lcd.virtualTopoId : state.lcd.topoDev.topoId
  if (init && state.recenc.wlayout.init) {
    return
  }
  dispatch(types.REFRESH_WLAYOUT_START)  
  mediaPhyApi.setDevId(lcdTopoId)
  mediaPhyApi.sendCmd('getWLayout').then(function(res) {    
    
    if (res && res.data && res.data.code === 0) {
      let switchSets = res.data.data.switchSets
      dispatch(types.SET_WLAYOUTS, switchSets)
    }
    dispatch(types.REFRESH_WLAYOUT_END)
  }.bind(this))
}

//windowLayout
export function doLoadLcdState({dispatch, state}, {init = false} = {}) {
  dispatch(types.REFRESH_LCDSTATE_START)
  mediaPhyApi.getState('state').then(function(res) {    
    if (res && res.data && res.data.code === 0) {
      if(res.data.data._info){
        let lcdState = res.data.data._info.operationMode ? res.data.data._info.operationMode : '';

        dispatch(types.SET_LCDSTATE, lcdState)
      }      
    }
    dispatch(types.REFRESH_LCDSTATE_END)
  })
}

//platform: parent to child
export function sendPlatformDataToChild({dispatch}, val) {
  var recPlatDevs = val
  _.forEach(recPlatDevs, (item) => {
    makePlatformChildLink(item)
  })
  dispatch(types.SEND_PLATFORMDATA_TOCHILD, recPlatDevs)
}

export function setRecEncChecked({dispatch}, id, val) {
  dispatch(types.SET_RECENC_CHECKED, id, val)
}

export function setRecEncAllUnchecked({dispatch}) {
  dispatch(types.SET_RECENC_ALL_UNCHECKED)
}

export function doLoadRecEncVendors({dispatch, state}, {init = false} = {}) {
  if (init && state.recenc.vendor.init) {
    return
  }
  multimediaApi.getItems('recencVendorCfgs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      let vendors = res.data.data.recencVendorCfgs
      dispatch(types.SET_RECENC_VENDORS, vendors)
    }
  })
}

//pclist
export function doLoadPcList({dispatch, state}, {init = false} = {}) {
  if (init && state.pclist.init) {
    return
  }
  dispatch(types.REFRESH_PCLIST_START)
  multimediaApi.getItems('computers').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      let computers = res.data.data.computers
      dispatch(types.SET_PCLIST, computers)
    }
    dispatch(types.REFRESH_PCLIST_END)
  })
}