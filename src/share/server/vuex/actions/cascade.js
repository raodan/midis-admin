import _ from 'lodash'
import ebus from 'libs/ebus.js'
import {cascadeApi} from '../../api.js'
import {cascadeTypeApi} from '../../../webapp/api.js'
import * as types from '../mutation-types'

export function setCascadeTopoDev({dispatch}, val) {
  dispatch(types.SET_CASCADE_TOPO_DEV, val)
}

export function doLoadCascadeLDevs({dispatch, state}, {init = false} = {}) {
  if (init && state.cascade.lDevs.init) {
    return
  }
  cascadeTypeApi.getItems('topoDevs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_CASCADE_LDEVS, res.data.data.topoDevs)
    }
  })
}
