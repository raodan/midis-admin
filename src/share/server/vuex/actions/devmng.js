import _ from 'lodash'
import ebus from 'libs/ebus.js'
import {devmngApi, ssl} from '../../../webapp/api.js'
import * as types from '../mutation-types'

//ddis dev
function makeDdisDev(ddisDev,state) {
  var port = 8080
  let showType = ['encoder','decoder','edfd','cdev.transcoder','edu-fd','edu.gen-fd','edu-codyy'];
  let showType1 = ['cdev.encoder','cdev.decoder'];
  let lists = state.devmng.devModelsSummary.list;
  let curdev=lists.filter(v=>{

    if(v.devModel.includes("*")) {
      return ddisDev.devModel === v.devModel.slice(0,v.devModel.length-1);
    }
    return v.devModel === ddisDev.devModel
  } )
  
   if (ddisDev.devType === 'server.media') {
    port = 6080
  } else if (ddisDev.devType === 'server.main') {
    port = 9080
  } else if(_.includes(showType, ddisDev.devType) || (curdev[0] && curdev[0].home)){
    port = 8080
  } else if(_.includes(showType1, ddisDev.devType) || (curdev[0] && curdev[0].home)){
    port = 9080
  }else {
    return
  }
  ddisDev.link = `<a href="http://${ddisDev.ipaddr}:${port}" target="_blank">Home</a>`
 
}

export function doLoadDdisDevs({dispatch, state}, init) {
  if (init && state.devmng.ddis.init) {
    return
  }
  dispatch(types.REFRESH_DDIS_DEVS_START)
  devmngApi.getItems('ddisDevs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var ddisDevs = res.data.data.ddisDevs
      _.forEach(ddisDevs, (item, index) => {
        item._id = index + 1
        makeDdisDev(item,state)
      })
      dispatch(types.SET_DDIS_DEVS, ddisDevs)
    }
    dispatch(types.REFRESH_DDIS_DEVS_END)
  })
}

//topo summary
export function doLoadTopoSummary({dispatch, state}, {init = false, cb = {}} = {}) {
  if (init && state.devmng.topoSummary.init) {
    return
  }
  devmngApi.getItems('topoDevsSummary').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_TOPO_SUMMARY, res.data.data.topoDevsSummary)
      cb && cb.success && cb.success()
    } else {
      cb && cb.error && cb.error(res.data)
    }
  }, function(res) {
    cb && cb.noResponse && cb.noResponse()
  })
}

//topo binds
export function doLoadTopoBinds({dispatch, state}, {init = false} = {}) {
  if (init && state.devmng.topoBinds.init) {
    return
  }
  devmngApi.getItems('topoBinds').then(function(res) { 
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_TOPO_BINDS, res.data.data.topoBinds)
    }
  })
}

//topo devmodel name
export function doLoadDevmodelSummary({dispatch, state}, {init = false} = {}) {
  
  if (init && state.devmng.devModelsSummary.init) {
    return
  }
  devmngApi.getItems('devModelsSummary').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_DEVMODEL_SUMMARY, res.data.data.devModelsSummary)
    }
  })
}