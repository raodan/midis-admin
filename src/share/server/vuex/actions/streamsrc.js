import _ from 'lodash'
import ebus from 'libs/ebus.js'
import {streamSrcApi} from '../../../webapp/api.js'
import * as types from '../mutation-types'

//ipcam
function makeIpcam(ipcam) {
  ipcam._checked = false
  ipcam.srcType = 'ipc'
  ipcam.topoId = 0
  ipcam.portName = ipcam.portName ? ipcam.portName : `ipc_${ipcam._id}`  
}

export function doLoadIpcams({dispatch, state}, init) {  
  if (init && state.ipcam.ipcam.init) {
    return
  }
  let extendQuery = {
    filterDisable: 1
  }
  dispatch(types.REFRESH_IPCAMS_START)
  streamSrcApi.getItems('ipcameras', {extendQuery: JSON.stringify(extendQuery)}).then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var ipcameras = res.data.data.ipcameras
      dispatch(types.SET_IPCAMS, ipcameras)
    }
    dispatch(types.REFRESH_IPCAMS_END)
  })
}

export function setRecencOperationCfg({dispatch}, val) {
  dispatch(types.SET_RECENC_OPERATETION_DETAIL, val)
}

export function setIpcamChecked({dispatch}, id, val) {
  dispatch(types.SET_IPCAM_CHECKED, id, val)
}

export function setIpcamAllUnchecked({dispatch}) {
  dispatch(types.SET_IPCAM_ALL_UNCHECKED)
}

//ipcam verdor
function makeVendor(vendor) {
  vendor.productModelsStr = ''
  vendor.productModelsEdit = ''
  let models = JSON.parse(vendor.productModels)
  if (Array.isArray(models)) {
    for (var i = 0; i < models.length; i++) {
      vendor.productModelsStr += "<p class='my-table-textarea'>" + models[i] + "</p>"
      if (i > 0) {
        vendor.productModelsEdit += '|'
      }
      vendor.productModelsEdit += models[i]
    }
  }
}

export function doLoadVendors({dispatch, state}, init) {
  if (init && state.ipcam.vendor.init) {
    return
  }
  dispatch(types.REFRESH_IPCAM_VENDORS_START)
  streamSrcApi.getItems('ipcamVendorCfgs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      let vendors = _.forEach(res.data.data.ipcamVendorCfgs, (item) => {
        makeVendor(item)
      })
      dispatch(types.SET_IPCAM_VENDORS, vendors)
    }
    dispatch(types.REFRESH_IPCAM_VENDORS_END)
  })
}

var index = 0
function addStreams(dispatch, srcType, items, useItemSrc) {
  let streams = _.map(items, (item) => {
    index++
    return {
      _id: index, 
      _checked: false,
      topoId: useItemSrc ? item.topoId : 0,
      name: item.name,
      tag: item.tag,
      portName: item.portName ? item.portName : `${srcType}_${item._id}`,
      mainUrl: item.mainUrl,
      previewUrl: item.previewUrlReal ? item.previewUrlReal : item.previewUrl,
      srcType: srcType,        
      displayName: item.displayName        
    }    
  })
  dispatch(types.ADD_STREAM_SRCS, srcType, streams)
}

export function setStreamChecked({dispatch}, id, val) {
  dispatch(types.SET_STREAM_SRC_CHECKED, id, val)
}
export function setStreamItemChecked({dispatch}, srcs, val) {
  dispatch(types.SET_STREAM_SRC_ITEM_CHECKED, srcs, val)
}
export function setIPCStreamChecked({dispatch}, id, val) {
  dispatch(types.SET_IPCSTREAM_SRC_CHECKED, id, val)
}

export function setStreamAllUnchecked({dispatch}) {
  dispatch(types.SET_STREAM_SRC_ALL_UNCHECKED)
}
export function setStreamCurPageChecked({dispatch}, srcs, index, count) {  
  dispatch(types.SET_STREAM_SRC_CUR_PAGE_CHECKED, srcs, index, count)
}
export function setStreamAllChecked({dispatch}) {
  dispatch(types.SET_STREAM_SRC_ALL_CHECKED)
}
export function setIPCStreamAllUnchecked({dispatch}) {
  dispatch(types.SET_IPCSTREAM_SRC_ALL_UNCHECKED)
}
export function setLoadStreamSrcsStart({dispatch}) {
  dispatch(types.SET_LOAD_STREAMSRCS_START)
}
export function setLoadStreamSrcsEnd({dispatch}) {
  dispatch(types.SET_LOAD_STREAMSRCS_END)
}

//softenc
export function doLoadSoftEncs({dispatch, state}, {init = false} = {}) {
  if (init && state.softenc.init) {
    return
  }
  dispatch(types.REFRESH_SOFTENCS_START)
  streamSrcApi.getItems('softencs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_SOFTENCS, res.data.data.softencs)
      addStreams(dispatch, 'softenc', res.data.data.softencs, false)
    }
    dispatch(types.REFRESH_SOFTENCS_END)
  })
}

//superhdscreen
export function doLoadSuperHdScreens({dispatch, state}, {init = false} = {}) {
  if (init && state.superhdscreen.init) {
    return
  }
  dispatch(types.REFRESH_SUPERHDSCREENS_START)
  streamSrcApi.getItems('superhdscreens').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_SUPERHDSCREENS, res.data.data.superhdscreens)
      addStreams(dispatch, 'superhdscreen', res.data.data.superhdscreens, false)
    }
    dispatch(types.REFRESH_SUPERHDSCREENS_END)
  })
}

//mtsrc
export function doLoadMtSrcs({dispatch, state}, {init = false} = {}) {
  if (init && state.mtsrc.init) {
    return
  }
  dispatch(types.REFRESH_MTSRCS_START)
  streamSrcApi.getItems('mtsrcs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_MTSRCS, res.data.data.mtsrcs)
      addStreams(dispatch, 'mtsrc', res.data.data.mtsrcs, false)
    }
    dispatch(types.REFRESH_MTSRCS_END)
  })
}

//ipcsrc
export function doLoadIpcSrcs({dispatch, state}, {init = false} = {}) {
  if (init && state.ipcsrc.init) {
    return
  }
  dispatch(types.REFRESH_IPCSRCS_START)
  streamSrcApi.getItems('ipcameras').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_IPCSRCS, res.data.data.ipcameras)
      addStreams(dispatch, 'ipc', res.data.data.ipcameras, false)
    }
    dispatch(types.REFRESH_IPCSRCS_END)
  })
}

//encoder
export function doLoadEncoders({dispatch, state}, {init = false} = {}) {
  if (init && state.encoder.init) {
    return
  }
  dispatch(types.REFRESH_ENCODERS_START)
  streamSrcApi.getItems('encoders').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_ENCODERS, res.data.data.encoders)
      addStreams(dispatch, 'encoder', res.data.data.encoders, true)
    }
    dispatch(types.REFRESH_ENCODERS_END)
  })
}

//encoder102e
export function doLoadEncoder102es({dispatch, state}, {init = false} = {}) {
  if (init && state.encoder102e.init) {
    return
  }
  dispatch(types.REFRESH_ENCODER102ES_START)
  streamSrcApi.getItems('encoder102es').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_ENCODER102ES, res.data.data.encoder102es)
      addStreams(dispatch, 'encoder102e', res.data.data.encoder102es, true)
    }
    dispatch(types.REFRESH_ENCODER102ES_END)
  })
}

//codec
export function doLoadCodecs({dispatch, state}, {init = false} = {}) {
  if (init && state.codec.init) {
    return
  }
  dispatch(types.REFRESH_CODECS_START)
  streamSrcApi.getItems('codecs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_CODECS, res.data.data.codecs)
      addStreams(dispatch, 'codecs', res.data.data.codecs, false)
    }
    dispatch(types.REFRESH_CODECS_END)
  })
}

//proxysrc
export function doLoadProxySrcs({dispatch, state}, {init = false} = {}) {
  if (init && state.proxysrc.init) {
    return
  }
  dispatch(types.REFRESH_PROXYSRCS_START)
  streamSrcApi.getItems('proxys').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_PROXYSRCS, res.data.data.proxys)
      addStreams(dispatch, 'proxy', res.data.data.proxys, false)
    }
    dispatch(types.REFRESH_PROXYSRCS_END)
  })
}

//capStream
export function doLoadCapStreams({dispatch, state}, {init = false} = {}) {
  if (init && state.capstream.init) {
    return
  }
  dispatch(types.REFRESH_CAPSTREAMS_START)
  streamSrcApi.getItems('capStreams').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_CAPSTREAMS, res.data.data.capStreams)
      addStreams(dispatch, 'capStream', res.data.data.capStreams, true)
    }
    dispatch(types.REFRESH_CAPSTREAMS_END)
  })
}

//rproxysrc
export function doLoadRProxySrcs({dispatch, state}, {init = false} = {}) {
  if (init && state.rproxysrc.init) {
    return
  }
  dispatch(types.REFRESH_RPROXYSRCS_START)
  streamSrcApi.getItems('rproxys').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_RPROXYSRCS, res.data.data.rproxys)
      addStreams(dispatch, 'rproxy', res.data.data.rproxys, false)
    }
    dispatch(types.REFRESH_RPROXYSRCS_END)
  })
}

//cascadeSrc
export function doLoadCascadeSrcs({dispatch, state}, {init = false} = {}) {
  if (init && state.cascade.src.init) {
    return
  }
  dispatch(types.REFRESH_CASCADESRCS_START)
  streamSrcApi.getItems('cascadesrcs').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_CASCADESRCS, res.data.data.cascadesrcs)
      addStreams(dispatch, 'cascadesrc', res.data.data.cascadesrcs, true)
    }
    dispatch(types.REFRESH_CASCADESRCS_END)
  })
}

//encoderMcns
export function doLoadencoderMcns({dispatch, state}, {init = false} = {}) {  
  if (init && state.cascade.mcns.init) {
    return
  }
  dispatch(types.REFRESH_ENCODERMCNS_START)
  streamSrcApi.getItems('encoderMcns').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_ENCODERMCNS, res.data.data.encoderMcns)
      addStreams(dispatch, 'encoderMcn', res.data.data.encoderMcns, true)
    }
    dispatch(types.REFRESH_ENCODERMCNS_END)
  })
}

//encoderTcns
export function doLoadencoderTcns({dispatch, state}, {init = false} = {}) {
  if (init && state.cascade.tcns.init) {
    return
  }
  dispatch(types.REFRESH_ENCODERTCNS_START)
  streamSrcApi.getItems('encoderTcns').then(function(res) {    
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_ENCODERTCNS, res.data.data.encoderTcns)
      addStreams(dispatch, 'encoderTcn', res.data.data.encoderTcns, true)
    }
    dispatch(types.REFRESH_ENCODERTCNS_END)
  })
}

//tcnIpcs
export function doLoadtcnIpcs({dispatch, state}, {init = false} = {}) {
  if (init && state.cascade.tcnIpcs.init) {
    return
  }
  dispatch(types.REFRESH_TCNIPCS_START)
  streamSrcApi.getItems('tcnIpcs').then(function(res) {    
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_TCNIPCS, res.data.data.tcnIpcs)
      addStreams(dispatch, 'tcnIpcs', res.data.data.tcnIpcs, true)
    }
    dispatch(types.REFRESH_TCNIPCS_END)
  })
}





