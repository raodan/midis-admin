import Vue from 'vue'
import Vuex from 'vuex'
//modules
import auth from './modules/auth'
import system from './modules/system'
import ipcam from './modules/ipcam'
import softenc from './modules/softenc'
import superhdscreen from './modules/superhdscreen'
import mtsrc from './modules/mtsrc'
import ipcsrc from './modules/ipcsrc'
import proxysrc from './modules/proxysrc'
import capstream from './modules/capstream'
import encoder from './modules/encoder'
import encoder102e from './modules/encoder102e'
import codec from './modules/codec'
import monitor from './modules/monitor'
import media from './modules/media'
import devmng from './modules/devmng'
import vidlayout from './modules/vidlayout'
import vlayout from './modules/vlayout'
import vswitch from './modules/vswitch'
import streamsrc from './modules/streamsrc'
import lcd from './modules/lcd'
import record from './modules/record'
import webapp from './modules/webapp'
import cascade from './modules/cascade'
import recenc from './modules/recenc'
import pclist from './modules/pclist'
import rproxysrc from './modules/rproxysrc'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    system,
    ipcam,
    softenc,
    superhdscreen,
    mtsrc,
    ipcsrc,
    proxysrc,
    capstream,
    encoder,
    encoder102e,
    codec,
    monitor,
    media,
    devmng,
    vidlayout,
    vlayout,
    vswitch,
    streamsrc,
    lcd,
    record,
    webapp,
    cascade,
    rproxysrc,
    recenc,
    pclist
  },
  strict: true
})