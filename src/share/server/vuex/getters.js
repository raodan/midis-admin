import {mediaPhyApi} from '../api.js'
import {getMultiScreenOfLcd} from 'share/server/utils.js'

export function devsOfLcd (state) {
  return state.devmng.topoSummary.list.filter((item) => {
    return item.devModel.includes('lcd')
  })
}

export function devsOfCascade (state) {
  return state.devmng.topoSummary.list.filter((item) => {
    return item.devModel.includes('cascade')
  })
}

function getLcdInfo(lcd) {
  var lcdInfo = {}

  let topoDev = lcd.topoDev
  if (topoDev.topoId < 0) {
    return null
  }
  if (lcd.virtualTopoId > 0) {
    if (lcd.modes.length <= 0) {
      return null
    }
    let mode = lcd.modes[lcd.modeId - 1]
    let area = _.find(mode.areas, {virtualTopoId: lcd.virtualTopoId})
    lcdInfo.topoId = lcd.virtualTopoId
    lcdInfo.multiScreenX = area.multiScreenX
    lcdInfo.multiScreenY = area.multiScreenY
    lcdInfo.name = topoDev.devName + '-' + mode.name + '-' + area.name
  } else {
    let multiScreen = getMultiScreenOfLcd(topoDev.devModel)
    lcdInfo.topoId = topoDev.topoId
    if ((multiScreen.x <= 0)
      || (multiScreen.y <= 0)) {
      lcdInfo.multiScreenX = lcd.multiScreenX
      lcdInfo.multiScreenY = lcd.multiScreenY      
    } else {
      lcdInfo.multiScreenX = multiScreen.x
      lcdInfo.multiScreenY = multiScreen.y
    }
    lcdInfo.name = topoDev.devName
  }

  lcdInfo.realTopoId = topoDev.topoId
  lcdInfo.base = lcd.base

  return lcdInfo 
}

export function currVSwitchLcdInfo (state) {
  return getLcdInfo(state.vswitch)
}

export function currLcdInfo (state) {
  return getLcdInfo(state.lcd)
}