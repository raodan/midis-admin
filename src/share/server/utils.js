import _ from 'lodash'
import {mediaPhyApi} from './api.js'
import {MD5} from 'libs/crypto.js'

export function getMultiScreenOfLcd(devModel) {
  var x = 1
  var y = 1
  if (devModel.includes('lcdwall')) {
    let multis = devModel.split('.')[2]
    x = parseInt(multis[0] + multis[1])
    y = parseInt(multis[2] + multis[3])       
  } else if (devModel.includes('lcd.dyn')) {
    x = -1
    y = -1
  }

  return {
    x,
    y
  }
}

export function loadLcdLayoutByName(topoIdOfLcd, name) {
  var paramlist = {
    name: name
  }

  mediaPhyApi.setDevId(topoIdOfLcd)
  return mediaPhyApi.sendCmd('loadLayoutByName', paramlist) 
}

export function saveLcdLayoutByName(topoIdOfLcd, name) {
  var paramlist = {
    name: name
  }

  mediaPhyApi.setDevId(topoIdOfLcd)
  return mediaPhyApi.sendCmd('saveLayoutByName', paramlist) 
}

export function setLcdWindows(topoIdOfLcd, multiScreenX, multiScreenY, windows) {
  var paramlist = {
    multiScreenX,
    multiScreenY,
    windowList: windows
  }

  mediaPhyApi.setDevId(topoIdOfLcd)
  return mediaPhyApi.sendCmd('setLayout', paramlist) 
}

export function setwLcdWindows(topoIdOfLcd, multiScreenX, multiScreenY, windows) {
  var paramlist = {
    layout: {
      multiScreenX,
      multiScreenY,
      windowList: windows
    }
    
  }

  mediaPhyApi.setDevId(topoIdOfLcd)
  return mediaPhyApi.sendCmd('setWLayout', paramlist) 
}

export function encryptPassword(plaintext) {
  let ciphertext = MD5(plaintext).toLowerCase()
  return 'md5#' + ciphertext  
}

export function getTopoDevsOptionsOfMedia(ctx, topoDevs, use) {
  let onlineStr = ctx.$t('brackets', {content: ctx.$t('common.online')})
  let topoDevsForUse = _.filter(topoDevs, (item) => {
    if (!item.mediaServerUses || item.mediaServerUses.includes(use)) {
      return true
    } else {
      return false
    }
  })
  var options = _.map(topoDevsForUse, (item) => {
    return {
      text: item.devName + (1 === item.online ? onlineStr : ''),
      value: item.topoId
    }
  })

  options.splice(0, 0, {
    text: ctx.$t('common.autosel'),
    value: -1
  })

  return options
}
export function setSrcSetsToSelect(list) {  
  var options = _.map(list, (item) => {
    return {
      text: item.name,
      value: item.id
    }
  })
  return options
}