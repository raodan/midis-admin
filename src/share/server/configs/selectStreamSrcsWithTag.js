import _ from 'lodash'
import {showPublicUrl} from 'libs/utils.js'

export function List(ctx) {
  var list = [{
    field: 'name',
    width: '40%'
  },{
    field: 'tag',
    width: '40%'
  }]

  if (ctx.showLevel >= 1) {
    list.splice(list.length, 0, {
      field: 'topoId',
      width: 'auto'    
    })

    list.splice(list.length, 0, {
      field: 'portName',
      width: 'auto'    
    })
  }

  if ((ctx.showLevel >= 2)
  || (ctx.showLevel === 0)) {
    list.splice(list.length, 0, {
      field: 'srcType',
      width: 'auto'    
    })
  }

  if (ctx.showLevel >= 3) {
    list.splice(list.length, 0, {
      field: 'mainUrl',
      width: '20%',
      show: function(item) {
        return showPublicUrl(item.mainUrl)
      }      
    })
    list.splice(list.length, 0, {
      field: 'previewUrl',
      width: '20%',
      show: function(item) {
        return showPublicUrl(item.previewUrl)
      }      
    })
  }

  _.forEach(list, (item) => {
    let keypath = 'list.' + item.field
    item.name = ctx.$t(keypath)
  })

  return list 
}