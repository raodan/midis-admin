/*
 * webapp访问页面为http，端口改为9081
 * 
 */

import Vue from 'vue'
import debugCfg from 'share/debugCfg.js'

var hostname = window.location.hostname;
var port1 = window.location.protocol === 'https:' ? '9080' : '9081';
var ssl1 = window.location.protocol === 'https:' ? 'https' : 'http';
var ws1 = websocket = window.location.protocol === 'https:' ? 'wss' : 'ws';
if (debugCfg.debugOn) {
	hostname = debugCfg.ctServerIp;
	ssl1 = 'https';
	port1 = '9080'
}
export var ssl = ssl1;
export var websocket = ws1;
export var port = port1;

export function makeUri(processName, devId, entry) {
  return `/devs/${devId}/${processName}/${entry}`
}

function makeApiUri(processName, devId, entry) {
  return `/api/${processName}/${entry}`
}

export function setHostname(str) {
  hostname = str
}

export function getHostname(str) {
  return hostname
}

export function getUrlPrefix(scheme) {
	var useScheme = scheme || ssl;
	var _port = scheme == 'http' ? 9081 : (scheme == 'ws' ? 9080 : port);//当传入http时，表示由https进入webapp的http页面，此时端口手动设置为9081
  if (hostname !== '') {
    return `${useScheme}://${hostname}:${_port}`
  } else if (debugCfg.debugOn) {
    return `${useScheme}://${debugCfg.ctServerIp}:${_port}`
  } else {
    return ''
  }
}

export function newWebsocket(uri) {
  return new WebSocket(getUrlPrefix('ws') + uri, ['chat'])
}

export function http(uri, method, data, {timeout = 300000, headers = {}, upload = {}} = {}) {
  var options = {
    url: getUrlPrefix() + encodeURI(uri),
    method: method,
    timeout: timeout, 
    data: data,
    headers: headers,
    upload: upload
  }

  if ((hostname !== '')
    || (debugCfg.debugOn)) {
    options.xhr = {}
    options.xhr.withCredentials = true
  }
  return Vue.http(options)
}

export function http1(host, uri, method, data, {timeout = 30000, headers = {}, upload = {}, beforeSend = {}} = {}) {
  var options = {
    url: host + encodeURI(uri),
    method: method,
    timeout: timeout, 
    data: data,
    headers: headers,
    beforeSend: beforeSend,
    upload: upload,
    xhr: {
      withCredentials: true
    }
  }
  return Vue.http(options)
}

import {ProcessApi, StorageApi, RestApi} from 'libs/api.js'

export var systemApi = new ProcessApi(http, 'sys', 0, makeUri)
export var streamSrcApi = new ProcessApi(http, 'streamsrc', 0, makeUri)
export var multimediaApi = new ProcessApi(http, 'multimedia', 0, makeUri)
export var devmultimediaApi = new ProcessApi(http, 'multimedia', -1, makeUri)
export var devmngApi = new ProcessApi(http, 'devmng', 0, makeUri)
export var mediaPhyApi = new ProcessApi(http, 'mediaPhy', -1, makeUri)
export var mediaApi = new ProcessApi(http, 'media', 0, makeUri)
export var transferApi = new ProcessApi(http, 'transfer', -1, makeUri)
export var cascadeTypeApi = new ProcessApi(http, 'cascade', 0, makeUri)
export var cascadeApi = new ProcessApi(http, 'cascade', -1, makeUri)
export var audioApi = new ProcessApi(http, 'audioprocessor', -1, makeUri)
export var conctrolApi = new ProcessApi(http, 'control', -1, makeUri)

export var clusterApi = new ProcessApi(http, 'cluster', 0, makeApiUri)
export var commonApi = new ProcessApi(http, 'common', 0, makeApiUri)

function runShell(shName) {
  let paramlist = {
    shName: shName
  }

  return commonApi.sendCmd('runShell', paramlist)
}

function runShellWithArg(shName, shArg) {
  let paramlist = {
    shName: shName,
    shArg: shArg
  }

  return commonApi.sendCmd('runShell', paramlist)
}

export var shortcutApi = {
  runShell: runShell,
  runShellWithArg: runShellWithArg,
  reboot () {
    runShell('restart.sh')
  }
}

export var storageApi = new StorageApi(http)

export var webAppApi = new RestApi(http, 'webapp')
export var scriptApi = new RestApi(http, 'script')

