import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import {
  SET_MULTIMEDIA_CFG,
  SET_PLATFORM,
  SET_SUBTITLE_LIST_OF_WINDOW,
  SET_SUBTITLE_LIST_OF_VPOUT,
} from './mutation-types'

Vue.use(Vuex)

const state = {
  multimedia: {
    cfg: {},
    subtitleListOfWindow: [],
    subtitleListOfVpOut: [],
  },
  platform: {
    devType: '',
    devModel: ''
  }
}

const mutations = {
  [SET_MULTIMEDIA_CFG] (state, val) {
    state.multimedia.cfg = val
  },
  [SET_PLATFORM] (state, val) {
    state.platform = val
  },
  [SET_SUBTITLE_LIST_OF_WINDOW] (state, val) {
    state.multimedia.subtitleListOfWindow = val
  },
  [SET_SUBTITLE_LIST_OF_VPOUT] (state, val) {
    state.multimedia.subtitleListOfVpOut = val
  }
}

export default new Vuex.Store({
  state,
  mutations
})