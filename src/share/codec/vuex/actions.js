import _ from 'lodash'
import {http, multimediaApi} from '../api.js'
import * as types from './mutation-types'

export function getPlatformInfo({dispatch}, cb) {
  http('/api/platform/cfg', 'GET').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_PLATFORM, res.data.data)
      cb && cb.success && cb.success()
    } else {
      cb && cb.error && cb.error(res.data)
    }
  }, function(res) {
    cb && cb.noResponse && cb.noResponse()
  })
}

export function getMultimediaCfg({dispatch}) {
  multimediaApi.getCfg().then(function(res) {
    if (res && res.data && res.data.code === 0) {
      dispatch(types.SET_MULTIMEDIA_CFG, res.data.data)
    }
  })
}

function makeFontColorShow(list) {
  _.forEach(list, function(item) {
    item.fontColorShow = '<div style="border-style:solid;border-color:#000;border-width:1px;background-color:' + item.fontColor + ';width:40px;height:20px;margin:0;margin-left:0;margin-right:auto"></div>'
  })
}

export function getSubtitleOfWindow({dispatch}) {
  multimediaApi.getItems('subtitleListOfWindow').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var list = res.data.data.subtitleListOfWindow
      makeFontColorShow(list)
      dispatch(types.SET_SUBTITLE_LIST_OF_WINDOW, list)
    }
  })
}

export function getSubtitleOfVpOut({dispatch}) {
  multimediaApi.getItems('subtitleListOfVpOut').then(function(res) {
    if (res && res.data && res.data.code === 0) {
      var list = res.data.data.subtitleListOfVpOut
      makeFontColorShow(list)
      dispatch(types.SET_SUBTITLE_LIST_OF_VPOUT, list)
    }
  })
}