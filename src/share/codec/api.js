import Vue from 'vue'

function makeUri(processName, devId, entry) {
  return `/dev/${processName}/${entry}`
}

function makeApiUri(processName, devId, entry) {
  return `/api/${processName}/${entry}`
}

var hostname = '';
export var ssl = window.location.protocol === 'https:' ? 'https' : 'http';

export function setHostname(val) {
  hostname = val
}

export function getUrlPrefix() {
  return `${ssl}://${hostname}:7080`
}

export function http(uri, method, data, {timeout = 30000, headers = {}, upload = {}} = {}) {
  var options = {
    url: getUrlPrefix() + encodeURI(uri),
    method: method,
    timeout: timeout, 
    data: data,
    headers: headers,
    upload: upload,
    xhr: {
      withCredentials: true
    }
  }

  return Vue.http(options)
}

import {ProcessApi, StorageApi, RestApi} from 'libs/api.js'

export var multimediaApi = new ProcessApi(http, 'multimedia', 0, makeUri)

