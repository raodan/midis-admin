import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import {
  GET_RECORDED_FILES,
  GET_RECORDED_FILES_LINK,
} from './mutation-types'

Vue.use(Vuex)

const state = {
  recordFiles: [],
  recordFilesLink: []
}

const mutations = {
  [GET_RECORDED_FILES] (state, val) {
    state.recordFiles = val
  },
  [GET_RECORDED_FILES_LINK] (state, val) {
    state.recordFilesLink = val
  }
 }

export default new Vuex.Store({
  state,
  mutations
})