export default {
  startRecord: 'Start Record',
  stopRecord: 'Stop Record',
  pauseRecord: 'Pause Record',
  resumeRecord: 'Resume Record',
  startLive: 'Start Live',
  stopLive: 'Stop Live',
  recOperate: 'Record Operation',
}