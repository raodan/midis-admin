export default {
  day: '1 day',
  week: '1 week',
  month: '1 month',
  quarter: '1 quarter',
  year: '1 year',
  days: '{num} days',
  weeks: '{num} weeks',
  months: '{num} months',
  quarters: '{num} quarters',
  years: '{num} years'
}