export default {
  standard: 'Standard 2010',
  standard2011: 'Standard 2011',
  standard2016: 'Standard 2016',
  haige: 'HAIGE',
  keda: 'KEDACOM',
  haikang: 'HIKVISION',
  dahua: 'DAHUA',
  yushi: 'UNIVIEW',
  yingfeituo: 'INFINOVA',
  jinzhi: 'WISCOM VISION',
  anxunshi: 'AXIS',
  tiandiweiye: 'TIANDY'
}