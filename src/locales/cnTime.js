export default {
  day: '1天',
  week: '1周',
  month: '1月',
  quarter: '1季度',
  year: '1年',
  days: '{num}天',
  weeks: '{num}周',
  months: '{num}月',
  quarters: '{num}季度',
  years: '{num}年'
}