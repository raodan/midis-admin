export default {
  username: '用户名',
  password: '密码',
  rememberme: '记住我',
  please: '请登陆',
  signin: '登陆',
  chgpwd: '修改密码',
  newPassword: '新登陆密码',
  reNewPassword: '重复新登陆密码',
}