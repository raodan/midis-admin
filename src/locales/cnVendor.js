export default {
  standard: '标准2010',
  standard2011: '标准2011',
  standard2016: '标准2016',
  haige: '海格',
  keda: '科达',
  haikang: '海康威视',
  dahua: '大华',
  yushi: '宇视',
  yingfeituo: '英飞拓',
  jinzhi: '金智',
  anxunshi: '安讯士',
  tiandiweiye: '天地伟业'
}