export default {
  type: {
    encoder: '编码器流',
    ipc: '网络摄像机流',
    proxy: '媒体代理流',
    softenc: '屏幕分享流',
    superhdscreen: '超高分辨率流',
    mtsrc: '监控平台流',
    capStream: '媒体抓包流',
    encoder102e: '102e编码流',
    encoderMcns: 'MCN编码流',
    encoderTcns: 'TCN编码流'

  }
}