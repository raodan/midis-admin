export default {
  startRecord: '启动录制',
  stopRecord: '停止录制',
  pauseRecord: '暂停录制',
  resumeRecord: '恢复录制',
  startLive: '开启直播',
  stopLive: '停止直播',
  recOperate: '录播操作',
}