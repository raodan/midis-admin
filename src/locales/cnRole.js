export default {
  sys: '系统维护员',
  ui: '客户端',
  webSysAdmin: '系统管理员',
  webSecAdmin: '安全保密管理员',
  webAuditor: '安全审计员',
  webUser: '操作员'
}