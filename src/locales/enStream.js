export default {
  type: {
    encoder: 'Encoder',
    ipc: 'Ip Camera',
    proxy: 'Proxy',
    softenc: 'Software Encoder',
    superhdscreen: 'Super HD Screen',
    mtsrc: 'Monitor Stream',
    capStream: 'Capture Stream',
    encoder102e: '102e Encoder',
    encoderMcns: 'MCN Encoder',
    encoderTcns: 'TCN Encoder'
  }
}