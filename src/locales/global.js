import enCommon from './enCommon.js'
import enError from './enError.js'
import enAlert from './enAlert.js'
import enTime from './enTime.js'
import enRole from './enRole.js'
import enAuth from './enAuth.js'
import enCode from './enCode.js'
import enVendor from './enVendor.js'
import enLcd from './enLcd.js'
import enApp from './enApp.js'
import enStream from './enStream.js'
import enTopo from './enTopo.js'
import enMedia from './enMedia.js'

import cnCommon from './cnCommon.js'
import cnError from './cnError.js'
import cnAlert from './cnAlert.js'
import cnTime from './cnTime.js'
import cnRole from './cnRole.js'
import cnAuth from './cnAuth.js'
import cnCode from './cnCode.js'
import cnVendor from './cnVendor.js'
import cnLcd from './cnLcd.js'
import cnApp from './cnApp.js'
import cnStream from './cnStream.js'
import cnTopo from './cnTopo.js'
import cnMedia from './cnMedia.js'

export default {
  en: {
    common: enCommon,
    code: enCode,
    error: enError,
    alert: enAlert,
    time: enTime,
    role: enRole,
    auth: enAuth,
    vendor: enVendor,
    lcd: enLcd,
    app: enApp,
    stream: enStream,
    topo: enTopo,
    media: enMedia,
    brackets: '({content})',
    msg: {
      sendCmdSuccess: 'Send cmd success'
    },
    tip: {
      seg: 'Use "|"'
    },
    video: {
      quality: 'quality',
      resolution: 'resolution',
      sd: 'SD',
      hd: 'HD'
    }
  },
  cn: {
    common: cnCommon,
    code: cnCode,
    error: cnError,
    alert: cnAlert,
    time: cnTime,
    role: cnRole,
    auth: cnAuth,
    vendor: cnVendor,
    lcd: cnLcd,
    app: cnApp,
    stream: cnStream,
    topo: cnTopo,
    media: cnMedia,
    brackets: '（{content}）',
    msg: {
      sendCmdSuccess: '命令发送成功'
    },
    tip: {
      seg: '使用“|”分割多个单元'
    },
    video: {
      quality: '画质',
      resolution: '视频源分辨率',
      sd: '标清',
      hd: '高清'
    }
  }  
}