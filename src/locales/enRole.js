export default {
  sys: 'System Maintainer',
  ui: 'Client',
  webSysAdmin: 'System Administrator',
  webSecAdmin: 'Security Administrator',
  webAuditor: 'Security Auditor',
  webUser: 'Operator'
}