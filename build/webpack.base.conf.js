var path = require('path')
var config = require('../config')
var utils = require('./utils')
var webpack = require('webpack')
var projectRoot = path.resolve(__dirname, '../')

try {
  require('os').networkInterfaces()
} catch (e) {
  require('os').networkInterfaces = () => ({})
}

module.exports = {
  entry: {
    app: './src/apps/' + config.build.appName + '/main.js'
  },
  output: {
    path: config.build.assetsRoot,
    publicPath: config.build.assetsPublicPath,
    filename: '[name].js'
  },
  resolve: {
    root: path.join(__dirname, '../'),
    extensions: ['', '.js', '.vue','.json'],
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      'src': path.resolve(__dirname, '../src'),
      'share': path.resolve(__dirname, '../src/share'),
      'locales': path.resolve(__dirname, '../src/locales'),
      'external': path.resolve(__dirname, '../src/external'),
      'libs': path.resolve(__dirname, '../src/libs'),
      'components': path.resolve(__dirname, '../src/components')
    },
    modulesDirectories: ['node_modules']
  },
  resolveLoader: {
    fallback: [path.join(__dirname, '../node_modules')]
  },
  plugins: [
    new webpack.ProvidePlugin({
  $: "jquery",
  jQuery: "jquery",
  jquery: "jquery",
  "window.jQuery": "jquery"
    })
  ],
  module: {
    loaders: [{
      test: /\.vue$/,
      loader: 'vue'
    }, {
      test: /\.js$/,
      loader: 'babel',
      include: projectRoot,
      exclude: [
        /node_modules/,
        /external/
      ]
    }, {
      test: /\.json$/,
      loader: 'json'
    }, {
      test: /\.html$/,
      loader: 'vue-html'
    }, {
      test: /\.(png|jpe?g|gif|svg|swf)(\?.*)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        name: utils.assetsPath('img/[name].[hash:7].[ext]')
      }
    }, {
      test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
      }
    }, {
      test: /\.js$/,
      loader: 'babel-loader',
      include: [
        path.resolve('src'), 
        path.resolve('test'),
        path.resolve('node_modules/_nuid@1.1.4@nuid/lib')
      ]
    }]
  },
  vue: {
    loaders: utils.cssLoaders()
  },
  babel: {
    // enable stage 0 babel transforms.
    presets: ['es2015', 'stage-2'],
    plugins: ['transform-runtime']
  },
  node: {
    fs: 'empty'
  }
}