// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')

var getAppName = function () {
  var appName = 'ct-server'
  if (process.argv.length >= 3) {
    appName = process.argv[2]
  }
  return appName
}
var appName = getAppName()

var getAssetsPublicPath = function () {
  if (process.env.NODE_ENV === 'production') {
    return appName.startsWith('webapp') ? `/${appName}/` : '/'
  } else {
    return '/'
  }
}

module.exports = {
  build: {
    env: require('./prod.env'),
    index: path.resolve(__dirname, '../dist/' + appName + '/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist/' + appName),
    assetsSubDirectory: 'static',
    assetsPublicPath: getAssetsPublicPath(),
    //assetsPublicPath: '', //相对路径
    productionSourceMap: false,
    appName: appName,
    appIndex: path.resolve(__dirname, '../src/apps/' + appName + '/index.html'),
  },
  dev: {
    env: require('./dev.env'),
    port: 9080,
    proxyTable: {
      '/api': {
        // target: 'http://10.9.7.12:9080',
        // target: 'http://10.9.7.90:9080',
        target: 'https://192.168.60.100:9080',
				changeOrigin: true,
				pathRewrite: {
					'^/api': '/' 
				}
      },
    }
  }
}
