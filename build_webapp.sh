#/bin/sh

function print_help {
  echo " "
  echo " Usage:"
  echo " # ./build_webapp.sh name [install]"
  echo " "
  echo " Valid name are:"
  echo "   webapp.vmarker"
  echo "   webapp.upgrade"
  echo "   webapp.api"
  echo "   webapp.ws"
  echo "   webapp.lcdbg"
  echo "   webapp.lcdwall.area"
  echo "   webapp.vkvm"
  echo "   ..."
}

if [ "$1" == "" ];
then
  print_help
  exit
fi

echo -e "\e[1;31m npm run build $1 \e[0m"
npm run build $1
echo -e "\e[1;31m npm run webapp make $1 \e[0m"
npm run webapp make $1
echo -e "\e[1;31m npm run webapp pack $1 \e[0m"
npm run webapp pack $1
if [ "$2" == "install" ];
then
  echo -e "\e[1;31m install $1 \e[0m"
  cp webapp/$1.tar /home/huangwei/project/founder/app/webapps/
fi

exit
