module.exports = [{
  id: 'webapp.api',
  version: '1.8.1',
  desc: 'WebApi List',
  provider: 'Team Founder',
  name: 'API',
  icon: 'icon.png',
  tag: '',
  category: 'debug',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'API'
    },
    cn: {
      name: 'API'
    }
  }
}, {
  id: 'webapp.ws',
  version: '1.8.1',
  desc: 'Websocket Test',
  provider: 'Team Founder',
  name: 'Websocket',
  icon: 'icon.png',
  tag: '',
  category: 'debug',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Websocket'
    },
    cn: {
      name: 'Websocket'
    }
  }
}, {
  id: 'webapp.lcdbg',
  version: '1.8.1',
  desc: 'Set background of lcd',
  provider: 'Team Founder',
  name: '背景设置',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'LCD Background'
    },
    cn: {
      name: '背景设置'
    }
  }
}, {
  id: 'webapp.lcdbanner',
  version: '1.8.1',
  desc: 'Set banner of lcd',
  provider: 'Team Founder',
  name: 'LCD横幅',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'LCD Banner'
    },
    cn: {
      name: 'LCD横幅'
    }
  }
},  {
  id: 'webapp.lcdbanner.mcn',
  version: '1.12.5',
  desc: 'Set banner of lcd',
  provider: 'Team Founder',
  name: 'MCN LCD横幅',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'MCN LCD Banner'
    },
    cn: {
      name: 'MCN LCD横幅'
    }
  }
}, {
  id: 'webapp.subtitle',
  version: '1.8.1',
  desc: 'Set subtitle of codec',
  provider: 'Team Founder',
  name: '字幕设置',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Subtitle Settings'
    },
    cn: {
      name: '字幕设置'
    }
  }
}, {
  id: 'webapp.vmarker',
  version: '1.8.1',
  desc: 'Video Marker',
  provider: 'Team Founder',
  name: '视频标注',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: 'ddis/devType[encoder]',
  i18n: {
    en: {
      name: 'Video Marker'
    },
    cn: {
      name: '视频标注'
    }
  }
}, {
  id: 'webapp.upgrade',
  version: '1.17.0',
  desc: 'Upgrade Tool',
  provider: 'Team Founder',
  name: '升级工具',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Upgrade Tool'
    },
    cn: {
      name: '升级工具'
    }
  }
}, {
  id: 'webapp.codec',
  version: '1.8.1',
  desc: 'Codec Settings',
  provider: 'Team Founder',
  name: '编解码器',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: 'ddis/devType[encoder];ddis/devType[decoder]',
  i18n: {
    en: {
      name: 'Codec Settings'
    },
    cn: {
      name: '编解码器'
    }
  }
}, {
  id: 'webapp.monitor.tree',
  version: '1.8.1',
  desc: 'Monitor Device Tree',
  provider: 'Team Founder',
  name: '监控平台树',
  icon: 'icon.png',
  tag: '',
  category: 'multimedia',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Monitor Device Tree'
    },
    cn: {
      name: '监控平台树'
    }
  }
}, {
  id: 'webapp.monitors',
  version: '1.8.1',
  desc: 'Monitor Devices For Mobile',
  provider: 'Team Founder',
  name: '监控平台',
  icon: 'icon.png',
  tag: '',
  category: 'multimedia',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Monitor Devices'
    },
    cn: {
      name: '监控平台'
    }
  }
}, {
  id: 'webapp.lcdwall.area',
  version: '1.8.1',
  desc: 'Cfg Area Of Lcdwall',
  provider: 'Team Founder',
  name: '拼接墙区域划分',
  icon: 'icon.png',
  tag: '',
  category: 'multimedia',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Area Of LCD Wall'
    },
    cn: {
      name: '拼接墙区域划分'
    }
  }
}, {
  id: 'webapp.vkvm',
  version: '1.8.1',
  desc: 'Virtual KVM',
  provider: 'Team Founder',
  name: '虚拟KVM',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Virtual KVM'
    },
    cn: {
      name: '虚拟KVM'
    }
  }
}, {
  id: 'webapp.net.test',
  version: '1.8.1',
  desc: 'Codec Network Test',
  provider: 'Team Founder',
  name: '网络测试工具',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Codec Network Test'
    },
    cn: {
      name: '网络测试工具'
    }
  }
}, {
  id: 'webapp.log',
  version: '1.9.4',
  desc: 'Debug Log',
  provider: 'Team Founder',
  name: '调试日志',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Debug Log'
    },
    cn: {
      name: '调试日志'
    }
  }
}, {
  id: 'webapp.manufacturers',
  version: '1.1.1',
  desc: 'manufacturers model',
  provider: 'Team Founder',
  name: '厂家型号',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Manufacturers Model'
    },
    cn: {
      name: '厂家型号'
    }
  }
}, {
  id: 'webapp.toolbox',
  version: '1.17.0',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Tool Box'
    },
    cn: {
      name: '工具命令'
    }
  }
}, {
  id: 'webapp.agingtest',
  version: '1.9.1',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Aging Test'
    },
    cn: {
      name: '老化测试'
    }
  }
}, {
  id: 'webapp.flvtest',
  version: '1.8.1',
  desc: 'Flv Player Test',
  provider: 'Team Founder',
  name: 'FLV',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'FLV'
    },
    cn: {
      name: 'FLV'
    }
  }
}, {
  id: 'webapp.rtmp.test',
  version: '1.8.1',
  desc: 'RTMP Test',
  provider: 'Team Founder',
  name: 'RTMP',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'RTMP'
    },
    cn: {
      name: 'RTMP'
    }
  }
},  {
  id: 'webapp.inisetting',
  version: '1.17.0',
  desc: 'System Configuration Setting',
  provider: 'Team Founder',
  name: 'STBPC',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'IniSetting'
    },
    cn: {
      name: '配置文件管理工具'
    }
  }
}, {
  id: 'webapp.agingtest.mcn',
  version: '1.17.0',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'MCN Aging Test'
    },
    cn: {
      name: 'MCN老化测试'
    }
  }
}, {
  id: 'webapp.previewstream',
  version: '1.10.3',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'Stream Preview'
    },
    cn: {
      name: '码流预览工具'
    }
  }
}, {
  id: 'webapp.kvm',
  version: '1.11.0',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'KVM Seat'
    },
    cn: {
      name: 'KVM坐席'
    }
  }
}, {
  id: 'webapp.TEController',
  version: '1.11.0',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'TEController'
    },
    cn: {
      name: 'TE遥控器'
    }
  }
}, {
  id: 'webapp.agingtest.tcn',
  version: '1.11.3',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'TCN Agingtest'
    },
    cn: {
      name: 'TCN老化测试'
    }
  }
}, {
  id: 'webapp.agingtest.tcn102',
  version: '1.11.4',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'TCN102 Agingtest'
    },
    cn: {
      name: 'TCN102老化测试'
    }
  }
}, {
  id: 'webapp.agingtest.mcu',
  version: '1.13.1',
  desc: 'Tool Box',
  provider: 'Team Founder',
  name: '工具命令',
  icon: 'icon.png',
  tag: '',
  category: 'tool',
  authCode: '',
  mimeType: '',
  i18n: {
    en: {
      name: 'MCU1090 Agingtest'
    },
    cn: {
      name: 'MCU1090老化测试'
    }
  }
}]